import React, { useEffect } from "react";
import "./InventoryDetailsProfile.css";
import { getInventory } from "../Redux/actions/inventoryactions";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import Spinner from "../Spinner/Spinner";

export default function InventoryProfile() {
  const params = useParams();
  const dispatch = useDispatch();
  const inventoryReducer = useSelector((store) => store.inventoryReducer);

  const { inventory, isLoading } = inventoryReducer;
  useEffect(() => {
    dispatch(getInventory(params.id));
  }, [dispatch, params.id]);
  if (isLoading || !inventory) return <Spinner />;
  return (
    <>
      <div className="container mt-5 mb-5">
        <div className="mb-5">
          <Link
            to="/login"
            style={{ textDecoration: "none", marginLeft: "5%" }}
          >
            <Button
              color="secondary"
              variant="outlined"
              style={{ fontSize: "14px" }}
            >
              <ArrowBackIosIcon />
              <span>Back</span>
            </Button>
          </Link>
        </div>
        <div className="row g-0">
          <div className="col-md-6 border-end">
            <div className="d-flex flex-column justify-content-center">
              <div className="main_image p-3">
                {" "}
                <img
                  src={
                    inventory.imagePath
                      ? process.env.REACT_APP_API_URL + inventory.imagePath
                      : "/picture.jpg"
                  }
                  alt="Inventory"
                  width="90%"
                  height="auto"
                />{" "}
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="right-side my-4">
              <div className="d-flex justify-content-between align-items-center">
                <h3 style={{color: "#ff6600"}}>
                  {inventory.name} ({inventory.inventoryId})
                </h3>{" "}
                <span className="heart">
                  <i className="bx bx-heart"></i>
                </span>
              </div>
              <div className="mt-2 pr-3 content">
                <p className="lead" style={{ fontSize: "1rem" }}>
                  Notes: {inventory.notes}
                </p>
              </div>
              <h5 className="lead">Procurred cost: {inventory.cost}</h5>
              <hr />
            </div>
            <div className="right-side my-4">
              <div className="row">
                <div className="col">
                  <div className="row">
                    <div className="col-3">
                      <h6 className="mb-0">Type</h6>
                    </div>
                    <div className="col-9">
                      <p>{inventory.category?.name}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <div className="row">
                    <div className="col">
                      <h6 className="mb-0">Brand</h6>
                    </div>
                    <div className="col">
                      <p>{inventory.brand || "-"}</p>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="row">
                    <div className="col">
                      <h6 className="mb-0">Model</h6>
                    </div>
                    <div className="col">
                      <p>{inventory.model || "-"}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <div className="row">
                    <div className="col">
                      <h6 className="mb-0">Procurred At</h6>
                    </div>
                    <div className="col">
                      <p>{inventory.procurredAt}</p>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="row">
                    <div className="col">
                      <h6 className="mb-0">Created At</h6>
                    </div>
                    <div className="col">
                      <p>{inventory.createdAt}</p>
                    </div>
                  </div>
                </div>
              </div>
              {inventory.requiresServicing && (
                <div>
                  <hr />
                  <h5>Servicings info</h5>
                  <div className="row">
                    <div className="col">
                      <div className="row">
                        <div className="col">
                          <h6 className="mb-0">Service At</h6>
                        </div>
                        <div className="col">
                          <p>
                            {
                              inventory.servicings.find(
                                (s) => s.isCurrentServicing
                              ).serviceAt
                            }
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              {inventory.inRepair && (
                <div className="alert alert-warning">
                  Repair is in progress...
                </div>
              )}
              {inventory.inServicing && (
                <div className="alert alert-warning">
                  Servicing is in progress...
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
