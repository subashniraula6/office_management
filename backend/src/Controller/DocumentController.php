<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\DocumentCategory;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use App\Controller\UserController;
use Symfony\Component\Filesystem\Filesystem;

class DocumentController extends AbstractController
{
    /**
     * @Route("/api/users/{id}/documents", name="create_document", methods={"POST"})
     */
    public function postDocument(Request $request, $id, SerializerInterface $serializer, UserController $userController)
    {
        $em = $this->getDoctrine()->getManager();

        $user =  $em->getRepository(User::class)->findOneBy(['employeeId' => $id]);
        $category = $em->getRepository(DocumentCategory::class)->findOneBy(['category' => $_POST['category']]);

        //check if  user has already document of the requested category
        $documents = $user->getDocuments()->getValues(); //to array
        if (!empty($documents)) {
            $document = $em->getRepository(Document::class)->findOneBy(['category' => $category, 'user' => $user]);
            if (in_array($document, $documents)) {
                $document = $documents[array_search($document, $documents)];
                // delete previous file
                $filesystem = new Filesystem();
                $filesystem->remove($document->getImagePath());
            }
        }
        // new document
        if (empty($document)) {
            $document = new Document();
            $document->setCategory($category);
            $document->setUser($user);
        }
        //upload file
        $file = $request->files->get('file');
        $destination = 'images/';
        $imagepath = '';
        $originalFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $newFileName = Urlizer::urlize($originalFileName) . '-' . uniqid() . '.' . $file->guessExtension();
        $path = $file->move($destination, $newFileName);
        $imagePath = $path->getPathName();
        $validTypes = ["image/jpg", "image/jpeg", 'image/png'];

        $document->setImagePath($imagePath);
        $user->addDocument($document);
        $em->persist($document);
        $em->persist($user);
        $em->flush();

        // return updatedUser
        return $userController->fetchUser($id);
    }
}
