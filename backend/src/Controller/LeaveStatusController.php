<?php

namespace App\Controller;

use App\Entity\LeaveStatus;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class LeaveStatusController extends AbstractController
{
    /**
     * @Route("/leave/status", name="leave_status", methods="GET")
     */
    public function index()
    {
        $allLeaveStatus = $this->getDoctrine()->getRepository(LeaveStatus::class)->findAll();
        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($allLeaveStatus, 'json');
        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }
}
