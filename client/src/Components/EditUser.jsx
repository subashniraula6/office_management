import React, { useEffect, useState } from "react"
import {
	Person,
	Email,
	Work,
	Business,
	Home,
	Event,
	AccountBalance,
	AddIcCall,
	AccessibilityNew,
	CardMembership,
} from "@material-ui/icons"
import { Link } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import EditIcon from "@material-ui/icons/Edit"
import { fetchUser, updateUser } from "./Redux/actions/userActions"
import { useParams } from "react-router-dom"
import Spinner from "./Spinner/Spinner"
import { useHistory } from "react-router-dom"
import { Button } from "@material-ui/core"
import {
	FormControlLabel,
	Radio,
	FormLabel,
	FormControl,
	RadioGroup,
} from "@material-ui/core"
import FormInput from "./Common/Input/FormInput"
import { notify } from "./utils/toast"

export default function EditUser() {
	const params = useParams()
	//userData form state
	const [userData, setUserData] = useState({
		firstName: "",
		lastName: "",
		email: "",
		designation: "",
		roles: "",
		joinedAt: "",
		probationEndAt: "",
		employeeId: "",
		dateOfBirth: "",
		citizenshipNumber: "",
		panNumber: "",
		gender: "",
		phoneNumber: "",
		temporaryAddress: "",
		permanentAddress: "",
		department: "",
		relationName: "",
		relation: "",
		relationPhoneNumber: "",
		notes: "",
		accountNumber: "",
	})
	const dispatch = useDispatch()
	const userReducer = useSelector((store) => store.userReducer)
	let { user: storedUser, isLoading, errors } = userReducer

	let user = JSON.parse(JSON.stringify(storedUser))

	if (user) {
		user.roles = user ? user.roles[0] : ""
		user.citizenshipNumber = user?.userInfo?.citizenshipNumber
		user.panNumber = user?.userInfo?.panNumber
		user.phoneNumber = user?.userInfo?.phoneNumber
		user.temporaryAddress = user?.userInfo?.temporaryAddress
		user.permanentAddress = user?.userInfo?.permanentAddress
		user.relationName = user?.contactRelation?.name
		user.relation = user?.contactRelation?.relation
		user.relationPhoneNumber = user?.contactRelation?.phoneNumber
		user.notes = user?.userInfo?.notes
		user.accountNumber = user?.userInfo?.accountNumber
	}

	const history = useHistory()
	useEffect(() => {
		dispatch(fetchUser(params.id))
	}, [dispatch, params.id])

	useEffect(() => {
		setUserData({
			...userData,
			firstName: user?.firstName ?? "",
			lastName: user?.lastName ?? "",
			email: user?.email ?? "",
			designation: user?.designation ?? "",
			roles: user?.roles ? user.roles : "",
			joinedAt: user?.joinedAt ?? "",
			probationEndAt: user?.probationEndAt ?? "",
			employeeId: user?.employeeId,
			dateOfBirth: user?.dateOfBirth ?? "",
			citizenshipNumber: user?.userInfo.citizenshipNumber ?? "",
			panNumber: user?.userInfo.panNumber ?? "",
			gender: user?.gender ?? "",
			phoneNumber: user?.userInfo.phoneNumber ?? "",
			temporaryAddress: user?.userInfo.temporaryAddress ?? "",
			permanentAddress: user?.userInfo.permanentAddress ?? "",
			department: user?.department ?? "",
			relationName: user?.contactRelation?.name ?? "",
			relation: user?.contactRelation?.relation ?? "",
			relationPhoneNumber: user?.contactRelation?.phoneNumber ?? "",
			notes: user?.userInfo?.notes ?? "",
			accountNumber: user?.userInfo.accountNumber ?? "",
		})
		// eslint-disable-next-line
	}, [storedUser])

	// user form functions
	function handleChange(e) {
		const { name, value } = e.target
		setUserData({ ...userData, [name]: value })
	}
	function checkUpdated(userData, user) {
		//Check if updated
		let updated = false
		for (let key of Object.keys(userData)) {
			if (user[key] === userData[key] || (!user[key] && userData[key] === "")) {
				updated = false
			} else {
				console.log(user[key], userData[key])
				updated = true
				break
			}
		}
		return updated
	}
	function handleSubmit(e) {
		e.preventDefault()
		const updated = checkUpdated(userData, user)
		if (updated) {
			dispatch(updateUser(params.id, userData, history))
		} else {
			notify("error", "no changes made!")
		}
	}

	if (isLoading) return <Spinner />
	return (
		<>
			{console.log(userData)}
			<div className="border container pb-5">
				<div
					className="card-header text-white text-center mx-2 my-2"
					style={{ backgroundColor: "#ff6600" }}
				>
					<EditIcon /> Edit User
				</div>
				<form onSubmit={handleSubmit} noValidate>
					<div className="container mx-2 my-2">
						<div className="row justify-content-center align-items-center">
							<div className="mt-2">
								<h4>Basic Information</h4>
								<hr />
							</div>
							<FormInput
								type="text"
								label="First Name"
								name="firstName"
								value={userData.firstName}
								handleChange={handleChange}
								Icon={<Person />}
								required
								feedback={errors?.firstName}
							/>
							<FormInput
								type="text"
								label="Last Name"
								name="lastName"
								Icon={<Person />}
								value={userData.lastName}
								handleChange={handleChange}
								required
								feedback={errors?.lastName}
							/>
							<div className="col-lg-4">
								<FormControl component="fieldset">
									<FormLabel component="label">Gender</FormLabel>
									<RadioGroup
										row
										aria-label="gender"
										name="gender"
										value={userData.gender}
										onChange={handleChange}
									>
										<FormControlLabel
											value="female"
											control={<Radio />}
											label="Female"
										/>
										<FormControlLabel
											value="male"
											control={<Radio />}
											label="Male"
										/>
									</RadioGroup>
								</FormControl>
							</div>
							<FormInput
								name="email"
								type="email"
								value={userData.email}
								Icon={<Email />}
								label="Email"
								handleChange={handleChange}
								required
								feedback={errors?.email}
							/>

							<FormInput
								type="date"
								label="Date of birth(AD)"
								name="dateOfBirth"
								value={userData.dateOfBirth}
								handleChange={handleChange}
								required
							/>

							<FormInput
								type="text"
								label="Phone Number"
								name="phoneNumber"
								value={userData.phoneNumber}
								handleChange={handleChange}
								feedback={errors?.phoneNumber}
								required
							/>
						</div>
						<div className="row">
							<div className="mt-2">
								<h4>Correspondence Address</h4>
								<hr />
							</div>
							<div className="col-lg-4">
								<div className="form-group my-2 ">
									<label>
										<Home />
										Permanent Address<small className="text-danger">*</small>
									</label>
									<textarea
										className="form-control"
										col-lg="10"
										rows="3"
										name="permanentAddress"
										value={userData.permanentAddress}
										onChange={handleChange}
									></textarea>
								</div>
							</div>
							<div className="col-lg-4">
								<div className="form-group my-2 ">
									<label>
										<Home />
										Temporary Address<small className="text-danger">*</small>
									</label>
									<textarea
										className="form-control"
										rows="3"
										label="Temporary Address"
										name="temporaryAddress"
										value={userData.temporaryAddress}
										onChange={handleChange}
									></textarea>
								</div>
							</div>
						</div>
						<div className="row mt-4">
							<div className="mt-2">
								<h4>Official Details</h4>
								<hr />
							</div>
							<FormInput
								type="text"
								label="Citizenship Number"
								name="citizenshipNumber"
								value={userData.citizenshipNumber}
								handleChange={handleChange}
								Icon={<CardMembership />}
								min={0}
							/>
							<FormInput
								type="text"
								label="PAN Number"
								name="panNumber"
								value={userData.panNumber}
								handleChange={handleChange}
								Icon={<CardMembership />}
								min={0}
							/>
							<FormInput
								type="text"
								label="Bank A/C Number"
								name="accountNumber"
								value={userData.accountNumber}
								Icon={<AccountBalance />}
								min={0}
								handleChange={handleChange}
							/>
							<FormInput
								type="text"
								className="form-control"
								label="Employee ID"
								name="employeeId"
								value={userData.employeeId}
								handleChange={handleChange}
								Icon={<Person />}
								required
								feedback={errors?.employeeId}
							/>
							<div className="col-lg-4">
								<div className="form-group my-2 ">
									<label>
										<Work />
										Designation<small className="text-danger">*</small>
									</label>
									<select
										id="inputState"
										name="designation"
										value={userData.designation}
										onChange={handleChange}
									>
										<option value="Engineer">Engineer</option>
										<option value="CTO">CTO</option>
										<option value="HR">HR</option>
										<option value="Intern">Intern</option>
										<option value="Employee">Employee</option>
									</select>
								</div>
							</div>
							<div className="col-lg-4">
								<div className="form-group my-2 ">
									<label>
										<Business />
										Department<small className="text-danger">*</small>
									</label>
									<select
										id="inputState"
										name="department"
										value={userData.department}
										onChange={handleChange}
										required
									>
										<option>IT</option>
										<option>Admin</option>
										<option>HR</option>
										<option>Engineering</option>
										<option>Operation</option>
									</select>
								</div>
							</div>

							<FormInput
								type="date"
								label="Joined At"
								Icon={<Event />}
								name="joinedAt"
								value={new Date(userData.joinedAt)}
								handleChange={handleChange}
								required
							/>
							<FormInput
								type="date"
								name="probationEndAt"
								label="Probation End At"
								Icon={<Event />}
								value={userData.probationEndAt}
								handleChange={handleChange}
							/>
						</div>
						<div className="row mt-4">
							<div className="mt-2">
								<h4>Emergency Details</h4>
								<hr />
							</div>
							<FormInput
								type="text"
								label="Full Name"
								name="relationName"
								value={userData.relationName}
								Icon={<AccessibilityNew />}
								handleChange={handleChange}
							/>
							<FormInput
								type="text"
								className="form-control"
								label="Relation"
								name="relation"
								value={userData.relation}
								Icon={<AccessibilityNew />}
								handleChange={handleChange}
							/>
							<FormInput
								type="text"
								label="Emergency Contact Number"
								className="form-control"
								name="relationPhoneNumber"
								value={userData.relationPhoneNumber}
								handleChange={handleChange}
								Icon={<AddIcCall />}
							/>
						</div>
						<div className="row">
							<div className="mt-4">
								<h4>Authority</h4>
								<hr />
								<div className="form-group my-2 col-lg-3">
									<label>
										Role<small className="text-danger">*</small>
									</label>
									<select
										type="select"
										name="roles"
										value={userData.roles}
										onChange={handleChange}
									>
										<option value="ROLE_USER">User</option>
										<option value="ROLE_ADMIN">Admin</option>
									</select>
								</div>
							</div>
						</div>
						<hr />
						<div className="form-group my-2">
							<label>Notes:</label>
							<textarea
								className="form-control"
								col-lg="10"
								rows="3"
								name="notes"
								value={userData.notes}
								onChange={handleChange}
							></textarea>
						</div>
						<div className="form-group my-2">
							<Button type="submit" color="primary" variant="outlined">
								Edit
							</Button>
							<Link
								to={`/dashboard/users/${params.id}`}
								style={{ textDecoration: "none", margin: "8px" }}
							>
								<Button color="secondary" variant="outlined">
									Cancel
								</Button>
							</Link>
						</div>
					</div>
				</form>
			</div>
		</>
	)
}
