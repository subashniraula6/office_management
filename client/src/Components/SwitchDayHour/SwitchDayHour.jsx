import React from "react"
import { withStyles } from "@material-ui/core/styles"
import FormGroup from "@material-ui/core/FormGroup"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Switch from "@material-ui/core/Switch"
import { makeStyles } from "@material-ui/styles"
import { Grid, Typography } from "@material-ui/core"

const CustomSwitch = withStyles({
	switchBase: {
		color: "#ff6600",
		"&$checked": {
			color: "#ff6600",
		},
		"&$checked + $track": {
			backgroundColor: "#ff6600",
		},
	},
	checked: {},
	track: {},
})(Switch)

export default function SwitchDayHour({ showHour, setShowHour }) {
	const handleChange = (e) => {
		setShowHour(!showHour)
	}

	return (
		<div>
			<FormGroup>
				<Typography component="div">
					<Grid component="label" container alignItems="center" spacing={0.5}>
						<Grid item style={{ color: "#ff6600" }}>
							Day
						</Grid>
						<Grid item>
							<CustomSwitch checked={showHour} onChange={handleChange} />
						</Grid>
						<Grid item style={{ color: "#ff6600" }}>
							Hour
						</Grid>
					</Grid>
				</Typography>
			</FormGroup>
		</div>
	)
}
