<?php

namespace App\Controller;

use App\Entity\Inventory;
use App\Entity\Repair;
use App\Entity\Request;
use App\Entity\Servicing;
use App\Entity\User;
use App\Utils\Query;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    use Query;
    /**
     * @Route("/api/dashboard", name="dashboard", methods={"GET"})
     */
    public function dashboard(EntityManagerInterface $em): JsonResponse
    {
        // Users statistics
        $total_users = count($em->getRepository(User::class)->findAll());
        $total_inventories = count($em->getRepository(Inventory::class)->findAll());
        $inventory_category = $this->fetchInventoryCategories();
        $user_category = $this->fetchUserCategories();

        // user birthdays
        $users = $em->getRepository(User::class)->findBy(['hasLeft' => false]);
        // filter user based on upcoming days
        $users = array_filter($users, function ($user) {
            $dateOfirth = new DateTime($user->getDateOfBirth());
            return $dateOfirth->format('m') === date('m') && $dateOfirth->format('d') >= date('d');
        });
        // sort users based on birthdays
        usort($users, function ($a, $b) {
            return date('d', strtotime($a->getDateOfBirth())) - date('d', strtotime($b->getDateOfBirth()));
        });

        // make new birthday structure
        $birthdays = array_map(function ($user) {
            $user_birthday = new stdClass();
            $user_birthday->name = $user->getFirstName() . " " . $user->getLastName();
            $user_birthday->employeeId = $user->getEmployeeId();
            $user_birthday->dateOfBirth = $user->getDateOfBirth();
            $user_birthday->image = $user->getUserInfo()->getPpImagePath();
            return $user_birthday;
        }, $users);

        // Request statistics
        $total_requests = count($em->getRepository(Request::class)->findAll());
        $total_pending_requests = count($em->getRepository(Request::class)->findBy(['status' => 'pending']));
        $total_accepted_requests = count($em->getRepository(Request::class)->findBy(['status' => 'accepted']));
        $total_rejected_requests = count($em->getRepository(Request::class)->findBy(['status' => 'rejected']));

        // Servicing statistics
        $total_completed_servicings = count($em->getRepository(Servicing::class)->findBy(['isCompleted' => true]));
        $total_ongoing_servicings = count($em->getRepository(Servicing::class)->findBy(['isSent' => true, 'isCompleted' => false]));

        // Repair statistics
        $total_repairs = count($em->getRepository(Repair::class)->findAll());
        $total_completed_repairs = count($em->getRepository(Repair::class)->findBy(['isCompleted' => true]));
        $total_ongoing_repairs = count($em->getRepository(Repair::class)->findBy(['isCompleted' => false]));

        $result = array(
            'user' => [
                'total' => $total_users,
                'categories' => $user_category
            ],
            'inventory' => [
                'total' => $total_inventories,
                'categories' => $inventory_category
            ],
            'request' => [
                'total' => $total_requests,
                'pending' => $total_pending_requests,
                'accepted' => $total_accepted_requests,
                'rejected' => $total_rejected_requests
            ],
            'repair' => [
                'total' => $total_repairs,
                'completed' => $total_completed_repairs,
                'ongoing' => $total_ongoing_repairs
            ],
            'servicing' => [
                'completed' => $total_completed_servicings,
                'ongoing' => $total_ongoing_servicings
            ],
            'birthdays' => $birthdays
        );
        return new JsonResponse([
            'code' => 200,
            'errors' => null,
            'result' => $result,
        ]);
    }
}
