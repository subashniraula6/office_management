<?php

namespace App\Controller;

use App\Entity\User;
use DateTime;
use DateTimeImmutable;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
// use Symfony\Component\Security\Core\User\UserProviderInterface;

class AuthController extends AbstractController
{
    /**
     * @Route("/api/login", methods={"POST"})
     */
    public function getTokenUser(UserInterface $user, JWTTokenManagerInterface $JWTManager)
    {
        $hasLeft = $user->getHasLeft();
        if ($hasLeft) {
            return new JsonResponse(['message' => 'user left the Company']);
        }
        return new JsonResponse(['token' => $JWTManager->create($user)]);
    }

    /**
     * @Route("/api/oauth", methods={"POST"})
     */
    public function oAuth(Request $request, JWTTokenManagerInterface $JWTManager)
    {
        $id_token = $request->getContent();
        $payload = explode(".", $id_token)[1];
        $decoded_token = base64_decode($payload);
        $email = json_decode($decoded_token, 1)['email'];
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]);
        return new JsonResponse(['token' => $JWTManager->create($user)]);
    }

    /**
     * @Route("/api/me", methods={"GET"})
     */
    public function showUser()
    {
        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'email', 'designation', 'joinedAt', 'status', 'firstName', 'employeeId',
                'roles', 'inventory', 'name', 'brand', 'model', 'status', 'description', 'category', 'inventoryId', 'servicings', 'serviceAt',
                'allocations', 'alllocatedAt', 'isCurrentAllocation', 'user', 'userInfo', 'ppImagePath', 'imagePath', 'inRepair'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];
        $serializer = new Serializer($normalizers, $encoders);
        $user = $serializer->serialize($this->getUser(), 'json');
        return new JsonResponse(['user' => json_decode($user)]);
    }
}
