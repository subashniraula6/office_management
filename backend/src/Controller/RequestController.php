<?php

namespace App\Controller;

use App\Entity\Allocation;
use App\Entity\Inventory;
use App\Entity\Repair;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Request as UserRequest;
use App\Entity\Servicing;
use App\Repository\RequestRepository;
use Normalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use App\Utils\ValidateUtils;
use Doctrine\ORM\EntityManagerInterface;

class RequestController extends AbstractController
{
    use ValidateUtils;
    // admin views all requests
    /**
     * @Route("/api/requests", name="get_all_requests", methods={"GET"})
     */
    public function getAllRequests(RequestRepository $requestRepository, EntityManagerInterface $em)
    {
        parse_str($_SERVER['QUERY_STRING'], $result);
        $per_page = $result['per_page'];
        $page = $result['page'];
        $offset = $per_page * ($page - 1);
        // $total = count($this->getDoctrine()->getRepository(User::class)->findAll());

        //total count
        $query = $em->createQuery(
            'SELECT COUNT(r.id) 
            FROM APP\ENTITY\REQUEST r'
        );
        $count = $query->getResult();
        $count = $count[0][1];

        //Filter
        $user = null;
        if (array_key_exists('user', $result)) {
            $user = $result['user'];
        }
        $type = null;
        if (array_key_exists('type', $result)) {
            $type = $result['type'];
        }

        $sql = 'SELECT request.id from request
                    inner join user on request.user_id=user.id
                    where (user.first_name like :user
                    or user.last_name like :user)
                    and request.type like :type';

        $conn = $em->getConnection();
        $stmt = $conn->prepare($sql);
        $params = [
            'user' => '%' . $user . '%',
            'type' => '%' . $type . '%'
        ];
        $result = $stmt->executeQuery($params);
        $id_arr =  $result->fetchAllAssociative();
        // convert 2d array to 1d
        $id_arr = array_map(function ($d) {
            return $d['id'];
        }, $id_arr);

        $role = $this->getUser()->getRoles()[0];
        if ($role === 'ROLE_ADMIN') {
            $requests = $requestRepository->findBy(['id' => $id_arr], ['id' => 'DESC'], $per_page, $offset);
        } else if ($role === 'ROLE_USER') {
            $requests = $requestRepository->findBy(['user' => $this->getUser()], ['id' => 'DESC'], $per_page, $offset);
        }
        // handling circular reference error and ignoring attributes
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getType();
            },
            AbstractNormalizer::ATTRIBUTES => [
                "id", "createdAt", "type", "status", "inventory", "inventoryId", "name",
                "message", "user", "firstName", "lastName", 'response', 'subject', 'isUrgent'
            ]
        ];
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];
        $serializer = new Serializer($normalizers, $encoders);

        $result = $serializer->serialize($requests, 'json');

        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($result),
            'page' => (int)$page,
            'totalCount' => $count
        );
        return new JsonResponse($response, 200);
    }


    // admin views users request
    /**
     * @Route("/api/admin/requests/{id}", name="get_request", methods={"GET"})
     */
    public function getRequest($id, RequestRepository $requestRepository)
    {
        $roles = $this->getUser()->getRoles();

        // Check if current user is admin
        if (in_array('ROLE_ADMIN', $roles)) {
            $request = $requestRepository->find($id);
            //check if request id empty
            if (empty($request)) {
                $response = array(
                    'code' => 404,
                    'errors' => 'Request not found',
                    'result' => null
                );
                return new JsonResponse($response, 404);
            }

            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    return $object->getType();
                },
                AbstractNormalizer::IGNORED_ATTRIBUTES => ["createdAt", "joinedAt"]
            ];
            $encoders = [new JsonEncoder()];
            $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];
            $serializer = new Serializer($normalizers, $encoders);


            $result = $serializer->serialize($request, 'json');
            $em = $this->getDoctrine()->getRepository(UserRequest::class);

            $response = array(
                'code' => 200,
                'errors' => null,
                'result' => json_decode($result)
            );
            return new JsonResponse($response, 200);
        }
        $response = array(
            'code' => 401,
            'errors' => 'unauthorized',
            'result' => null
        );
        return new JsonResponse($response, 401);
    }

    // Make request
    /**
     * @Route("/api/requests", name="make_request", methods={"POST"})
     */
    public function makeRequest(Request $request)
    {

        $em = $this->getDoctrine()
            ->getManager();

        $parameters = json_decode($request->getContent(), true);
        $requestType = $parameters['type'];
        $message = $parameters['message'];
        $subject = $parameters['subject'];

        $request = new UserRequest();
        $request->setType($requestType);
        $request->setSubject($subject);
        $request->setMessage($message);
        $request->setCreatedAt(new \DateTime("NOW"));
        $request->setStatus('pending');
        $request->setIsUrgent($parameters['isUrgent']);

        if ($requestType === 'repair' || $requestType === 'upgrade') {
            $inventoryId = $parameters['inventoryId'];
            $inventory = $em->getRepository(Inventory::class)->findOneBy(['inventoryId' => $inventoryId]);
            $repair = $em->getRepository(Repair::class)->findOneBy(['inventory' => $inventory, 'isCurrentRepair' => true]);
            if ($repair) {
                $response = array(
                    'code' => 404,
                    'errors' => ['requestError' => 'Repair already in progress'],
                    'result' => null
                );
                return new JsonResponse($response, 400);
            }
            $request->setInventory($inventory);
        }
        $request->setUser($this->getUser());
        // Validation
        $err = $this->validateInput($request);
        if (!empty($err)) {
            $response = array(
                'code' => 422,
                'errors' => $err,
                'result' => null
            );
            return new JsonResponse($response, 422);
        }

        $em->persist($request);
        $em->flush();


        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::ATTRIBUTES => ['requests', 'status', 'subject', 'type', 'isUrgent', 'inventory', 'user', 'email', 'employeedId']
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($request, 'json');
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/api/admin/requests/{id}/{action}", name="respond_request", methods={"PUT"})
     */
    public function respondRequest($id, $action, Request $request, SerializerInterface $serializer)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $entityManager = $this->getDoctrine()->getManager();
        $parameters = json_decode($request->getContent(), 1);
        $request = $entityManager->getRepository(UserRequest::class)
            ->find($id);
        if ($request->getStatus() !== "pending") {
            $response = array(
                'code' => 404,
                'errors' => "Request already accepted/rejected",
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        $type = $request->getType();
        if ($action !== 'accept' && $action !== 'reject') {
            $response = array(
                'code' => 404,
                'errors' => "Bad request",
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        if ($action === 'reject') {
            $message = $parameters['message'];
            $request->setResponse($message);
            $request->setStatus('rejected');
        } else if ($action === 'accept') {
            $request->setStatus('accepted');
            if ($type === 'repair' || $type === 'upgrade') {
                // Make new repair/upgrade
                $inventory = $entityManager->getRepository(Inventory::class)->findOneBy(['inventoryId' => $parameters['inventoryId']]);
                if (empty($inventory)) {
                    $response = array(
                        'code' => 404,
                        'errors' => 'No inventory',
                        'result' => null
                    );
                    return new JsonResponse($response, 404);
                } else if ($inventory->getInRepair()) {
                    $response = array(
                        'code' => 400,
                        'errors' => 'Already in repair',
                        'result' => null
                    );
                }
                $repair = new Repair();
                $repair->setSentAt(new \DateTime('NOW'));
                $repair->setInventory($inventory);
                $repair->setIsCompleted(false);
                $repair->setIsCurrentRepair(true);
                $inventory->setInRepair(true);
                $entityManager->persist($inventory);
                $entityManager->persist($repair);
            }
        }

        $entityManager->persist($request);
        $entityManager->flush();


        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'name',  'inventory', 'inventoryId', 'subject', 'message', 'requests', 'status', 'type', 'user', 'firstName', 'lastName', 'response'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($request, 'json');
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }

    // Delete request
    /**
     * @Route("/api/requests/{id}", name="delete_request", methods={"DELETE"})
     */
    public function deleteRequest($id)
    {
        $request = $this->getDoctrine()->getRepository(UserRequest::class)->find($id);
        if (empty($request)) {
            $response = array(
                'code' => 404,
                'errors' => 'No request found!',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        $em = $this->getDoctrine()
            ->getManager();
        $requestId = $request->getId();
        $em->remove($request);
        $em->flush();
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => ['requestId' => $requestId]
        );
        return new JsonResponse($response, 200);
    }
}
