<?php

namespace App\Entity;

use App\Repository\LeaveRequestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LeaveRequestRepository::class)
 */
class LeaveRequest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $leaveDescription;

    /**
     * @ORM\Column(type="date")
     */
    private $dateOfLeave;

    /**
     * @ORM\Column(type="date")
     */
    private $dateOfArrival;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $declineMessage;



    /**
     * @ORM\ManyToOne(targetEntity=LeaveType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $leaveType;

    /**
     * @ORM\ManyToOne(targetEntity=LeaveStatus::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $leaveStatus;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="approvedRequest")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=Approvers::class, mappedBy="leaveRequest")
     */
    private $approvers;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $leaveMode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $periodOfDay;

    /**
     * @ORM\Column(type="time")
     */
    private $leaveTime;

    /**
     * @ORM\Column(type="time")
     */
    private $arrivalTime;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->approvers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLeaveDescription(): ?string
    {
        return $this->leaveDescription;
    }

    public function setLeaveDescription(string $leaveDescription): self
    {
        $this->leaveDescription = $leaveDescription;

        return $this;
    }

    public function getDateOfLeave(): ?\DateTimeInterface
    {
        return $this->dateOfLeave;
    }

    public function setDateOfLeave(\DateTimeInterface $dateOfLeave): self
    {
        $this->dateOfLeave = $dateOfLeave;

        return $this;
    }

    public function getDateOfArrival(): ?\DateTimeInterface
    {
        return $this->dateOfArrival;
    }

    public function setDateOfArrival(\DateTimeInterface $dateOfArrival): self
    {
        $this->dateOfArrival = $dateOfArrival;

        return $this;
    }

    public function getDeclineMessage(): ?string
    {
        return $this->declineMessage;
    }

    public function setDeclineMessage(string $declineMessage): self
    {
        $this->declineMessage = $declineMessage;

        return $this;
    }


    public function getLeaveType(): ?LeaveType
    {
        return $this->leaveType;
    }

    public function setLeaveType(?LeaveType $leaveType): self
    {
        $this->leaveType = $leaveType;

        return $this;
    }

    public function getLeaveStatus(): ?LeaveStatus
    {
        return $this->leaveStatus;
    }

    public function setLeaveStatus(?LeaveStatus $leaveStatus): self
    {
        $this->leaveStatus = $leaveStatus;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @return Collection|Approvers[]
     */
    public function getApprovers(): Collection
    {
        return $this->approvers;
    }

    public function addApprover(Approvers $approver): self
    {
        if (!$this->approvers->contains($approver)) {
            $this->approvers[] = $approver;
            $approver->setLeaveRequest($this);
        }

        return $this;
    }

    public function removeApprover(Approvers $approver): self
    {
        if ($this->approvers->removeElement($approver)) {
            // set the owning side to null (unless already changed)
            if ($approver->getLeaveRequest() === $this) {
                $approver->setLeaveRequest(null);
            }
        }

        return $this;
    }

    public function getLeaveMode(): ?string
    {
        return $this->leaveMode;
    }

    public function setLeaveMode(string $leaveMode): self
    {
        $this->leaveMode = $leaveMode;

        return $this;
    }

    public function getPeriodOfDay(): ?string
    {
        return $this->periodOfDay;
    }

    public function setPeriodOfDay(?string $periodOfDay): self
    {
        $this->periodOfDay = $periodOfDay;

        return $this;
    }

    public function getLeaveTime(): ?\DateTimeInterface
    {
        return $this->leaveTime;
    }

    public function setLeaveTime(\DateTimeInterface $leaveTime): self
    {
        $this->leaveTime = $leaveTime;

        return $this;
    }

    public function getArrivalTime(): ?\DateTimeInterface
    {
        return $this->arrivalTime;
    }

    public function setArrivalTime(\DateTimeInterface $arrivalTime): self
    {
        $this->arrivalTime = $arrivalTime;

        return $this;
    }
}
