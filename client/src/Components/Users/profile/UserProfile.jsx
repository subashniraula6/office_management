import React, { useEffect, useState } from "react";
import "./userprofile.css";
import { useSelector, useDispatch } from "react-redux";
import { fetchUser } from "../../Redux/actions/userActions";
import { Link, useParams } from "react-router-dom";
import Spinner from "../../Spinner/Spinner";
import ProfileTabs from "../tab/ProfileTabs";
import EditIcon from "@material-ui/icons/Edit";
import { Button } from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import FileUploader from "../../FileUploader/FileUploader";

const UserProfile = () => {
  const [imagePath, setImagePath] = useState("");
  const dispatch = useDispatch();
  const role = useSelector((store) => store.authReducer.user.roles[0]);
  const userReducer = useSelector((store) => store.userReducer);
  const { user, isLoading } = userReducer;
  const params = useParams();
  useEffect(() => {
    dispatch(fetchUser(params.id));
  }, [dispatch, params.id]);
  useEffect(() => {
    setImagePath(user?.userInfo.ppImagePath);
  }, [user]);
  const avatarImageUrl =
    user && user.gender === "female" ? "/femaleAvatar.png" : "/maleAvatar.png";
  if (isLoading || !user) return <Spinner />;
  return (
    <div>
      {user.hasLeft && (
        <div className="container mb-5">
          <h3 className="text-danger text-center">
            This user has left the company
          </h3>
          <h5 className="text-center">You can see profile here</h5>
        </div>
      )}
      <div>
        <Link
          to={role === "ROLE_ADMIN" ? "/dashboard/users" : "/login"}
          style={{ textDecoration: "none", marginLeft: "5%" }}
        >
          `
          <Button
            color="secondary"
            variant="outlined"
            style={{ fontSize: "14px" }}
          >
            <ArrowBackIosIcon />
            <span>Back</span>
          </Button>
        </Link>
      </div>
      <h3 className="text-center mb-4" style={{color: "#ff6600"}}>Users profile</h3>
      <div className="row text-md-start text-center">
        <div className="col-md-5">
          <div className="profile-img">
            <img
              src={
                imagePath
                  ? process.env.REACT_APP_API_URL + imagePath
                  : avatarImageUrl
              }
              style={{ height: "200px", width: "250px", objectFit: "contain" }}
              alt="profile"
              className="profile-img my-2"
            />
            {!user.hasLeft && (
              <FileUploader type="ppImage" setImagePath={setImagePath} />
            )}
          </div>
        </div>
        <div className="col-md-5" style={{ paddingTop: "50px" }}>
          <div className="profile-head">
            <h4 className="display-6">
              {user.gender === "male" ? "Mr. " : "Mrs. "}
              {user.firstName + " " + user.lastName}
            </h4>
            <h5>{user.designation}</h5>
            <p className="proile-rating" style={{ fontSize: "18px" }}>
              Employee id :{" "}
              <span style={{ color: "#ff6600", fontSize: "20px" }}>
                {user.employeeId}
              </span>
            </p>
          </div>
        </div>
        {user &&
          user.designation !== "CEO" &&
          role === "ROLE_ADMIN" &&
          !user.hasLeft && (
            <div className="col-md-2">
              <Link to={"/dashboard/users/edit/" + user.employeeId}>
                <button className="profile-edit-btn">
                  Edit Profile
                  <EditIcon
                    color="secondary"
                    style={{ fontSize: "1.1rem", margin: "4px" }}
                  />
                </button>
              </Link>
            </div>
          )}
      </div>
      <div className="container mt-3 d-flex justify-content-center">
        <div className="w-100 tab-content profile-tab">
          <ProfileTabs user={user} />
        </div>
      </div>
    </div>
  );
};

export default UserProfile;
