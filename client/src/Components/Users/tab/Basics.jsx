import React from "react"
import { dateFormat } from "../../utils/dateTimeFormat"
import "./infostyles.css"

const Basics = ({ user }) => {
	return (
		<div className="mb-3">
			<div className="row align-items-center">
				<div className="col-md-3">
					<p className="label">Email</p>
				</div>
				<div className="col-md-9">
					<p className="value">{user.email}</p>
				</div>
			</div>
			<div className="row align-items-center">
				<div className="col-md-3">
					<p className="label">Phone</p>
				</div>
				<div className="col-md-9">
					<p className="value">{user.userInfo.phoneNumber}</p>
				</div>
			</div>
			<div className="row align-items-center">
				<div className="col-md-3">
					<p className="label">Department</p>
				</div>
				<div className="col-md-9">
					<p className="value">{user.department}</p>
				</div>
			</div>
			<div className="row align-items-center">
				<div className="col-md-3">
					<p className="label">Designation</p>
				</div>
				<div className="col-md-9">
					<p className="value">{user.designation}</p>
				</div>
			</div>
			<div className="row align-items-center">
				<div className="col-md-3">
					<p className="label">Date of Birth(AD)</p>
				</div>
				<div className="col-md-9">
					<p className="value">{user.dateOfBirth || "-"}</p>
				</div>
			</div>
			<div className="row align-items-center">
				<div className="col-md-3">
					<p className="label">Joined at</p>
				</div>
				<div className="col-md-9">
					<p className="value">{user.joinedAt}</p>
				</div>
			</div>
			<div className="row align-items-center">
				<div className="col-md-3">
					<p className="label">Probation End At</p>
				</div>
				<div className="col-md-9">
					{console.log(user.probationEndAt)}
					<p className="value">
						{user.probationEndAt == null
							? "-"
							: dateFormat(user.probationEndAt)}
					</p>
				</div>
			</div>
		</div>
	)
}

export default Basics
