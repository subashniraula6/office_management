import React from "react";
import { Bar } from "react-chartjs-2";

const createData = (label, data) => ({
  labels: data && data.categories.map((c) => c.name),
  datasets: [
    {
      label: 'No of items',
      data: data && data.categories.map((c) => c.count),
      backgroundColor: [
        "rgba(255, 99, 132, 0.2)",
        "rgba(54, 162, 235, 0.2)",
        "rgba(255, 206, 86, 0.2)",
        "rgba(75, 192, 192, 0.2)",
        "rgba(153, 102, 255, 0.2)",
        "rgba(255, 159, 64, 0.2)",
      ],
      borderColor: [
        "rgba(255, 99, 132, 1)",
        "rgba(54, 162, 235, 1)",
        "rgba(255, 206, 86, 1)",
        "rgba(75, 192, 192, 1)",
        "rgba(153, 102, 255, 1)",
        "rgba(255, 159, 64, 1)",
      ],
      borderWidth: 1,
    },
  ],
});

const options = {
  scale: 1,
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          stepSize: 1,
          // max: 10,
          // min: 0,
        },
      },
    ],
  },
};

const BarDiagram = ({ label, data }) => (
  <Bar data={() => createData(label, data)} options={options} />
);

export default BarDiagram;
