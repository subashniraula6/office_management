<?php

namespace App\Entity;

use App\Repository\DisposeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DisposeRepository::class)
 */
class Dispose
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $disposedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isRecycled;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $recycledAt;

    /**
     * @ORM\ManyToOne(targetEntity=Inventory::class, inversedBy="disposes")
     * @ORM\JoinColumn(nullable=false, name="inventory_id", referencedColumnName="id")
     */
    private $inventory;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCurrent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDisposedAt(): ?string
    {
        return $this->disposedAt ? $this->disposedAt->format("F j, Y"): null;
    }

    public function setDisposedAt(\DateTimeInterface $disposedAt): self
    {
        $this->disposedAt = $disposedAt;

        return $this;
    }

    public function getIsRecycled(): ?bool
    {
        return $this->isRecycled;
    }

    public function setIsRecycled(bool $isRecycled): self
    {
        $this->isRecycled = $isRecycled;

        return $this;
    }

    public function getRecycledAt(): ?string
    {
        return $this->recycledAt ? $this->recycledAt->format("F j, Y"): null;
    }

    public function setRecycledAt(?\DateTimeInterface $recycledAt): self
    {
        $this->recycledAt = $recycledAt;

        return $this;
    }

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): self
    {
        $this->inventory = $inventory;

        return $this;
    }

    public function getIsCurrent(): ?bool
    {
        return $this->isCurrent;
    }

    public function setIsCurrent(bool $isCurrent): self
    {
        $this->isCurrent = $isCurrent;

        return $this;
    }
}
