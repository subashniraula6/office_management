<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CategoryController extends AbstractController
{
    /**
     * @Route("/api/categories", name="get_categories", methods={"GET"})
     */
    public function getCategories(SerializerInterface $serializer): Response
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        return new JsonResponse([
            'code' => 200,
            'errors' => null,
            'result' => json_decode($serializer->serialize($categories, 'json'))
        ]);
    }

    /**
     * @Route("/api/categories", name="create_category", methods={"POST"})
     */
    public function createCategory(Request $request, SerializerInterface $serializer): Response
    {
        $em = $this->getDoctrine()->getManager();
        $parameters = json_decode($request->getContent(), 1);
        
        //Check existing category
        $category = $em->getRepository(Category::class)->findOneBy(['name' => $parameters['category']]);
        if (empty($category)) {
            $category = new Category();
            $category->setName($parameters['category']);       
            $em->persist($category);
            $em->flush($category);
        }
        return new JsonResponse([
            'code' => 200,
            'errors' => null,
            'result' => json_decode($serializer->serialize($category, 'json'))
        ]);
    }
}
