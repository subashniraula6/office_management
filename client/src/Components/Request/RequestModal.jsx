import React, {useRef} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { Button } from '@material-ui/core';
import {Form} from 'react-bootstrap'
import { useParams } from 'react-router-dom';
import axios from '../utils/axios';
import { useDispatch } from 'react-redux';
import { notify } from '../utils/toast';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px'
  },
}));

export default function RequestModal({type}) {
  const params = useParams();
  const dispatch = useDispatch();
  const classes = useStyles();
  const messageRef = useRef();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function handleSubmit(e){
    e.preventDefault();
    const message = {message: messageRef.current.value, type}
    axios.post(`/api/inventories/${params.id}/requests`, JSON.stringify(message))
    .then(res => {
      dispatch({
        type: "UPDATE_INVENTORY_SUCCESS",
        payload: res.data.result,
      });
      setOpen(false);
      notify('success', 'Request sent!')
    })
    .catch(err => console.log(err))
  }
  return (
    <div>
      <Button variant='outlined' color="primary" onClick={handleOpen}>
        {type==='allocation'?'Request for allocation': 
        'Request for servicing'}
      </Button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <p id="transition-modal-description">
            <Form onSubmit={handleSubmit}>
                <Form.Group className="mb-3">
                  <Form.Label>Message</Form.Label>
                  <Form.Control as="textarea" rows={3} placeholder="Leave message" name="remarks" required ref={messageRef}/>
                </Form.Group>
                
                <Button color="primary" variant="outlined" type="submit" disableElevation>
                  Submit
                </Button>
            </Form>
            </p>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
