<?php

namespace App\Entity;

use App\Repository\InventoryRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InventoryRepository::class)
 */
class Inventory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $brand;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $model;


    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;


    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @Assert\NotBlank
     * 
     */
    private $inventoryId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cost;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $procurredAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $notes;

    /**
     * @ORM\OneToMany(targetEntity=Allocation::class, mappedBy="inventory", orphanRemoval=true)
     */
    private $allocations;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAllocatable;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAllocated;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDisposed;

    /**
     * @ORM\Column(type="boolean")
     */
    private $requiresServicing;

    /**
     * @ORM\OneToMany(targetEntity=Servicing::class, mappedBy="inventory", orphanRemoval=true)
     */
    private $servicings;

    /**
     * @ORM\OneToMany(targetEntity=Dispose::class, mappedBy="inventory", orphanRemoval=true)
     */
    private $disposes;

    /**
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "Please upload a valid PDF"
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagePath;

    /**
     * @ORM\OneToMany(targetEntity=Request::class, mappedBy="inventory", orphanRemoval=true)
     */
    private $requests;

    /**
     * @ORM\OneToMany(targetEntity=Repair::class, mappedBy="inventory", orphanRemoval=true)
     */
    private $repairs;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\Column(type="boolean")
     */
    private $inRepair;

    /**
     * @ORM\Column(type="boolean")
     */
    private $inServicing;

    public function __construct()
    {
        $this->allocations = new ArrayCollection();
        $this->isDisposed = false;
        $this->createdAt = new DateTime('NOW');
        $this->isAllocated = false;
        $this->servicings = new ArrayCollection();
        $this->disposes = new ArrayCollection();
        $this->requests = new ArrayCollection();
        $this->repairs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(?string $brand): self
    {
        if (empty($brand)) {
            $this->brand = null;
        } else {
            $this->brand = $brand;
        }

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $model): self
    {
        if (empty($model)) {
            $this->model = null;
        } else {
            $this->model = $model;
        }
        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->createdAt->format("F j, Y");
    }

    public function getInventoryId(): ?string
    {
        return $this->inventoryId;
    }

    public function setInventoryId(string $inventoryId): self
    {
        $this->inventoryId = $inventoryId;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(?int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getProcurredAt(): ?string
    {
        return $this->procurredAt ? $this->procurredAt->format("F j, Y") : null;
    }

    public function setProcurredAt(?\DateTimeInterface $procurredAt): self
    {
        $this->procurredAt = $procurredAt;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return Collection|Allocation[]
     */
    public function getAllocations(): Collection
    {
        return $this->allocations;
    }

    public function addAllocation(Allocation $allocation): self
    {
        if (!$this->allocations->contains($allocation)) {
            $this->allocations[] = $allocation;
            $allocation->setInventory($this);
        }

        return $this;
    }

    public function removeAllocation(Allocation $allocation): self
    {
        if ($this->allocations->removeElement($allocation)) {
            // set the owning side to null (unless already changed)
            if ($allocation->getInventory() === $this) {
                $allocation->setInventory(null);
            }
        }

        return $this;
    }

    public function getIsAllocatable(): ?bool
    {
        return $this->isAllocatable;
    }

    public function setIsAllocatable(bool $isAllocatable): self
    {
        $this->isAllocatable = $isAllocatable;

        return $this;
    }

    public function getIsAllocated(): ?bool
    {
        return $this->isAllocated;
    }

    public function setIsAllocated(bool $isAllocated): self
    {
        if ($this->isAllocatable) {
            $this->isAllocated = $isAllocated;
        } else {
            $this->isAllocated = false;
        }
        return $this;
    }

    public function getIsDisposed(): ?bool
    {
        return $this->isDisposed;
    }

    public function setIsDisposed(bool $isDisposed): self
    {
        $this->isDisposed = $isDisposed;

        return $this;
    }

    public function getRequiresServicing(): ?bool
    {
        return $this->requiresServicing;
    }

    public function setRequiresServicing(bool $requiresServicing): self
    {
        $this->requiresServicing = $requiresServicing;

        return $this;
    }

    /**
     * @return Collection|Servicing[]
     */
    public function getServicings(): Collection
    {
        return $this->servicings;
    }

    public function addServicing(Servicing $servicing): self
    {
        if (!$this->servicings->contains($servicing)) {
            $this->servicings[] = $servicing;
            $servicing->setInventory($this);
        }

        return $this;
    }

    public function removeServicing(Servicing $servicing): self
    {
        if ($this->servicings->removeElement($servicing)) {
            // set the owning side to null (unless already changed)
            if ($servicing->getInventory() === $this) {
                $servicing->setInventory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Dispose[]
     */
    public function getDisposes(): Collection
    {
        return $this->disposes;
    }

    public function addDispose(Dispose $dispose): self
    {
        if (!$this->disposes->contains($dispose)) {
            $this->disposes[] = $dispose;
            $dispose->setInventory($this);
        }

        return $this;
    }

    public function removeDispose(Dispose $dispose): self
    {
        if ($this->disposes->removeElement($dispose)) {
            // set the owning side to null (unless already changed)
            if ($dispose->getInventory() === $this) {
                $dispose->setInventory(null);
            }
        }

        return $this;
    }

    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    public function setImagePath(?string $imagePath): self
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * @return Collection|Request[]
     */
    public function getRequests(): Collection
    {
        return $this->requests;
    }

    public function addRequest(Request $request): self
    {
        if (!$this->requests->contains($request)) {
            $this->requests[] = $request;
            $request->setInventory($this);
        }

        return $this;
    }

    public function removeRequest(Request $request): self
    {
        if ($this->requests->removeElement($request)) {
            // set the owning side to null (unless already changed)
            if ($request->getInventory() === $this) {
                $request->setInventory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Repair[]
     */
    public function getRepairs(): Collection
    {
        return $this->repairs;
    }

    public function addRepair(Repair $repair): self
    {
        if (!$this->repairs->contains($repair)) {
            $this->repairs[] = $repair;
            $repair->setInventory($this);
        }

        return $this;
    }

    public function removeRepair(Repair $repair): self
    {
        if ($this->repairs->removeElement($repair)) {
            // set the owning side to null (unless already changed)
            if ($repair->getInventory() === $this) {
                $repair->setInventory(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getInRepair(): ?bool
    {
        return $this->inRepair;
    }

    public function setInRepair(bool $inRepair): self
    {
        $this->inRepair = $inRepair;

        return $this;
    }
    public function getInServicing(): ?bool
    {
        return $this->inServicing;
    }

    public function setInServicing(bool $inServicing): self
    {
        $this->inServicing = $inServicing;

        return $this;
    }
}
