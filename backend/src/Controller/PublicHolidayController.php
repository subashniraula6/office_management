<?php

namespace App\Controller;

use App\Entity\PublicHoliday;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Utils\LeaveCalculation;

class PublicHolidayController extends AbstractController
{
    use LeaveCalculation;
    /**
     * @Route("/public/holiday", name="public_holiday")
     */
    public function index(): Response
    {
        return $this->render('public_holiday/index.html.twig', [
            'controller_name' => 'PublicHolidayController',
        ]);
    }
    /**
     * @Route("api/add/publicHoliday", name="add_public_holiday", methods="POST")
     */
    public function addPublicHoliday(Request $request, EntityManagerInterface $em)
    {
        $userRole = $this->getUser()->getRoles();

        if ($userRole[0] == "ROLE_ADMIN") {
            $requestContent = $request->getContent();
            $requestContent = json_decode($requestContent, true);

            if ($requestContent['holidayDate'] !== null && $requestContent['holidayOccassion'] !== null) {
                $publicHoliday = new PublicHoliday();
                $publicHoliday->setHolidayDate(new \DateTime($requestContent['holidayDate']));
                $publicHoliday->setHolidayOccassion($requestContent['holidayOccassion']);
                $publicHoliday->setIsDeleted(false);
                $em->persist($publicHoliday);
                $em->flush();

                $con = $this->getDoctrine()->getConnection();
                $sql = 'SELECT * FROM public_holiday ORDER BY public_holiday.holiday_date';
                $stmt = $con->prepare($sql);
                $responseData = $stmt->executeQuery()->fetchAllAssociative();
                $normailzers = [new ObjectNormalizer()];
                $encoders = [new JsonEncoder()];
                $serializer = new Serializer($normailzers, $encoders);
                $data = $serializer->serialize($responseData, 'json');

                return new Response($data, 200, ['Content-Type' => 'application/json']);
            }
        }
    }

    /**
     * @Route("api/get/publicHoliday", name="get_public_holiday", methods="GET")
     */
    public function getPublicHoldiay()
    {
        $CFY = $this->findCurrentFiscalYear();
        $startCFY = $CFY['startDate'];
        $endCFY = $CFY['endDate'];
        $startCFY = $startCFY->format('Y-m-d');
        $endCFY = $endCFY->format('Y-m-d');

        $con = $this->getDoctrine()->getConnection();
        $sql = 'SELECT * FROM public_holiday WHERE public_holiday.holiday_date >=:start AND public_holiday.holiday_date <= :end ORDER BY public_holiday.holiday_date';
        $stmt = $con->prepare($sql);
        $params = array("start" => $startCFY, "end" => $endCFY);
        $responseData = $stmt->executeQuery($params)->fetchAllAssociative();

        $normailzers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normailzers, $encoders);
        $data = $serializer->serialize($responseData, 'json');
        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("api/delete/publicHoldiay/{id}", name="soft_delete_public_holiday", methods="PUT")
     */
    public function deletePublicHoliday($id, EntityManagerInterface $em)
    {
        $userRole = $this->getUser()->getRoles();

        if ($userRole[0] == "ROLE_ADMIN") {
            $delPublicHoliday = $this->getDoctrine()->getRepository(PublicHoliday::class)->findOneBy(['id' => $id]);
            $delPublicHoliday->setIsDeleted(true);
            $em->persist($delPublicHoliday);
            $em->flush();

            $con = $this->getDoctrine()->getConnection();
            $sql = 'SELECT * FROM public_holiday ORDER BY public_holiday.holiday_date';
            $stmt = $con->prepare($sql);
            $responseData = $stmt->executeQuery()->fetchAllAssociative();

            $normailzers = [new ObjectNormalizer()];
            $encoders = [new JsonEncoder()];
            $serializer = new Serializer($normailzers, $encoders);
            $data = $serializer->serialize($responseData, 'json');
            return new Response($data, 200, ['Content-Type' => 'application/json']);
        }
    }

    /**
     * @Route("api/edit/publicHoliday/{id}", name="edit_public_holiday", methods="PUT")
     */
    public function editPublicHoliday($id, Request $request, EntityManagerInterface $em)
    {
        $userRole = $this->getUser()->getRoles();

        if ($userRole[0] == "ROLE_ADMIN") {
            $reqData = $request->getContent();
            $reqData = json_decode($reqData, true);
            $publicHoliday = $this->getDoctrine()->getRepository(PublicHoliday::class)->findOneBy(['id' => $id]);
            if ($publicHoliday->getHolidayDate() == new \DateTime($reqData['holidayDate']) && $publicHoliday->getHolidayOccassion() == $reqData['holidayOccassion']) {
                return new Response("nothing changed", 400);
            } else {
                $publicHoliday->setHolidayDate(new \DateTime($reqData['holidayDate']));
                $publicHoliday->setHolidayOccassion($reqData['holidayOccassion']);
                $em->persist($publicHoliday);
                $em->flush();

                $con = $this->getDoctrine()->getConnection();
                $sql = 'SELECT * FROM public_holiday ORDER BY public_holiday.holiday_date';
                $stmt = $con->prepare($sql);
                $responseData = $stmt->executeQuery()->fetchAllAssociative();

                $normailzers = [new ObjectNormalizer()];
                $encoders = [new JsonEncoder()];
                $serializer = new Serializer($normailzers, $encoders);
                $data = $serializer->serialize($responseData, 'json');
                return new Response($data, 200, ['Content-Type' => 'application/json']);
            }
        }
    }
}
