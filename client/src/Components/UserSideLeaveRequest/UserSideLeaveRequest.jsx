import {
	TableContainer,
	Paper,
	Card,
	TableCell,
	makeStyles,
	Table,
	TableHead,
	TableRow,
	TableBody,
	Button,
} from "@material-ui/core"
import { withStyles } from "@material-ui/styles"
import React from "react"
import SwitchDayHour from "../SwitchDayHour/SwitchDayHour"
import axios from "../utils/axios"
import CollapsiableUSLRTable from "./CollapsiableUSLRTable"

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: "#ff6600",
		color: theme.palette.common.white,
		textAlign: "center",
	},
}))(TableCell)

const useStyles = makeStyles({
	table: {
		minWidth: 700,
		padding: "10px",
	},
	tableContainer: {
		width: "90%",
		margin: "auto",
	},
})

export default function UserSideLeaveRequest() {
	const classes = useStyles()
	const [leaveDetails, setLeaveDetails] = React.useState()
	React.useEffect(() => {
		axios
			.get("api/pending/leaveRequest/userProfile")
			.then((res) => setLeaveDetails(res.data))
			.catch((err) => console.log(err))
	}, [])
	const [showHour, setShowHour] = React.useState(false)

	return (
		<div>
			<TableContainer className={classes.tableContainer} component={Paper}>
				<Card
					style={{
						background: "#ff6600",
						height: "50px",
						width: "100%",
						margin: "auto",
						marginTop: "2%",
					}}
				>
					<h4
						style={{ color: "white", marginTop: "0.7%", textAlign: "center" }}
					>
						Leave Requests
					</h4>
				</Card>
				<br />
				<div className="row">
					<div className="col-10"></div>
					<div
						className="col"
						style={{
							marginBottom: "1%",
							marginLeft: "4%",
						}}
					>
						<SwitchDayHour showHour={showHour} setShowHour={setShowHour} />
					</div>
				</div>
				<Table className={classes.table} aria-label="customized table">
					<TableHead>
						<TableRow>
							<StyledTableCell></StyledTableCell>
							<StyledTableCell>Employee Name</StyledTableCell>
							<StyledTableCell>Leave Type</StyledTableCell>

							<StyledTableCell>Date of Leave</StyledTableCell>
							<StyledTableCell>Leave Time</StyledTableCell>
							<StyledTableCell>Date of Arrival</StyledTableCell>
							<StyledTableCell>Arrival Time</StyledTableCell>

							<StyledTableCell>Duration</StyledTableCell>
							<StyledTableCell>Supervisor</StyledTableCell>
							<StyledTableCell>Action</StyledTableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{leaveDetails &&
							leaveDetails.map((ld) => (
								<CollapsiableUSLRTable
									ld={ld}
									showHour={showHour}
									setLeaveDetails={setLeaveDetails}
								/>
							))}
						{leaveDetails && leaveDetails.length == 0 && (
							<TableRow>
								<StyledTableCell colSpan={10}>
									<p style={{ textAlign: "center" }}> No leave Requests yet!</p>
								</StyledTableCell>
							</TableRow>
						)}
					</TableBody>
					<br />
				</Table>
			</TableContainer>
		</div>
	)
}
