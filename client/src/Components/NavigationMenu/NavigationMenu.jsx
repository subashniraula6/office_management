import React, { useState } from "react"

import Menu from "@material-ui/core/Menu"
import MenuItem from "@material-ui/core/MenuItem"
import { Nav } from "react-bootstrap"
import { Link } from "react-router-dom"
export default function NavigationMenu({ path, activeStyle }) {
	const [anchorEl, setAnchorEl] = useState(null)

	const handleMouseEnter = (event) => {
		setAnchorEl(event.currentTarget)
	}

	const handleClose = () => {
		setAnchorEl(null)
	}
	return (
		<div>
			<Nav.Link
				onClick={handleMouseEnter}
				onMouseEnter={handleMouseEnter}
				as={Link}
				style={
					path === "/leave/applyLeave" ||
					path === "/leave/myLeaveDetails" ||
					path === "/leave/leaveRequests"
						? activeStyle
						: null
				}
			>
				Leave
			</Nav.Link>
			<Menu
				id="leave-menu"
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={handleClose}
			>
				<MenuItem>
					<Nav.Link
						onClick={handleClose}
						as={Link}
						to="/leave/applyLeave"
						style={path === "/leave/applyLeave" ? activeStyle : null}
					>
						Apply For Leave
					</Nav.Link>
				</MenuItem>
				<MenuItem>
					<Nav.Link
						onClick={handleClose}
						as={Link}
						to="/leave/myLeaveDetails"
						style={path === "/leave/myLeaveDetails" ? activeStyle : null}
					>
						My Leave Details
					</Nav.Link>
				</MenuItem>
				<MenuItem>
					<Nav.Link
						onClick={handleClose}
						as={Link}
						to="/leave/leaveRequests"
						style={path == "/leave/leaveRequests" ? activeStyle : null}
					>
						Leave Request
					</Nav.Link>
				</MenuItem>
			</Menu>
		</div>
	)
}
