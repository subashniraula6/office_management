import axios from "../../utils/axios";
import { notify } from "../../utils/toast";

export const fetchUsers = () => async (dispatch) => {
  try {
    const response = await axios.get("/api/users");
    const users = response.data.result;
    dispatch({
      type: "GET_USERS_SUCCESS",
      payload: users,
    });
  } catch (error) {
    dispatch({
      type: "GET_USERS_ERROR",
      payload: error.response,
    });
  }
};
export const fetchUserInventories = (id) => async (dispatch) => {
  try {
    const response = await axios.get(`/api/users/${id}/inventories`);
    dispatch({
      type: "GET_USER_INVENTORIES_SUCCESS",
      payload: response.data.result,
    });
  } catch (error) {
    dispatch({
      type: "GET_USER_INVENTORIES_ERROR",
      payload: error.response,
    });
  }
};
export const addUser = (user, history) => async (dispatch) => {
  try {
    const config = { headers: { "Content-Type": "multipart/form-data" } };
    const response = await axios.post(
      "/api/users",
      JSON.stringify(user),
      config
    );
    const result = response.data.result;
    dispatch({
      type: "ADD_USER_SUCCESS",
      payload: result,
    });
    history.push("/dashboard/users/" + result.employeeId);
    notify("success", "Successfully added!");
  } catch (error) {
    dispatch({
      type: "ADD_USER_ERROR",
      payload: error.response.data.errors,
    });
  }
};
export const updateUser = (id, user, history) => async (dispatch) => {
  try {
    const data = JSON.stringify(user);
    const response = await axios.put("/api/users/" + id, data);

    dispatch({
      type: "UPDATE_USER_SUCCESS",
      payload: response.data.result,
    });
    history.push("/dashboard/users/" + response.data.result.employeeId);
    notify("success", "Update success!");
  } catch (error) {
    dispatch({
      type: "UPDATE_USER_ERROR",
      payload: error.response.data.errors,
    });
  }
};

export const fetchUser = (id) => async (dispatch) => {
  try {
    const response = await axios.get(`/api/users/${id}`);
    const user = response.data.result;
    dispatch({
      type: "GET_USER_SUCCESS",
      payload: user,
    });
  } catch (error) {
    dispatch({
      type: "GET_USER_ERROR",
      payload: error.response,
    });
  }
};

export const removeUser = (id) => async (dispatch) => {
  try {
    const response = await axios.put(`/api/admin/users/${id}/remove`, null);
    const user = response.data.result;
    dispatch({
      type: "REMOVE_USER_SUCCESS",
      payload: user,
    });
    notify("success", "successfully removed!");
  } catch (error) {
    dispatch({
      type: "REMOVE_USER_ERROR",
      payload: error.response,
    });
  }
};
export const reviveUser = (id) => async (dispatch) => {
  try {
    const response = await axios.put(`/api/admin/users/${id}/revive`, null);
    const user = response.data.result;
    dispatch({
      type: "REVIVE_USER_SUCCESS",
      payload: user,
    });
    notify("success", "successfully restored!");
  } catch (error) {
    dispatch({
      type: "REVIVE_USER_ERROR",
      payload: error.response,
    });
  }
};
