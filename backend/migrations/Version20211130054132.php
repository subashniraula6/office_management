<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211130054132 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE leave_documents (id INT AUTO_INCREMENT NOT NULL, documents_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE leave_request (id INT AUTO_INCREMENT NOT NULL, leave_type_id INT NOT NULL, leave_status_id INT NOT NULL, leave_description VARCHAR(1000) NOT NULL, date_of_leave DATE NOT NULL, date_of_arrival DATE NOT NULL, start_first_half TINYINT(1) NOT NULL, start_second_half TINYINT(1) NOT NULL, end_first_half TINYINT(1) NOT NULL, end_second_half TINYINT(1) NOT NULL, decline_message VARCHAR(1000) NOT NULL, approvers LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_7DC8F7788313F474 (leave_type_id), INDEX IDX_7DC8F7782416DAC7 (leave_status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE leave_status (id INT AUTO_INCREMENT NOT NULL, leave_status_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE leave_type (id INT AUTO_INCREMENT NOT NULL, leave_type_name VARCHAR(255) NOT NULL, leave_type_total_days INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE leave_request ADD CONSTRAINT FK_7DC8F7788313F474 FOREIGN KEY (leave_type_id) REFERENCES leave_type (id)');
        $this->addSql('ALTER TABLE leave_request ADD CONSTRAINT FK_7DC8F7782416DAC7 FOREIGN KEY (leave_status_id) REFERENCES leave_status (id)');
        $this->addSql('ALTER TABLE repair DROP type');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE leave_request DROP FOREIGN KEY FK_7DC8F7782416DAC7');
        $this->addSql('ALTER TABLE leave_request DROP FOREIGN KEY FK_7DC8F7788313F474');
        $this->addSql('DROP TABLE leave_documents');
        $this->addSql('DROP TABLE leave_request');
        $this->addSql('DROP TABLE leave_status');
        $this->addSql('DROP TABLE leave_type');
        $this->addSql('ALTER TABLE repair ADD type VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
