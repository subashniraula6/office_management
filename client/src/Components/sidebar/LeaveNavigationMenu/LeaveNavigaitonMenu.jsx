import React, { useState } from "react"

import Menu from "@material-ui/core/Menu"
import MenuItem from "@material-ui/core/MenuItem"
import { Nav } from "react-bootstrap"
import { Link, useLocation } from "react-router-dom"

export default function LeaveNavigationMenu({ handleClose, anchorEl }) {
	const location = useLocation()
	const path = location.pathname
	let activeStyle = {
		color: "#ff6600",
	}
	return (
		<div>
			<Menu
				id="leave-menu"
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={handleClose}
			>
				<MenuItem>
					<Nav.Link
						onClick={handleClose}
						as={Link}
						to="/dashboard/leaveTab"
						style={path.includes(`/dashboard/leaveTab`) ? activeStyle : null}
					>
						Employee`s Leave
					</Nav.Link>
				</MenuItem>
				<MenuItem>
					<Nav.Link
						onClick={handleClose}
						as={Link}
						to="/dashboard/myLeaveDetails"
						style={
							path.includes(`/dashboard/myLeaveDetails`) ? activeStyle : null
						}
					>
						My Leave Details
					</Nav.Link>
				</MenuItem>
			</Menu>
		</div>
	)
}
