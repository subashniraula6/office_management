// import { mergeClasses } from "@material-ui/styles"
import React from "react"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableHead from "@material-ui/core/TableHead"
import TableFooter from "@material-ui/core/TableFooter"
import TableContainer from "@material-ui/core/TableContainer"
import TablePagination from "@material-ui/core/TablePagination"
import TableRow from "@material-ui/core/TableRow"
import Paper from "@material-ui/core/Paper"
import Card from "@material-ui/core/Card"
import Button from "@material-ui/core/Button"
import { withStyles } from "@material-ui/styles"
import { makeStyles } from "@material-ui/core"
import TablePaginationActions from "../TablePaginationActions/TablePaginationActions"
import CollapsiableTable from "./CollapsiableTable"
import TableChartIcon from "@material-ui/icons/TableChart"
import SwitchDayHour from "../SwitchDayHour/SwitchDayHour"

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: "#ff6600",
		color: theme.palette.common.white,
		textAlign: "center",
	},
}))(TableCell)

const useStyles = makeStyles({
	table: {
		minWidth: 700,
		padding: "10px",
	},
	tableContainer: {
		width: "90%",
		margin: "auto",
	},
})

export default function LeaveTable({
	leaveDetails,
	setLeaveDetails,
	page,
	setPage,
	rowsPerPage,
	setRowsPerPage,
	count,
	setCount,
	handleChangeFind,
	findBy,
}) {
	const classes = useStyles()

	const [showHour, setShowHour] = React.useState(false)

	const emptyRows =
		rowsPerPage - Math.min(rowsPerPage, count - page * rowsPerPage)

	const handleChangePage = (event, newPage) => {
		setPage(newPage)
	}

	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(parseInt(event.target.value, 10))
		setPage(0)
	}

	return (
		<div>
			<TableContainer className={classes.tableContainer} component={Paper}>
				<Card
					style={{
						background: "#ff6600",
						height: "50px",
						width: "100%",
						margin: "auto",
						marginTop: "2%",
					}}
				>
					<h4
						style={{ color: "white", marginTop: "0.7%", textAlign: "center" }}
					>
						<TableChartIcon style={{ fontSize: "30px" }} /> My Leave Details
					</h4>
				</Card>

				<div className="row">
					<div
						className="col-5"
						style={{ marginBottom: "1%", marginTop: "2%", textAlign: "center" }}
					>
						<label htmlFor="startDate" style={{ color: "#ff6600" }}>
							From
						</label>
						<input
							type="date"
							name="startDate"
							onChange={handleChangeFind}
							value={findBy.startDate}
						/>
					</div>
					<div
						className="col-5"
						style={{ marginBottom: "1%", marginTop: "2%", textAlign: "center" }}
					>
						<label htmlFor="endDate" style={{ color: "#ff6600" }}>
							To
						</label>
						<input
							type="date"
							name="endDate"
							onChange={handleChangeFind}
							value={findBy.endDate}
						/>
					</div>
					<div
						className="col"
						style={{
							marginBottom: "1%",
							marginTop: "2%",
							marginLeft: "4%",
							textAlign: "center",
						}}
					>
						<SwitchDayHour showHour={showHour} setShowHour={setShowHour} />
					</div>
				</div>

				<Table className={classes.table} aria-label="customized table">
					<TableHead>
						<TableRow centered>
							<StyledTableCell></StyledTableCell>
							<StyledTableCell>Leave Type</StyledTableCell>

							<StyledTableCell>Date of Leave</StyledTableCell>
							<StyledTableCell>Leave Time</StyledTableCell>
							<StyledTableCell>Date of Arrival</StyledTableCell>
							<StyledTableCell>Arrival Time</StyledTableCell>
							<StyledTableCell>Duration</StyledTableCell>
							<StyledTableCell>Leave Status</StyledTableCell>
							<StyledTableCell>Approvers</StyledTableCell>
							<StyledTableCell>Action</StyledTableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{leaveDetails.map((ld) => (
							<CollapsiableTable
								ld={ld}
								setLeaveDetails={setLeaveDetails}
								page={page}
								rowsPerPage={rowsPerPage}
								setCount={setCount}
								findBy={findBy}
								showHour={showHour}
							/>
						))}
						{emptyRows > 0 && (
							<TableRow style={{ height: 53 * emptyRows }}>
								<TableCell colSpan={1} />
							</TableRow>
						)}
					</TableBody>
					<TableFooter>
						<TableRow>
							<TablePagination
								rowsPerPageOptions={[5, 10, 25]}
								colSpan={9}
								count={count}
								rowsPerPage={rowsPerPage}
								page={page}
								SelectProps={{
									inputProps: { "aria-label": "rows per page" },
									native: true,
								}}
								onPageChange={handleChangePage}
								onRowsPerPageChange={handleChangeRowsPerPage}
								ActionsComponent={TablePaginationActions}
							/>
						</TableRow>
					</TableFooter>
				</Table>
			</TableContainer>
		</div>
	)
}
