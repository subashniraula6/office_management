import React, { useEffect, useState } from "react"
import LeaveTable from "../LeaveTable/LeaveTable"
import axios from "../utils/axios"

export default function MyLeaveDetails() {
	const [leaveDetails, setLeaveDetails] = useState([])
	const [page, setPage] = React.useState(0)
	const [rowsPerPage, setRowsPerPage] = React.useState(5)
	const [count, setCount] = React.useState()
	const [findBy, setFindBy] = React.useState({
		startDate: "",
		endDate: "",
	})
	const handleChangeFind = (e) => {
		setFindBy({ ...findBy, [e.target.name]: e.target.value })
	}
	useEffect(async () => {
		const responseData = await axios
			.get(
				`api/leave/requests?page=${page}&rowsPerPage=${rowsPerPage}&findBy=${JSON.stringify(
					findBy
				)}`
			)
			.then((response) => {
				return response.data
			})
			.catch((error) => console.log(error))

		setLeaveDetails(responseData.result)
		setCount(responseData.count)
	}, [page, rowsPerPage, findBy])
	return (
		<div>
			<LeaveTable
				leaveDetails={leaveDetails}
				setLeaveDetails={setLeaveDetails}
				page={page}
				setPage={setPage}
				rowsPerPage={rowsPerPage}
				setRowsPerPage={setRowsPerPage}
				count={count}
				setCount={setCount}
				handleChangeFind={handleChangeFind}
				findBy={findBy}
			/>
		</div>
	)
}
