import { useField } from "formik";

const TextArea = ({ label, ...props }) => {
  const [field, meta] = useField({ ...props, type: "textarea" });
  return (
    <div className='my-3'>
      <div className="d-flex justify-content-start">
        <label htmlFor={props.id || props.name}>{label}</label>
        <textarea {...field} {...props} />
      </div>
      {meta.touched && meta.error ? (
        <div className="error">{meta.error}</div>
      ) : null}
    </div>
  );
};
export default TextArea;
