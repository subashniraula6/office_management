import {
	TableContainer,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
	Button,
} from "@material-ui/core"
import React from "react"
import { withStyles, makeStyles } from "@material-ui/styles"
import axios from "../utils/axios"
import { useDispatch, useSelector } from "react-redux"
import { fetchCurrentUser } from "../Redux/actions/authActions"
import Card from "@material-ui/core/Card"

import SwitchDayHour from "../SwitchDayHour/SwitchDayHour"

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: "#ff6600",
		color: theme.palette.common.white,
		textAlign: "center",
	},
	body: {
		textAlign: "center",
	},
}))(TableCell)
const useStyles = makeStyles({
	tableContainer: {
		marginTop: "1%",
		height: 400,
		boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
	},
})

export default function MyLeaveStatistics() {
	const classes = useStyles()

	const [leaveStat, setLeaveStat] = React.useState()
	const [showHour, setShowHour] = React.useState(false)
	const { user } = useSelector((store) => store.authReducer)
	const dispatch = useDispatch()

	React.useEffect(() => {
		dispatch(fetchCurrentUser())
		axios
			.get(`api/leave/type?userId=${user.id}`)
			.then((res) => {
				return axios.post(`api/leaveStat`, res.data.data)
			})
			.then((res) => setLeaveStat(res.data))
			.catch((err) => console.log(err))
	}, [dispatch])
	return (
		<div style={{ width: "42%", height: 500 }}>
			<div className="d-flex justify-content-between">
				<h4
					style={{
						color: "#ff6600",
					}}
				>
					Your Leaves
				</h4>
				<SwitchDayHour showHour={showHour} setShowHour={setShowHour} />
			</div>

			<TableContainer className={classes.tableContainer} component={Card}>
				<Table className={classes.table} aria-label="customized table">
					<TableHead>
						<TableRow>
							<StyledTableCell>Leave Type</StyledTableCell>
							<StyledTableCell>Used</StyledTableCell>
							<StyledTableCell>Remaining</StyledTableCell>
							<StyledTableCell>Total</StyledTableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{leaveStat &&
							leaveStat.map((ls) => (
								<TableRow>
									<StyledTableCell>{ls.leaveTypeName}</StyledTableCell>
									<StyledTableCell>
										{showHour ? ls.usedHour : ls.usedDay}
									</StyledTableCell>
									<StyledTableCell>
										{showHour ? ls.remainingHour : ls.remainingDay}
									</StyledTableCell>
									<StyledTableCell>
										{showHour ? ls.totalHour : ls.totalDay}
									</StyledTableCell>
								</TableRow>
							))}
					</TableBody>
				</Table>
			</TableContainer>
		</div>
	)
}
