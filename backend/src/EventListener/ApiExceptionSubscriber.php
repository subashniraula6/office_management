<?php
    namespace App\EventListener;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpKernel\KernelEvents;
    use Symfony\Component\HttpKernel\Event\ExceptionEvent;

    final class ApiExceptionSubscriber implements EventSubscriberInterface
    {
        public static function getSubscribedEvents(): array
        {
            return [
                KernelEvents::EXCEPTION => 'onKernelException',

            ];
        }

        public function onKernelException(ExceptionEvent $event): void
        {
            $e = $event->getThrowable();
            $message = $e->getMessage();
            $code = $e->getCode();

            $response = array(
                'code' => $code,
                'error' => $message,
                'result' => null
            );
            // $event->setResponse(new JsonResponse($response, ($code!==0) ? $code: 500));
        }
    }