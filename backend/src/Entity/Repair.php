<?php

namespace App\Entity;

use App\Repository\RepairRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RepairRepository::class)
 */
class Repair
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sentAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $returnedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isCurrentRepair;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $remarks;

    /**
     * @ORM\ManyToOne(targetEntity=Inventory::class, inversedBy="repairs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $inventory;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cost;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCompleted;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSentAt(): ?string
    {
        return $this->sentAt->format("F j, Y");
    }

    public function setSentAt(?\DateTimeInterface $sentAt): self
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    public function getReturnedAt(): ?string
    {
        return $this->returnedAt ? $this->returnedAt->format("F j, Y"): null;
    }

    public function setReturnedAt(?\DateTimeInterface $returnedAt): self
    {
        $this->returnedAt = $returnedAt;

        return $this;
    }

    public function getIsCurrentRepair(): ?bool
    {
        return $this->isCurrentRepair;
    }

    public function setIsCurrentRepair(?bool $isCurrentRepair): self
    {
        $this->isCurrentRepair = $isCurrentRepair;

        return $this;
    }

    public function getRemarks(): ?string
    {
        return $this->remarks;
    }

    public function setRemarks(string $remarks): self
    {
        $this->remarks = $remarks;

        return $this;
    }

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): self
    {
        $this->inventory = $inventory;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(?int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getIsCompleted(): ?bool
    {
        return $this->isCompleted;
    }

    public function setIsCompleted(bool $isCompleted): self
    {
        $this->isCompleted = $isCompleted;

        return $this;
    }
}
