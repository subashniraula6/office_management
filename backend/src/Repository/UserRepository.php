<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findApprovers($id, $roles)
    {
        $con = $this->getEntityManager()->getConnection();
        if ($roles == "ROLE_USER") {
            $query = 'SELECT
            user.id,
            user.first_name,
            user.last_name,
            user.employee_id,
            user_info.pp_image_path
        FROM
            user
        JOIN user_info ON
            user.id = user_info.user_id
        WHERE user.id != :id';

            $stmt = $con->prepare($query);
            $stmt->bindParam('id', $id);
        } else {
            $query = 'SELECT
            user.id,
            user.first_name,
            user.last_name,
            user.employee_id,
            user_info.pp_image_path
        FROM
            user
        JOIN user_info ON
            user.id = user_info.user_id
        WHERE
            user.roles_id = 1';
            $stmt = $con->prepare($query);
        }

        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative();
    }
}
