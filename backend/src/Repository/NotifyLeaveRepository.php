<?php

namespace App\Repository;

use App\Entity\NotifyLeave;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NotifyLeave|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotifyLeave|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotifyLeave[]    findAll()
 * @method NotifyLeave[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotifyLeaveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NotifyLeave::class);
    }

    // /**
    //  * @return NotifyLeave[] Returns an array of NotifyLeave objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NotifyLeave
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
