<?php

namespace App\Entity;

use App\Repository\LeaveTypeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LeaveTypeRepository::class)
 */
class LeaveType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $leaveTypeName;

    /**
     * @ORM\Column(type="integer")
     */
    private $leaveTypeTotalDays;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPayable;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isYearlyRenewed;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLeaveTypeName(): ?string
    {
        return $this->leaveTypeName;
    }

    public function setLeaveTypeName(string $leaveTypeName): self
    {
        $this->leaveTypeName = $leaveTypeName;

        return $this;
    }

    public function getLeaveTypeTotalDays(): ?int
    {
        return $this->leaveTypeTotalDays;
    }

    public function setLeaveTypeTotalDays(int $leaveTypeTotalDays): self
    {
        $this->leaveTypeTotalDays = $leaveTypeTotalDays;

        return $this;
    }

    public function getIsPayable(): ?bool
    {
        return $this->isPayable;
    }

    public function setIsPayable(bool $isPayable): self
    {
        $this->isPayable = $isPayable;

        return $this;
    }

    public function getIsYearlyRenewed(): ?bool
    {
        return $this->isYearlyRenewed;
    }

    public function setIsYearlyRenewed(bool $isYearlyRenewed): self
    {
        $this->isYearlyRenewed = $isYearlyRenewed;

        return $this;
    }
}
