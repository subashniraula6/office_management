import React from "react"
import Paper from "@material-ui/core/Paper"
import { Row, Col, Container } from "react-bootstrap"
import Card from "@material-ui/core/Card"
import axios from "../utils/axios"
import {
	Avatar,
	List,
	ListItem,
	ListItemAvatar,
	ListItemText,
	makeStyles,
	Typography,
} from "@material-ui/core"
import { mergeClasses } from "@material-ui/styles"

const useStyles = makeStyles((theme) => ({
	root: {
		width: "100%",
		// maxWidth: "36ch",
	},
	inline: {
		display: "inline",
	},
}))

export default function TodaysLeavePaper() {
	const [leaveToday, setLeaveToday] = React.useState([])
	const classes = useStyles()
	React.useEffect(() => {
		axios
			.get("api/todays/leave")
			.then((res) => setLeaveToday(res.data))
			.catch((err) => console.log(err))
	}, [])
	return (
		<Col lg={6}>
			<h4 style={{ color: "#ff6600" }}>Employee's On Leave</h4>
			<div style={{ height: 250, overflow: "scroll", width: "100%" }}>
				<List className={classes.root}>
					{leaveToday &&
						leaveToday.map((lt) => (
							<ListItem alignItems="flex-start">
								<ListItemAvatar>
									<Avatar
										alt={lt.first_name}
										src={process.env.REACT_APP_API_URL + lt.pp_image_path}
									/>
								</ListItemAvatar>
								<ListItemText
									primary={lt.first_name + " " + lt.last_name}
									secondary={
										<Typography
											component="span"
											variant="body2"
											className={classes.inline}
											color="textPrimary"
										>
											{lt.leave_type_name}
										</Typography>
									}
								/>
							</ListItem>
						))}
					{leaveToday.length == 0 && (
						<div
							style={{
								border: "1px solid #f5c2c7",
								borderRadius: "4px",
								backgroundColor: "#f8d7da",
								height: 65,
								width: "100%",
							}}
						>
							<ListItem>
								<h6 style={{ color: "#842029", marginTop: "2%" }}>
									No one is on leave!
								</h6>
							</ListItem>
						</div>
					)}
				</List>
			</div>
		</Col>
	)
}
