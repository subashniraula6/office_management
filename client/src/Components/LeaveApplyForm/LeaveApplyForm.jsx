import React, { useState, useEffect, useRef } from "react"
import Container from "@material-ui/core/Container"
import Button from "@material-ui/core/Button"
import axios from "../utils/axios"
// import { SettingsInputAntennaTwoTone } from "@material-ui/icons"
import { notify } from "../utils/toast"
import { useHistory } from "react-router"
import MultipleSelectInput from "./MultipleSelectInput/MultipleSelectInput"
import { Card, Box, IconButton } from "@material-ui/core"
import ListAltIcon from "@material-ui/icons/ListAlt"
import AddIcon from "@material-ui/icons/Add"
import { dateFormat } from "../utils/dateTimeFormat"
import { Formik } from "formik"
import * as Yup from "yup"
import { useDispatch, useSelector } from "react-redux"
import { fetchCurrentUser } from "../Redux/actions/authActions"
import Spinner from "../Spinner/Spinner"

export default function LeaveApplyForm() {
	const [leaveDetails, setLeaveDetails] = useState({
		// dateOfLeave: "",
		// dateOfArrival: "",
		// leaveTime: "",
		// arrivalTime: "",
		// leaveType: "",
		// leaveDescription: "",
		// leaveMode: "",
		// periodOfDay: "",
	})
	const [isLoading, setIsLoading] = useState(false)
	const [approversOption, setApproversOption] = useState([])
	const [optionLeaveType, setOptionLeaveType] = useState([])
	const [joinDate, setJoinDate] = useState()
	const [userName, setUserName] = useState()
	const history = useHistory()
	const dispatch = useDispatch()

	const { user } = useSelector((store) => store.authReducer)
	useEffect(() => {
		dispatch(fetchCurrentUser())
		axios
			.get("api/leave/type")
			.then((response) => {
				setOptionLeaveType(response.data.data)
				setJoinDate(response.data.joinedAt.date)
				setUserName(response.data.firstName + " " + response.data.lastName)
				return axios.get("api/get/approvers")
			})
			.then((response) => setApproversOption(response.data))
			.catch((error) => console.log(error))
	}, [dispatch])

	const imageRef = useRef()
	// const handleChange = (event) => {
	// 	setLeaveDetails({
	// 		...leaveDetails,
	// 		[event.target.name]: event.target.value,
	// 	})
	// }

	// const handleSubmit = (event) => {
	// 	event.preventDefault()

	// 	var formData = new FormData()

	// 	var imageData = imageRef.current.files

	// 	for (var index = 0; index < Object.keys(imageData).length; index++) {
	// 		formData.append(`leaveDocuments${index}`, imageData[index])
	// 	}
	// 	formData.append("filesCount", Object.keys(imageData).length)
	// 	for (var key in leaveDetails) {
	// 		if (leaveDetails["leaveMode"] === "halfDay") {
	// 			if (leaveDetails["periodOfDay"] === "firstHalf") {
	// 				leaveDetails["leaveTime"] = "09:00"
	// 				leaveDetails["arrivalTime"] = "13:00"
	// 			}
	// 			if (leaveDetails["periodOfDay"] === "secondHalf") {
	// 				leaveDetails["leaveTime"] = "14:00"
	// 				leaveDetails["arrivalTime"] = "18:00"
	// 			}
	// 			leaveDetails["dateOfArrival"] = leaveDetails.dateOfLeave
	// 		}
	// 		if (leaveDetails["leaveMode"] === "fullDay") {
	// 			leaveDetails["leaveTime"] = "09:00"
	// 			leaveDetails["arrivalTime"] = "09:00"
	// 		}
	// 		formData.append(key, leaveDetails[key])
	// 	}
	// 	for (var pair of formData.entries()) {
	// 		console.log(pair[0] + ", " + pair[1])
	// 	}

	// 	// axios
	// 	// 	.post("api/leave/request", formData, {
	// 	// 		headers: {
	// 	// 			"Content-Type": "multipart/form-data",
	// 	// 		},
	// 	// 	})
	// 	// 	.then((response) => notify("success", "successfully applied"))
	// 	// 	.then(() => history.push("/leave/myLeaveDetails"))
	// 	// 	.catch((error) => console.log(error))
	// }

	return (
		<Container style={{ background: "#fff3e0", padding: "2%" }}>
			<div className="leaveApplyFormHeader">
				<Card style={{ background: "#ff6600", height: "50px" }}>
					<h5 style={{ color: "white", marginTop: "1%", textAlign: "center" }}>
						<ListAltIcon style={{ fontSize: "2.2rem" }} /> Leave Application
						Form
					</h5>
				</Card>
			</div>

			<Formik
				initialValues={{
					dateOfLeave: "",
					dateOfArrival: "",
					leaveTime: "",
					arrivalTime: "",
					leaveType: "",
					leaveDescription: "",
					leaveMode: "",
					periodOfDay: "",
				}}
				validationSchema={Yup.object({
					dateOfLeave: Yup.date().required("Required"),
					leaveType: Yup.string().required("Required"),
					leaveDescription: Yup.string().required("Required"),
					leaveMode: Yup.string().required("Required"),
					periodOfDay: Yup.string().when("leaveMode", {
						is: "halfDay",
						then: Yup.string().required("Required"),
					}),
					dateOfArrival: Yup.date().when("leaveMode", {
						is: (leaveMode) => leaveMode == "fullDay" || leaveMode == "custom",
						then: Yup.date().required("Required"),
					}),
					leaveTime: Yup.string().when("leaveMode", {
						is: "custom",
						then: Yup.string().required("Required"),
					}),
					arrivalTime: Yup.string().when("leaveMode", {
						is: "custom",
						then: Yup.string().required("Required"),
					}),
				})}
				onSubmit={(values, { setSubmitting }) => {
					var formData = new FormData()

					var imageData = imageRef.current.files

					for (var index = 0; index < Object.keys(imageData).length; index++) {
						formData.append(`leaveDocuments${index}`, imageData[index])
					}
					formData.append("filesCount", Object.keys(imageData).length)
					for (var key in values) {
						if (values["leaveMode"] === "halfDay") {
							if (values["periodOfDay"] === "firstHalf") {
								values["leaveTime"] = "09:00"
								values["arrivalTime"] = "13:00"
							}
							if (values["periodOfDay"] === "secondHalf") {
								values["leaveTime"] = "14:00"
								values["arrivalTime"] = "18:00"
							}
							values["dateOfArrival"] = values.dateOfLeave
						}
						if (values["leaveMode"] === "fullDay") {
							values["leaveTime"] = "09:00"
							if (values["dateOfLeave"] === values["dateOfArrival"]) {
								values["arrivalTime"] = "18:00"
							} else {
								values["arrivalTime"] = "09:00"
							}
							values["periodOfDay"] = ""
						}
						if (values["leaveMode"] === "custom") {
							values["periodOfDay"] = ""
						}
						formData.append(key, values[key])
					}
					for (var key in leaveDetails) {
						formData.append(key, leaveDetails[key])
					}
					for (var pair of formData.entries()) {
						console.log(pair[0] + ", " + pair[1])
					}
					const routePath =
						user.roles[0] == "ROLE_ADMIN"
							? "/dashboard/myLeaveDetails"
							: "/leave/myLeaveDetails"
					setIsLoading(true)
					axios
						.post("api/leave/request", formData, {
							headers: {
								"Content-Type": "multipart/form-data",
							},
						})
						.then((response) => {
							notify("success", "successfully applied")
							history.push(routePath)
							setIsLoading(false)
						})

						.catch((err) => {
							notify("error", err.response.data)
							setIsLoading(false)
						})
				}}
			>
				{({
					values,
					errors,
					touched,
					handleChange,
					handleBlur,
					handleSubmit,
					isSubmitting,
				}) =>
					isLoading == true ? (
						<Spinner />
					) : (
						<form onSubmit={handleSubmit}>
							<div className="row" style={{ marginTop: "3%" }}>
								<div className="col">
									<Card
										style={{
											boxShadow: "2px 2px 10px 2px #dbc6a4",
											width: "20%",
											float: "right",
										}}
									>
										<h6
											style={{
												color: "#ff6600",
												marginTop: "3%",
												textAlign: "center",
											}}
										>
											Join Date: {dateFormat(joinDate)}
										</h6>
									</Card>
								</div>
							</div>

							<div className="row">
								<div className="col-6">
									<label htmlFor="leaveType">Choose Leave Type</label>
									<select
										name="leaveType"
										className="custom-select"
										id="leaveType"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.leaveType}
									>
										<option selected value="">
											Please Choose Leave Type ...
										</option>
										{optionLeaveType.map((olt) => (
											<option key={olt.id} value={olt.id}>
												{olt.leave_type_name}
											</option>
										))}
									</select>
									<p className="text-danger">
										{errors.leaveType && touched.leaveType && errors.leaveType}
									</p>
								</div>
							</div>
							<br />
							<div className="row">
								<div className="col">
									<label htmlFor="leaveMode">Leave Mode:</label>
									<input
										type="radio"
										id="half_day"
										name="leaveMode"
										value="halfDay"
										onChange={handleChange}
										onBlur={handleBlur}
									/>

									<label htmlFor="half_day">Half Day</label>
									<input
										type="radio"
										id="full_day"
										name="leaveMode"
										value="fullDay"
										onChange={handleChange}
										onBlur={handleBlur}
									/>
									<label htmlFor="full_day">Full Day</label>
									<input
										type="radio"
										id="custom"
										name="leaveMode"
										value="custom"
										onChange={handleChange}
										onBlur={handleBlur}
									/>
									<label htmlFor="custom">Custom</label>
									<p className="text-danger">
										{errors.leaveMode && touched.leaveMode && errors.leaveMode}
									</p>
								</div>
								{values.leaveMode == "halfDay" && (
									<div className="col">
										<label htmlFor="period_of_day">Period of Day:</label>
										<input
											type="radio"
											name="periodOfDay"
											value="firstHalf"
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<label htmlFor="first_half">First Half</label>
										<input
											type="radio"
											name="periodOfDay"
											value="secondHalf"
											onChange={handleChange}
											onBlur={handleBlur}
										/>
										<label htmlFor="second_half">Second Half</label>
										<p className="text-danger">
											{errors.periodOfDay &&
												touched.periodOfDay &&
												errors.periodOfDay}
										</p>
									</div>
								)}
							</div>
							<br />
							<div className="row">
								<div className="row">
									<div className="col-6">
										<label>Date of Leave</label>
										<input
											type="date"
											className="form-control"
											name="dateOfLeave"
											id="dateOfLeave"
											onChange={handleChange}
											onBlur={handleBlur}
											value={values.dateOfLeave}
										/>
										<p className="text-danger">
											{errors.dateOfLeave &&
												touched.dateOfLeave &&
												errors.dateOfLeave}
										</p>
									</div>
									{values.leaveMode === "custom" && (
										<div className="col-6">
											<label htmlFor="leaveTime">Leave Time</label>
											<input
												className="form-control"
												type="time"
												name="leaveTime"
												id="leaveTime"
												value={values.leaveTime}
												onChange={handleChange}
												onBlur={handleBlur}
												min="09:00"
												max="18:00"
											/>
											<p className="text-danger">
												{errors.leaveTime &&
													touched.leaveTime &&
													errors.leaveTime}
											</p>
										</div>
									)}
								</div>

								<div className="row">
									{values.leaveMode !== "halfDay" && (
										<div className="col-6">
											<label>Date of Arrival</label>
											<input
												type="date"
												className="form-control"
												name="dateOfArrival"
												id="dateOfArrival"
												onChange={handleChange}
												onBlur={handleBlur}
												value={values.dateOfArrival}
											/>
											<p className="text-danger">
												{errors.dateOfArrival &&
													touched.dateOfArrival &&
													errors.dateOfArrival}
											</p>
										</div>
									)}
									{values.leaveMode === "custom" && (
										<div className="col-6">
											<label htmlFor="arrivalTime">Arrival Time</label>
											<input
												className="form-control"
												type="time"
												name="arrivalTime"
												id="arrivalTime"
												value={values.arrivalTime}
												onChange={handleChange}
												onBlur={handleBlur}
												min="09:00"
												max="18:00"
											/>
											<p className="text-danger">
												{errors.arrivalTime &&
													touched.arrivalTime &&
													errors.arrivalTime}
											</p>
										</div>
									)}
								</div>
							</div>
							<br />
							<div className="row">
								<div className="col-6">
									<MultipleSelectInput
										leaveDetails={leaveDetails}
										setLeaveDetails={setLeaveDetails}
										approversOption={approversOption}
									/>
								</div>
								<div className="col">
									<label>Leave Description</label>
									<br />
									<textarea
										name="leaveDescription"
										id="leaveDescription"
										cols="40"
										rows="3"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.leaveDescription}
									/>
									<p className="text-danger">
										{errors.leaveDescription &&
											touched.leaveDescription &&
											errors.leaveDescription}
									</p>
								</div>
							</div>
							<br />
							<div className="row">
								<div className="col">
									<label htmlFor="uploadDocument">
										Choose files or report if any? (Only images) <br />
										<i style={{ color: "#ff6600" }}>
											<b>Note: </b>Ctrl + select for multiple image selection
										</i>
									</label>

									<Box
										style={{
											border: "1px solid #adb5bd",
											borderRadius: "10px",
											background: "whitesmoke",
										}}
									>
										<input
											style={{
												marginTop: "1%",
												marginLeft: "1%",
												marginBottom: "1%",
											}}
											type="file"
											ref={imageRef}
											accept="image/*"
											multiple
										/>
									</Box>
								</div>
								<div className="col"></div>
							</div>
							<br />
							<br />
							<div className="row">
								<Button
									type="submit"
									style={{ width: "20%", color: "white" }}
									variant="contained"
									color="primary"
								>
									Apply
								</Button>
							</div>
						</form>
					)
				}
			</Formik>
		</Container>
	)
}
