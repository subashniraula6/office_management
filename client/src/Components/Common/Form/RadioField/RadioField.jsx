import React from "react";
import { useField } from "formik";

const RadioField = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <div className='my-3'>
      <label htmlFor={props.name}>{label}</label>
      <input type="radio" {...field} {...props} />
      {meta.touched && meta.error ? (
        <div className="error">{meta.error}</div>
      ) : null}
    </div>
  );
};

export default RadioField;
