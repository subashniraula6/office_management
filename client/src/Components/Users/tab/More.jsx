import React from "react";
import { Row, Col } from "react-bootstrap";
import "./infostyles.css";

const More = ({ user }) => {
  return (
    <Row>
      <Col md={5}>
        <div className="ps-2">
          <h5>Address details</h5>
          <hr />
          <hr />
        </div>
        <Row className="align-items-center justify-content-center">
          <Col md={4}>
            <p className="label">Temporary Address</p>
          </Col>
          <Col md={8}>
            <p className="value">{user.userInfo.temporaryAddress || "-"}</p>
          </Col>
        </Row>
        <Row className="align-items-center justify-content-center">
          <Col md={4}>
            <p className="label">Permanent Address</p>
          </Col>
          <Col md={8}>
            <p className="value">{user.userInfo.permanentAddress || "-"}</p>
          </Col>
        </Row>
      </Col>
      <Col md={5}>
        <div className="ps-2">
          <h5>Official details</h5>
          <hr />
          <hr />
        </div>
        <Row>
          <Col md={4}>
            <p className="label">Citizenship Number</p>
          </Col>
          <Col md={8}>
            <p className="value">{user.userInfo.citizenshipNumber || "-"}</p>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <p className="label">PAN Number</p>
          </Col>
          <Col md={8}>
            <p className="value">{user.userInfo.panNumber || "-"}</p>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <p className="label">Bank account number</p>
          </Col>
          <Col md={8}>
            <p className="value">{user.userInfo.accountNumber || "-"}</p>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <p className="label">Bio</p>
          </Col>
          <Col md={8}>
            <p className="value">{user.userInfo.notes}</p>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default More;
