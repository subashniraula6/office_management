import React from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";

const Redirector = () => {
  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );
  const role = useSelector((store) => store.authReducer.user?.roles[0]);
  const employeeId = useSelector((store) => store.authReducer.user?.employeeId);
  console.log(isAuthenticated, role)
  if (isAuthenticated && role === "ROLE_ADMIN") {
    return <Redirect to="/dashboard" />;
  } else if (isAuthenticated && role === "ROLE_USER") {
    return <Redirect to={`/users/${employeeId}/inventories`} />;
  }
  return null;
};

export default Redirector;
