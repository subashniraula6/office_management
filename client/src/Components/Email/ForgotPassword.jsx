import React, { useRef, useState } from "react";
import { Form } from "react-bootstrap";
import "./ForgotPassword.css";
import { Button } from "@material-ui/core";
import axios from "../utils/axios";
import { Link, useHistory } from "react-router-dom";
import { notify } from "../utils/toast";

const ForgotPassword = () => {
  const [errors, setErrors] = useState({});
  const emailRef = useRef();
  const history = useHistory();
  function handleSubmit(e) {
    e.preventDefault();
    axios
      .post(
        "/forgotpassword",
        JSON.stringify({ email: emailRef.current.value })
      )
      .then((res) => {
        history.push("/login");
        notify("success", "Verification link is send to your email");
      })
      .catch((err) => setErrors(err.response?.data?.errors));
  }
  return (
    <div className="main-wrapper">
      <label className="text-center h5">Forgot password</label>
      <div className="formInput">
        <Form onSubmit={handleSubmit}>
          <Form.Label>Email address</Form.Label>
          <Form.Control ref={emailRef} placeholder="Enter your email address" />
          <Form.Text className="text-danger">{errors?.email || null}</Form.Text>
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            className="my-5"
          >
            Submit
          </Button>
          <Link to="/login" className='link'>
            <Button
              variant="outlined"
              color="secondary"
              style={{margin: '5px 5px'}}
            >
              Cancel
            </Button>
          </Link>
        </Form>
      </div>
    </div>
  );
};

export default ForgotPassword;
