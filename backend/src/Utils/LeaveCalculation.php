<?php

namespace App\Utils;

use App\Entity\LeaveType;
use App\Entity\PublicHoliday;
use App\Entity\User;
use DateInterval;
use DatePeriod;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use PDO;

trait LeaveCalculation
{


    public function getHolidayDate()
    {

        $con = $this->em->getConnection();
        $sql = 'SELECT public_holiday.holiday_date FROM public_holiday WHERE public_holiday.is_deleted = false';
        $stmt = $con->prepare($sql);
        $result = $stmt->executeQuery();
        $holidayDates = $result->fetchAllAssociative();
        $arrayOfHolidayDate = array_map(function ($a) {

            return $a['holiday_date'];
        }, $holidayDates);
        return $arrayOfHolidayDate;
    }

    public function calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival)
    {

        $dateOfLeave = new \DateTime($dateOfLeave);
        $dateOfArrival = new \DateTime($dateOfArrival);
        $leaveTime = new \DateTime($leaveTime);
        $arrivalTime = new \DateTime($arrivalTime);
        $holidayArray = $this->getHolidayDate();


        for ($index = 0, $count = 0; $index < count($holidayArray); $index++) {
            $holidayDate = new \DateTime($holidayArray[$index]);
            if ($holidayDate->format('w') != "0" && $holidayDate->format('w') != "6") {
                if ($holidayDate > $dateOfLeave && $holidayDate < $dateOfArrival) {
                    $count++;
                }
            }
            $holidayCount = $count;
        }

        $interval = $dateOfLeave->diff($dateOfArrival);
        $DOL = clone $dateOfLeave;
        for ($i = 0, $weekendCount = 0; $i < $interval->days; $i++) {

            $DOL->modify('+1 day');
            $weekday = $DOL->format('w');

            if ($weekday == '0' || $weekday == '6') {
                $weekendCount++;
            }
        }



        if ($dateOfLeave != $dateOfArrival && $leaveTime == $arrivalTime) {

            $leave = $dateOfLeave->format('Y-m-d');
            $arrival = $dateOfArrival->format('Y-m-d');

            $leave = date_create($leave);
            $arrival = date_create($arrival);
            $duration = $leave->diff($arrival);
            $durationHour = (((int)$duration->days - $weekendCount) * 8) - ($holidayCount * 8);

            return $durationHour;
        } elseif ($dateOfLeave == $dateOfArrival && $leaveTime != $arrivalTime) {
            $halfTimeStart = date_create("13:00");
            $halfTimeEnd = date_create("14:00");


            $leave = $leaveTime->format('H:i:s');
            $arrival = $arrivalTime->format('H:i:s');
            $duration = date_create($leave)->diff(date_create($arrival));
            if (date_create($leave) <= $halfTimeStart && date_create($arrival) >= $halfTimeEnd) {
                $durationHour = (int)$duration->format('%h') + floatval($duration->format('%i') / 60) - 1;
            } else {
                $durationHour = (int)$duration->format('%h') + floatval($duration->format('%i') / 60);
            }

            return $durationHour;
        } else {
            $mergeLeave = new \DateTime($dateOfLeave->format('Y-m-d') . ' ' . $leaveTime->format('H:i:s'));
            $mergeArrival = new \DateTime($dateOfArrival->format('Y-m-d') . ' ' . $arrivalTime->format('H:i:s'));

            $duration = $mergeArrival->diff($mergeLeave);
            if ($duration->h > 16) {
                $setHour = $duration->h - 16;
            } else {
                $setHour = $duration->h;
            }

            $setDay = $duration->days - $holidayCount - $weekendCount;

            $durationHour =  $setDay * 8 + $setHour + $duration->i / 60;

            if ($leaveTime > date_create("13:00") && $arrivalTime < date_create("13:00")) {
                return round(($durationHour + 1), 3);
            } elseif ($leaveTime < date_create("13:00") && $arrivalTime > date_create("13:00")) {
                return round(($durationHour - 1), 3);
            } else {
                return round($durationHour, 3);
            }
        }
    }


    public function calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival)
    {
        $dateOfLeave = new \DateTime($dateOfLeave);
        $dateOfArrival = new \DateTime($dateOfArrival);
        $leaveTime = new \DateTime($leaveTime);
        $arrivalTime = new \DateTime($arrivalTime);
        $holidayArray = $this->getHolidayDate();
        for ($index = 0, $count = 0; $index < count($holidayArray); $index++) {
            $holidayDate = new \DateTime($holidayArray[$index]);
            if ($holidayDate->format('w') != "0" && $holidayDate->format('w') != "6") {
                if ($holidayDate > $dateOfLeave && $holidayDate < $dateOfArrival) {
                    $count++;
                }
            }

            $holidayCount = $count;
        }



        $interval = $dateOfLeave->diff($dateOfArrival);
        $DOL = clone $dateOfLeave;
        for ($i = 0, $weekendCount = 0; $i < $interval->days; $i++) {
            $DOL->modify('+1 day');

            $weekday = $DOL->format('w');

            if ($weekday == '0' || $weekday == '6') {
                $weekendCount++;
            }
        }
        // dd($holidayCount, $weekendCount);

        if ($dateOfLeave != $dateOfArrival && $leaveTime == $arrivalTime) {

            $leave = $dateOfLeave->format('Y-m-d');
            $arrival = $dateOfArrival->format('Y-m-d');

            $leave = date_create($leave);
            $arrival = date_create($arrival);


            $duration = $leave->diff($arrival);
            // dd($duration);

            $durationDay = ((int)$duration->days - $weekendCount) - $holidayCount;

            return $durationDay;
        } elseif ($dateOfLeave == $dateOfArrival && $leaveTime != $arrivalTime) {
            $halfTimeStart = date_create("13:00");
            $halfTimeEnd = date_create("14:00");


            $leave = $leaveTime->format('H:i:s');
            $arrival = $arrivalTime->format('H:i:s');
            $duration = date_create($leave)->diff(date_create($arrival));
            if (date_create($leave) <= $halfTimeStart && date_create($arrival) >= $halfTimeEnd) {
                $durationDay = ((int)$duration->format('%h') + floatval($duration->format('%i') / 60) - 1) / 8;
            } else {
                $durationDay = ((int)$duration->format('%h') + floatval($duration->format('%i') / 60)) / 8;
            }

            return $durationDay;
        } else {
            $mergeLeave = new \DateTime($dateOfLeave->format('Y-m-d') . ' ' . $leaveTime->format('H:i:s'));
            $mergeArrival = new \DateTime($dateOfArrival->format('Y-m-d') . ' ' . $arrivalTime->format('H:i:s'));

            $duration = $mergeArrival->diff($mergeLeave);
            if ($duration->h > 16) {
                $setHour = $duration->h - 16;
            } else {
                $setHour = $duration->h;
            }

            $setDay = $duration->days - $holidayCount - $weekendCount;

            $durationDay =  $setDay * 8 + $setHour + $duration->i / 60;
            if ($leaveTime > date_create("13:00") && $arrivalTime < date_create("13:00")) {
                return round((($durationDay + 1) / 8), 3);
            } elseif ($leaveTime < date_create("13:00") && $arrivalTime > date_create("13:00")) {
                return round((($durationDay - 1) / 8), 3);
            } else {
                return round(($durationDay / 8), 3);
            }
        }
    }

    public function findCurrentFiscalYear()
    {
        $month = date('m');
        $year = date('Y');
        $fiscalMonth = 7;
        if ($month > 6) {
            $startDate = new \DateTime($year . '-' . $fiscalMonth . '-01');
            $endDate = clone $startDate;
            $endDate->modify('+1 year');
            $endDate->modify('-1 day');
        } else {
            $startDate = new \DateTime($year . '-' . $fiscalMonth . '-01');
            $endDate = clone $startDate;
            $startDate->modify('-1 year');
            $endDate->modify('-1 day');
        }
        return array('startDate' => $startDate, 'endDate' => $endDate);
    }

    public function findCustomFiscalYear($startYear, $endYear)
    {
        $startMonth = 7;
        $endMonth = 6;
        $startDate = new \DateTime($startYear . '-' . $startMonth . '-01');
        $endDate = new \DateTime($endYear . '-' . $endMonth . '-30');

        return array('startDate' => $startDate, 'endDate' => $endDate);
    }

    public function findLeaveStatOfUser($id, $DOL, $userId, $requestId)
    {
        $month = (int)$DOL->format('m');
        $year = (int)$DOL->format('Y');
        if ($month < 7) {
            $FSY = date_create($year - 1 . '-' . 07 . '-' . 01);
            $FEY = date_create($year . '-' . 06 . '-' . 30);
        } else {
            $FSY = date_create($year . '-' . 07 . '-' . 01);
            $FEY = date_create($year + 1 . '-' . 06 . '-' . 30);
        }


        $probationEndAt =  $this->em->getRepository(User::class)->findOneBy(['id' => $userId])->getProbationEndAt();
        $afterProbation = date_create($probationEndAt);

        $con = $this->getDoctrine()->getConnection();
        $startDate = $FSY->format("Y-m-d");
        $endDate = $FEY->format("Y-m-d");
        $isYearlyRenewed = $this->em->getRepository(LeaveType::class)->findOneBy(['id' => $id])->getIsYearlyRenewed();
        if ($isYearlyRenewed == true) {
            $sql = 'SELECT
            leave_request.date_of_leave,
            leave_request.date_of_arrival,
            leave_request.leave_time,
            leave_request.arrival_time,
            leave_type.leave_type_name,
            leave_type.leave_type_total_days,
            leave_type.is_payable,
            leave_type.is_yearly_renewed
            FROM
                leave_request
            INNER JOIN user ON
                leave_request.user_id = user.id
            INNER JOIN leave_type ON
                leave_request.leave_type_id = leave_type.id
            INNER JOIN leave_status ON
                leave_request.leave_status_id = leave_status.id
            WHERE
            user.id = :userId
            AND leave_type.id = :id
            AND leave_request.id != :requestId
            AND leave_request.date_of_arrival >= :startDate
            AND leave_request.date_of_leave >= :startDate
            AND leave_request.date_of_arrival <= :endDate
            AND leave_request.date_of_leave <=:endDate
            AND leave_status.name = "Approved"';
            $stmt = $con->prepare($sql);
            $params = array("id" => $id, "userId" => $userId, "startDate" => $startDate, "endDate" => $endDate, "requestId" => $requestId);
        } else {
            $sql = 'SELECT
            leave_request.date_of_leave,
            leave_request.date_of_arrival,
            leave_request.leave_time,
            leave_request.arrival_time,
            leave_type.leave_type_name,
            leave_type.leave_type_total_days,
            leave_type.is_payable,
            leave_type.is_yearly_renewed
            FROM
                leave_request
            INNER JOIN user ON
                leave_request.user_id = user.id
            INNER JOIN leave_type ON
                leave_request.leave_type_id = leave_type.id
            INNER JOIN leave_status ON
                leave_request.leave_status_id = leave_status.id
            WHERE
            user.id = :userId
            AND leave_request.id != :requestId
            AND leave_type.id = :id
            AND leave_status.name = "Approved"';
            $stmt = $con->prepare($sql);
            $params = array("id" => $id, "userId" => $userId, "requestId" => $requestId);
        }

        $result = $stmt->executeQuery($params);
        $leaveStat = $result->fetchAllAssociative();
        if ($leaveStat) {
            $totalHour = 0;
            $totalDay = 0;

            foreach ($leaveStat as $ls) {

                $leaveTime = $ls['leave_time'];
                $arrivalTime = $ls['arrival_time'];
                $dateOfArrival = $ls['date_of_arrival'];
                $dateOfLeave = $ls['date_of_leave'];
                $durationHour = $this->calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                $durationDay = $this->calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                $totalHour = $durationHour + $totalHour;
                $totalDay = $durationDay + $totalDay;
                $leaveTypeName = $ls['leave_type_name'];
                $isPayable = (bool)$ls['is_payable'];
                $isYearlyRenew = (bool)$ls['is_yearly_renewed'];
            }

            $leaveTotal =  $this->em->getRepository(LeaveType::class)->findOneBy(['id' => $id])->getLeaveTypeTotalDays();




            if ($afterProbation > $FSY && $afterProbation < $FEY) {
                $duration = $afterProbation->diff($FEY);

                // $usedDay = $duration->m * 0.5 + $totalDay;
                // $usedHour = $duration->m * 4 + $totalHour;
                $leaveTotalDays = $duration->m * ($leaveTotal / 12);
                $leaveTotalHours = $leaveTotalDays * 8;
                $remainingDay =  $leaveTotalDays - $totalDay;
                $remainingHour = $leaveTotalHours - $totalHour;
            } else {
                $leaveTotalDays = $leaveTotal;
                $leaveTotalHours = $leaveTotal * 8;
                $remainingDay =  $leaveTotalDays - $totalDay;
                $remainingHour = $leaveTotalHours - $totalHour;
            }

            $arr_data = array(
                'usedDay' => round($totalDay, 3), 'usedHour' => round($totalHour, 3),
                'leaveTypeName' => $leaveTypeName, 'remainingDay' => $remainingDay,
                'remainingHour' => $remainingHour, 'totalDay' => $leaveTotalDays,
                'totalHour' => $leaveTotalHours
            );




            return $arr_data;
        } else {
            $sql2 = 'SELECT * FROM leave_type WHERE leave_type.id=:id';
            $stmt2 = $con->prepare($sql2);
            $stmt2->bindParam("id", $id, PDO::PARAM_INT);
            $leaveType = $stmt2->executeQuery()->fetchAllAssociative()[0];
            if ($afterProbation > $FSY && $afterProbation < $FEY) {
                $duration = $afterProbation->diff($FEY);

                $leaveTotalDays =  $duration->m * ($leaveType['leave_type_total_days'] / 12);
                $leaveTotalHours = $leaveTotalDays * 8;
            } else {
                $leaveTotalDays = $leaveType['leave_type_total_days'];
                $leaveTotalHours = $leaveType['leave_type_total_days'] * 8;
            }

            $arr_data = array(
                'usedDay' => 0, 'usedHour' => 0,
                'leaveTypeName' => $leaveType['leave_type_name'], 'remainingDay' =>  $leaveTotalDays,
                'remainingHour' =>  $leaveTotalHours, 'totalDay' =>  $leaveTotalDays,
                'totalHour' => $leaveTotalHours
            );


            return $arr_data;
        }
    }

    public function getDatesFromRange($dateOfLeave, $dateOfArrival)
    {
        $range = array();
        $dateOfLeave = new DateTime($dateOfLeave);
        $dateOfArrival = new DateTime($dateOfArrival);
        $diff = $dateOfLeave->diff($dateOfArrival);
        if ($diff->days == 0) {
            array_push($range, $dateOfLeave->format('Y-m-d'));
        } else {
            for ($index = 0; $index < $diff->days; $index++) {
                array_push($range, $dateOfLeave->format('Y-m-d'));
                $dateOfLeave->modify("+1 days");
            }
        }


        return $range;
    }
}
