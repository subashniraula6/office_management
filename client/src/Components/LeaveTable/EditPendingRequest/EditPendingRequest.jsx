import React from "react"
import Container from "@material-ui/core/Container"
import Card from "@material-ui/core/Card"
import Button from "@material-ui/core/Button"
import Box from "@material-ui/core/Box"
import { useHistory, useParams } from "react-router-dom"
import axios from "../../utils/axios"
import { IconButton } from "@material-ui/core"
import CancelIcon from "@material-ui/icons/Cancel"
import { notify } from "../../utils/toast"

export default function EditPendingRequest() {
	const history = useHistory()
	const editId = useParams()
	const [leaveDetails, setLeaveDetails] = React.useState({
		arrivalTime: "",
		dateOfArrival: "",
		dateOfLeave: "",
		leaveDescription: "",
		leaveMode: "",
		leaveTime: "",
		leaveTypeName: "",
		periodOfDay: "",
	})
	const [leaveTypeOptions, setLeaveTypeOptions] = React.useState([])

	React.useEffect(() => {
		axios
			.get(`api/leave/myLeaveDetails/edit/${editId.id}`)
			.then((res) => {
				console.log(res.data)
				res.data.map((ld) =>
					setLeaveDetails({
						arrivalTime: ld.arrival_time,
						dateOfArrival: ld.date_of_arrival,
						dateOfLeave: ld.date_of_leave,

						leaveDescription: ld.leave_description,
						leaveMode: ld.leave_mode,
						leaveTime: ld.leave_time,
						leaveTypeName: ld.leaveId,
						periodOfDay: ld.period_of_day,
						userId: ld.userId,
					})
				)

				return axios.get("api/leave/type")
			})
			.then((res) => setLeaveTypeOptions(res.data.data))
			.catch((err) => console.log(err))
	}, [])

	const handleChange = (e) => {
		setLeaveDetails({
			...leaveDetails,
			[e.target.name]: e.target.value,
		})
	}
	const handleSubmit = (e) => {
		e.preventDefault()

		for (var key in leaveDetails) {
			if (leaveDetails["leaveMode"] === "halfDay") {
				if (leaveDetails["periodOfDay"] === "firstHalf") {
					leaveDetails["leaveTime"] = "09:00"
					leaveDetails["arrivalTime"] = "13:00"
				}
				if (leaveDetails["periodOfDay"] === "secondHalf") {
					leaveDetails["leaveTime"] = "14:00"
					leaveDetails["arrivalTime"] = "18:00"
				}
				leaveDetails["dateOfArrival"] = leaveDetails.dateOfLeave
			}
			if (leaveDetails["leaveMode"] === "fullDay") {
				leaveDetails["leaveTime"] = "09:00"
				leaveDetails["arrivalTime"] = "09:00"
			}
			leaveDetails["declineMessage"] = ""
			// formData.append(key, leaveDetails[key])
		}
		// for (var pair of formData.entries()) {
		// 	console.log(pair[0] + ", " + pair[1])
		// }

		axios
			.put(`api/edit/leaveRequest/${editId.id}`, leaveDetails)
			.then((res) => {
				console.log(res.data)
				notify("success", "Successfully Edited")
				history.push("/leave/myLeaveDetails")
			})
			.catch((err) => console.log(err))
	}
	return (
		<div>
			<Container style={{ background: "#fff3e0", padding: "2%" }}>
				<div className="leaveEditFormHeader">
					<Card style={{ background: "#ff6600", height: "50px" }}>
						<h5
							style={{ color: "white", marginTop: "1%", textAlign: "center" }}
						>
							Edit Leave Request
						</h5>
					</Card>
				</div>
				{console.log(leaveDetails)}
				<form onSubmit={handleSubmit}>
					<div className="row">
						<div className="col">
							<label htmlFor="leaveType">Choose Leave Type</label>
							<select
								name="leaveTypeName"
								className="custom-select"
								id="leaveType"
								value={leaveDetails.leaveTypeName}
								required
								onChange={handleChange}
							>
								{leaveTypeOptions.map((lto) => (
									<option value={lto.id}>{lto.leave_type_name}</option>
								))}
							</select>
						</div>
						<div className="col"></div>
					</div>
					<br />
					<div className="row">
						<div className="col">
							<label htmlFor="leaveMode">Leave Mode:</label>
							<input
								type="radio"
								id="half_day"
								name="leaveMode"
								value="halfDay"
								required
								checked={leaveDetails.leaveMode === "halfDay"}
								onChange={handleChange}
							/>
							<label htmlFor="half_day">Half Day</label>
							<input
								type="radio"
								id="full_day"
								name="leaveMode"
								value="fullDay"
								checked={leaveDetails.leaveMode === "fullDay"}
								onChange={handleChange}
							/>
							<label htmlFor="full_day">Full Day</label>
							<input
								type="radio"
								id="custom"
								name="leaveMode"
								value="custom"
								checked={leaveDetails.leaveMode === "custom"}
								onChange={handleChange}
							/>
							<label htmlFor="custom">Custom</label>
						</div>

						{leaveDetails.leaveMode == "halfDay" && (
							<div className="col">
								<label htmlFor="period_of_day">Period of Day:</label>
								<input
									type="radio"
									name="periodOfDay"
									value="firstHalf"
									required
									checked={leaveDetails.periodOfDay === "firstHalf"}
									onChange={handleChange}
								/>
								<label htmlFor="first_half">First Half</label>
								<input
									type="radio"
									name="periodOfDay"
									value="secondHalf"
									checked={leaveDetails.periodOfDay === "secondHalf"}
									onChange={handleChange}
								/>
								<label htmlFor="second_half">Second Half</label>
							</div>
						)}
					</div>
					<br />
					<div className="row">
						<div className="row">
							<div className="col-6">
								<label>Date of Leave</label>
								<input
									type="date"
									className="form-control"
									name="dateOfLeave"
									id="dateOfLeave"
									value={leaveDetails.dateOfLeave}
									required
									onChange={handleChange}
								/>
							</div>

							{leaveDetails.leaveMode === "custom" && (
								<div className="col-6">
									<label htmlFor="leaveTime">Leave Time</label>
									<input
										className="form-control"
										type="time"
										name="leaveTime"
										id="leaveTime"
										required
										value={leaveDetails.leaveTime}
										min="09:00"
										max="18:00"
										onChange={handleChange}
									/>
								</div>
							)}
						</div>

						<div className="row">
							{leaveDetails.leaveMode !== "halfDay" && (
								<div className="col-6">
									<label>Date of Arrival</label>
									<input
										type="date"
										className="form-control"
										name="dateOfArrival"
										id="dateOfArrival"
										value={leaveDetails.dateOfArrival}
										required
										onChange={handleChange}
									/>
								</div>
							)}
							{leaveDetails.leaveMode === "custom" && (
								<div className="col-6">
									<label htmlFor="arrivalTime">Arrival Time</label>
									<input
										className="form-control"
										type="time"
										name="arrivalTime"
										id="arrivalTime"
										value={leaveDetails.arrivalTime}
										required
										onChange={handleChange}
									/>
								</div>
							)}
						</div>
					</div>
					<br />
					<div className="row">
						<div className="col-6">
							<label>Leave Description</label>
							<br />
							<textarea
								name="leaveDescription"
								id="leaveDescription"
								cols="35"
								rows="3"
								value={leaveDetails.leaveDescription}
								required
								onChange={handleChange}
							/>
						</div>
					</div>
					<br />
					<div className="row">
						<div className="col">
							<Button
								type="submit"
								style={{ width: "40%", color: "white" }}
								variant="contained"
								color="primary"
							>
								Edit
							</Button>
						</div>
						<div className="col">
							<Button
								onClick={() => history.push("/leave/myLeaveDetails")}
								style={{ width: "40%", float: "right" }}
								variant="contained"
								color="secondary"
							>
								Cancel
							</Button>
						</div>
					</div>
				</form>
			</Container>
		</div>
	)
}
