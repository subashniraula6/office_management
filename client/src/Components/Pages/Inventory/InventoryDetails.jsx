import { useState, useEffect } from "react"
import { Box, Button, Card, CardHeader, Divider } from "@material-ui/core"
import { Filter, Note } from "@material-ui/icons"
import { Link, useHistory, useParams } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import Spinner from "../../Spinner/Spinner"
import { updateInventory } from "../../Redux/actions/inventoryactions"
import FormInput from "../../Common/Input/FormInput"
import BuildIcon from "@material-ui/icons/Build"
import { Row, Col } from "react-bootstrap"
import { Alert } from "react-bootstrap"
import axios from "../../utils/axios"
import { notify } from "../../utils/toast"

const InventoryDetails = (props) => {
	const history = useHistory()
	const params = useParams()
	const { inventoryItem: storedInventory } = props
	let inventoryItem = JSON.parse(JSON.stringify(storedInventory))
	let previousInventory
	if (inventoryItem) {
		previousInventory = {
			name: inventoryItem?.name || "",
			brand: inventoryItem?.brand || "",
			model: inventoryItem?.model || "",
			cost: inventoryItem?.cost || 0,
			procurredAt: inventoryItem?.procurredAt || "",
			category: inventoryItem?.category.name || "",
			inventoryId: inventoryItem?.inventoryId || "",
			notes: inventoryItem?.notes || "",
			requiresServicing: inventoryItem?.requiresServicing || false,
			servicingDuration: inventoryItem?.servicings?.find(
				(s) => s.isCurrentServicing === true
			)?.durationInMonth,
		}
	}
	const [categories, setCategories] = useState([])
	const dispatch = useDispatch()
	const [inventory, setInventory] = useState({
		name: "",
		brand: "",
		model: "",
		cost: 0,
		procurredAt: "",
		category: "",
		inventoryId: "",
		description: "",
		notes: "",
		requiresServicing: "",
		servicingDuration: "",
	})
	const errors = useSelector((store) => store.inventoryReducer.errors) || {}
	useEffect(() => {
		axios
			.get("/api/categories")
			.then((res) => {
				setCategories(res.data.result)
			})
			.catch((err) => console.log(err))
	}, [])
	useEffect(() => {
		setInventory(previousInventory)
		// eslint-disable-next-line
	}, [])
	function handleChange(e) {
		const { name, value } = e.target
		setInventory({ ...inventory, [name]: value })
	}
	function checkUpdated(inventory, previousInventory) {
		//Check if updated
		let updated = false
		for (let key of Object.keys(inventory)) {
			if (
				previousInventory[key] === inventory[key] ||
				(!previousInventory[key] && inventory[key] === "")
			) {
				updated = false
			} else {
				updated = true
				break
			}
		}
		return updated
	}
	function handleSubmit(e) {
		e.preventDefault()
		const updated = checkUpdated(inventory, previousInventory)
		if (updated) {
			dispatch(updateInventory(params.id, inventory, history))
		} else {
			notify("error", "No changes made!")
		}
	}
	if (!inventoryItem) return <Spinner />
	return (
		<>
			{inventoryItem.isDisposed && (
				<Alert variant="danger">
					<h4>Inventory is disposed</h4>
				</Alert>
			)}
			<Box component="form" autoComplete="off" onSubmit={handleSubmit}>
				<fieldset disabled={inventoryItem.isDisposed}>
					<Card style={{ boxShadow: "none", padding: "0 10px" }}>
						<CardHeader
							subheader="The information can be edited"
							title="Inventory Profile"
							style={{ padding: "0" }}
						/>
						<Divider />
						<Row>
							<FormInput
								type="text"
								label="Inventory Name"
								name="name"
								value={inventory.name}
								handleChange={handleChange}
								feedback={errors?.name || null}
							/>
							<FormInput
								type="text"
								label="Brand Name"
								name="brand"
								value={inventory.brand}
								handleChange={handleChange}
							/>
						</Row>
						<Row>
							<FormInput
								label="Model Name"
								type="text"
								name="model"
								value={inventory.model}
								handleChange={handleChange}
							/>
							<FormInput
								label="Inventory Id"
								type="text"
								name="inventoryId"
								value={inventory.inventoryId}
								handleChange={handleChange}
								required
								feedback={errors?.inventoryId || null}
							/>
						</Row>

						<Row>
							<div className="my-2 col-lg-4">
								<div className="input-group-prepend">
									<label className="input-group-text">
										<Filter />
										Category <small className="text-danger">*</small>
									</label>
								</div>
								<select
									className="custom-select"
									id="device"
									name="category"
									value={inventory.category}
									onChange={handleChange}
									required
								>
									{categories.map((cat) => (
										<option key={cat.id} value={cat.name}>
											{cat.name}
										</option>
									))}
								</select>
							</div>
						</Row>
						<div>
							<h5>Procurement details</h5>
						</div>
						<Row>
							<FormInput
								type="number"
								label="Procurement cost (Rs.)"
								name="cost"
								value={inventory.cost}
								handleChange={handleChange}
							/>
							<FormInput
								type="date"
								required
								label="Procurement Date"
								name="procurredAt"
								value={inventory.procurredAt}
								handleChange={handleChange}
							/>
						</Row>
						<div>
							<h5 className="mt-3">Other details</h5>
						</div>
						<Row>
							<Col xl={5}>
								<div className="input-group my-2">
									<label className="input-group-text">
										<BuildIcon />
										Servicing Duration <small className="text-danger">*</small>
									</label>
								</div>
								<select
									className="form-control"
									name="servicingDuration"
									value={inventory.servicingDuration}
									onChange={handleChange}
									required
									disabled={!inventory.requiresServicing}
								>
									<option value={6}>6 months</option>
									<option value={9}>9 months</option>
									<option value={12}>12 months</option>
									<option value={15}>15 months</option>
									<option value={24}>24 months</option>
								</select>
							</Col>
							<Col xl={7}>
								<div className="form-group w-50 my-2">
									<label>
										<Note />
										Notes <small className="text-danger">*</small>
									</label>
									<textarea
										className="w-100"
										rows="3"
										name="notes"
										value={inventory.notes}
										onChange={handleChange}
									></textarea>
									{errors?.notes ? (
										<small className="text-danger">{errors.notes}</small>
									) : null}
								</div>
							</Col>
						</Row>
						<Divider />
						<Box
							sx={{
								display: "flex",
								justifyContent: "flex-start",
								p: 2,
							}}
						>
							<Button
								color="primary"
								variant="outlined"
								type="submit"
								disabled={inventoryItem.isDisposed}
							>
								Save details
							</Button>
							<Link
								to="/dashboard/inventories"
								style={{ textDecoration: "none" }}
							>
								<Button
									color="secondary"
									variant="outlined"
									style={{ marginLeft: "3px" }}
								>
									Back
								</Button>
							</Link>
						</Box>
					</Card>
				</fieldset>
			</Box>
		</>
	)
}

export default InventoryDetails
