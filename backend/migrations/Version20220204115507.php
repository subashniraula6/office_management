<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220204115507 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE notify_leave (id INT AUTO_INCREMENT NOT NULL, leave_request_id INT NOT NULL, seen_dates VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_B9C6F8B1F2E1C15D (leave_request_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE notify_leave ADD CONSTRAINT FK_B9C6F8B1F2E1C15D FOREIGN KEY (leave_request_id) REFERENCES leave_request (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE notify_leave');
    }
}
