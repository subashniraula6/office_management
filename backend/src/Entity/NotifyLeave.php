<?php

namespace App\Entity;

use App\Repository\NotifyLeaveRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NotifyLeaveRepository::class)
 */
class NotifyLeave
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=LeaveRequest::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $leaveRequest;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $seenDates;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLeaveRequest(): ?LeaveRequest
    {
        return $this->leaveRequest;
    }

    public function setLeaveRequest(LeaveRequest $leaveRequest): self
    {
        $this->leaveRequest = $leaveRequest;

        return $this;
    }

    public function getSeenDates(): ?string
    {
        return $this->seenDates;
    }

    public function setSeenDates(?string $seenDates): self
    {
        $this->seenDates = $seenDates;

        return $this;
    }
}
