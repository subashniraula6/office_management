<?php

namespace App\Repository;

use App\Entity\LeaveRequest;
use App\Entity\User;
use App\Entity\LeaveType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use PDO;
use App\Utils\LeaveCalculation;



/**
 * @method LeaveRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeaveRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeaveRequest[]    findAll()
 * @method LeaveRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeaveRequestRepository extends ServiceEntityRepository
{
    use LeaveCalculation;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeaveRequest::class);
    }

    // /**
    //  * @return LeaveRequest[] Returns an array of LeaveRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LeaveRequest
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findLeaveDetailsUser($rowsPerPage, $offset, $userId, $startFindDate, $endFindDate)
    {

        $con = $this->getEntityManager()->getConnection();
        if ($startFindDate == "" && $endFindDate == "") {
            $sql = 'SELECT
            leave_request.id,
            leave_request.leave_description,
            leave_request.date_of_leave,
            leave_request.date_of_arrival,
            leave_request.decline_message,
            leave_request.period_of_day,
            leave_request.leave_mode,
            leave_request.leave_time,
            leave_request.arrival_time,
            leave_type.leave_type_name,
            leave_type.leave_type_total_days,
            leave_status.name
            FROM
                leave_request
            INNER JOIN 
                leave_type ON
                leave_request.leave_type_id = leave_type.id
            INNER JOIN leave_status ON
                leave_request.leave_status_id = leave_status.id
            INNER JOIN 
                user ON
                leave_request.user_id = user.id
            WHERE
                user.id = :id
            ORDER BY
                leave_request.id DESC
            LIMIT :offset,
            :rowsPerPage';
            $stmt = $con->prepare($sql);
        } else {
            $sql = 'SELECT
            leave_request.id,
            leave_request.leave_description,
            leave_request.date_of_leave,
            leave_request.date_of_arrival,
            leave_request.decline_message,
            leave_request.period_of_day,
            leave_request.leave_mode,
            leave_request.leave_time,
            leave_request.arrival_time,
            leave_type.leave_type_name,
            leave_type.leave_type_total_days,
            leave_status.name
            FROM
                leave_request
            INNER JOIN 
                leave_type ON
                leave_request.leave_type_id = leave_type.id
            INNER JOIN leave_status ON
                leave_request.leave_status_id = leave_status.id
            INNER JOIN 
                user ON
                leave_request.user_id = user.id
            WHERE
                user.id = :id
                AND leave_request.date_of_leave BETWEEN :startFindDate AND :endFindDate
            ORDER BY
                leave_request.id DESC
            LIMIT :offset,
            :rowsPerPage';
            $stmt = $con->prepare($sql);
            $stmt->bindParam('startFindDate', $startFindDate);
            $stmt->bindParam('endFindDate', $endFindDate);
        }
        $stmt->bindParam('id', $userId, PDO::PARAM_INT);
        $stmt->bindParam('offset', $offset, PDO::PARAM_INT);
        $stmt->bindParam('rowsPerPage', $rowsPerPage, PDO::PARAM_INT);


        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative();
    }



    public function findAllLeavePendingRequests($offset, $rowsPerPage)
    {
        $con = $this->getEntityManager()->getConnection();
        $sql = "SELECT
        leave_request.id,
        leave_request.leave_description,
        leave_request.date_of_leave,
        leave_request.leave_time,
        leave_request.arrival_time,
        leave_request.date_of_arrival,
        leave_request.decline_message,
        leave_type.leave_type_name,
        leave_type.leave_type_total_days,
        leave_status.name,
        user.first_name,
        user.last_name
        FROM
            leave_request
        JOIN leave_type ON
            leave_request.leave_type_id = leave_type.id
        JOIN leave_status ON
            leave_request.leave_status_id = leave_status.id
        JOIN user ON
            leave_request.user_id = user.id
        WHERE
            leave_status.name = 'Pending'
        ORDER BY
            leave_request.id DESC
                LIMIT :offset, :rowsPerPage";
        $stmt = $con->prepare($sql);
        $stmt->bindParam('offset', $offset, PDO::PARAM_INT);
        $stmt->bindParam('rowsPerPage', $rowsPerPage, PDO::PARAM_INT);
        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative();
    }

    public function findAllLeaveNoPendingRequests($offset, $rowsPerPage, $startFindDate, $endFindDate)
    {
        $con = $this->getEntityManager()->getConnection();
        if ($startFindDate == "" && $endFindDate == "") {
            $sql = "SELECT
        leave_request.id,
        leave_request.leave_description,
        leave_request.date_of_leave,
        leave_request.leave_time,
        leave_request.arrival_time,
        leave_request.date_of_arrival,
        leave_request.decline_message,
        leave_type.leave_type_name,
        leave_type.leave_type_total_days,
        leave_status.name,
        user.first_name,
        user.last_name
        FROM
            leave_request
        JOIN leave_type ON
            leave_request.leave_type_id = leave_type.id
        JOIN leave_status ON
            leave_request.leave_status_id = leave_status.id
        JOIN user ON
            leave_request.user_id = user.id
        WHERE
            leave_status.name != 'Pending'
        ORDER BY
            leave_request.id DESC
                LIMIT :offset, :rowsPerPage";
            $stmt = $con->prepare($sql);
        } else {
            $sql = "SELECT
        leave_request.id,
        leave_request.leave_description,
        leave_request.date_of_leave,
        leave_request.leave_time,
        leave_request.arrival_time,
        leave_request.date_of_arrival,
        leave_request.decline_message,
        leave_type.leave_type_name,
        leave_type.leave_type_total_days,
        leave_status.name,
        user.first_name,
        user.last_name
        FROM
            leave_request
        JOIN leave_type ON
            leave_request.leave_type_id = leave_type.id
        JOIN leave_status ON
            leave_request.leave_status_id = leave_status.id
        JOIN user ON
            leave_request.user_id = user.id
        WHERE
            leave_status.name != 'Pending'
            AND leave_request.date_of_leave BETWEEN :startFindDate AND :endFindDate
        ORDER BY
            leave_request.id DESC
                LIMIT :offset, :rowsPerPage";
            $stmt = $con->prepare($sql);
            $stmt->bindParam('startFindDate', $startFindDate);
            $stmt->bindParam('endFindDate', $endFindDate);
        }


        $stmt->bindParam('offset', $offset, PDO::PARAM_INT);
        $stmt->bindParam('rowsPerPage', $rowsPerPage, PDO::PARAM_INT);
        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative();
    }

    public function findLeaveRequest($id)
    {
        $con = $this->getEntityManager()->getConnection();
        $sql = "SELECT
        leave_request.id,
        leave_request.leave_description,
        leave_request.date_of_leave,
        leave_request.date_of_arrival,
        leave_request.decline_message,
        leave_request.period_of_day,
        leave_request.leave_mode,
        leave_request.leave_time,
        leave_request.arrival_time,
        leave_type.leave_type_name,
        leave_type.leave_type_total_days,
        leave_status.name,
        user.first_name,
        user.last_name,
        leave_documents.documents_path
        FROM
            leave_request
        INNER JOIN leave_type ON
            leave_request.leave_type_id = leave_type.id
        INNER JOIN leave_status ON
            leave_request.leave_status_id = leave_status.id
        INNER JOIN user ON
            leave_request.user_id = user.id
        LEFT JOIN leave_documents ON
            leave_documents.leave_request_id = leave_request.id
        WHERE
            (leave_request.id = :id)";
        $stmt = $con->prepare($sql);
        $params = array('id' => $id);
        $result = $stmt->executeQuery($params);
        return $result->fetchAllAssociative()[0];
    }

    public function findByTodaysLeave()
    {
        $con = $this->getEntityManager()->getConnection();
        $sql = "SELECT
        leave_request.id,
        leave_request.leave_description,
        leave_request.date_of_leave,
        leave_request.date_of_arrival,
        leave_request.decline_message,
        leave_request.period_of_day,
        leave_request.leave_mode,
        leave_request.leave_time,
        leave_request.arrival_time,
        leave_type.leave_type_name,
        leave_type.leave_type_total_days,
        leave_status.name,
        user.first_name,
        user.last_name,
        user_info.pp_image_path
        FROM
            leave_request
        INNER JOIN leave_type ON
            leave_request.leave_type_id = leave_type.id
        INNER JOIN leave_status ON
            leave_request.leave_status_id = leave_status.id
        INNER JOIN user ON
            leave_request.user_id = user.id
        LEFT JOIN user_info ON
            user.id = user_info.user_id
        WHERE
            CURDATE() >= leave_request.date_of_leave
        AND
        CASE
            WHEN leave_request.date_of_leave = leave_request.date_of_arrival THEN
                    CURDATE() <= leave_request.date_of_arrival
            ELSE CURDATE() < leave_request.date_of_arrival
        END
        AND 
                    leave_status.name = 'Approved'";
        $stmt = $con->prepare($sql);
        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative();
    }

    public function findLeaveDetailsBetweenYear($sfy, $efy, $leaveId, $userId)
    {
        $con = $this->getEntityManager()->getConnection();
        $sql = 'SELECT
        leave_request.date_of_leave,
        leave_request.date_of_arrival,
        leave_request.leave_time,
        leave_request.arrival_time,
        leave_type.leave_type_name,
        leave_type.leave_type_total_days,
        leave_type.is_payable,
        leave_type.is_yearly_renewed,
        user.joined_at,
        user.probation_end_at,
        user_info.pp_image_path,
        user.first_name,
        user.last_name,
        user_info.pp_image_path
        FROM
            leave_request
        INNER JOIN user ON
            leave_request.user_id = user.id
        LEFT JOIN user_info ON
            user.id = user_info.user_id
        INNER JOIN leave_type ON
            leave_request.leave_type_id = leave_type.id
        INNER JOIN leave_status ON
            leave_request.leave_status_id = leave_status.id
        WHERE
            (leave_request.date_of_leave >= :startYear AND leave_request.date_of_leave <=:endYear)
        AND
            (leave_request.date_of_arrival >=:startYear AND leave_request.date_of_arrival <=:endYear)        
        AND
         leave_type.id = :leaveId
        AND 
         user.id = :userId
        AND
        leave_status.name = "Approved"';
        $stmt = $con->prepare($sql);
        $params = array('startYear' => $sfy, 'endYear' => $efy, 'leaveId' => $leaveId, 'userId' => $userId);
        $result = $stmt->executeQuery($params)->fetchAllAssociative();

        return $result;
    }

    public function findLeaveDatesOfUser($userId, $requestId)
    {
        $con = $this->getEntityManager()->getConnection();
        $sql = "SELECT
        leave_request.date_of_leave,
        leave_request.date_of_arrival
        FROM
            leave_request
        WHERE
            leave_request.user_id = :userId
            AND
            leave_request.id != :requestId";
        $stmt = $con->prepare($sql);
        $params = array("userId" => $userId, "requestId" => $requestId);
        $result = $stmt->executeQuery($params);
        $result = $result->fetchAllAssociative();
        return $result;
    }
}
