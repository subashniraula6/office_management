import React from "react";
import { Paper } from "@material-ui/core";
import { Row, Col } from "react-bootstrap";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "10px",
    fontFamily: '"Poppins","sans-serif"',
    fontSize: "16px",
  },
  paper: {
    padding: "20px",
    height: "90px",
  },
}));
const CountPaper = ({ icon, label, value }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Paper elevation={3} className={classes.paper}>
        <Row className="align-items-center">
          <Col xs={4}>{icon}</Col>
          <Col xs={8}>
            <Row>
              <h5>{value}</h5>
            </Row>
            <Row>{label}</Row>
          </Col>
        </Row>
      </Paper>
    </div>
  );
};

export default CountPaper;
