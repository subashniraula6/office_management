<?php

namespace App\Entity;

use App\Repository\ServicingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ServicingRepository::class)
 */
class Servicing
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $serviceAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $durationInMonth;

    /**
     * @ORM\ManyToOne(targetEntity=Inventory::class, inversedBy="servicings")
     * @ORM\JoinColumn(nullable=false, name="inventory_id", referencedColumnName="id")
     */
    private $inventory;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCurrentServicing;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sentAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $returnedAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cost;


    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $remarks;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCompleted;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServiceAt(): ?string
    {
        return $this->serviceAt ? $this->serviceAt->format("F j, Y"): null;
    }

    public function setServiceAt(?\DateTime $serviceAt): self
    {
        $this->serviceAt = $serviceAt;

        return $this;
    }

    public function getDurationInMonth(): ?int
    {
        return $this->durationInMonth;
    }

    public function setDurationInMonth(int $durationInMonth): self
    {
        $this->durationInMonth = $durationInMonth;

        return $this;
    }

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): self
    {
        $this->inventory = $inventory;

        return $this;
    }

    public function getIsCurrentServicing(): ?bool
    {
        return $this->isCurrentServicing;
    }

    public function setIsCurrentServicing(bool $isCurrentServicing): self
    {
        $this->isCurrentServicing = $isCurrentServicing;

        return $this;
    }

    public function getSentAt(): ?string
    {
        return $this->sentAt ? $this->sentAt->format("F j, Y") : null;
    }

    public function setSentAt(?\DateTimeInterface $sentAt): self
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    public function getReturnedAt(): ?string
    {
        return $this->returnedAt ? $this->returnedAt->format("F j, Y") : null;
    }

    public function setReturnedAt(?\DateTimeInterface $returnedAt): self
    {
        $this->returnedAt = $returnedAt;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(?int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getRemarks(): ?string
    {
        return $this->remarks;
    }

    public function setRemarks(?string $remarks): self
    {
        $this->remarks = $remarks;

        return $this;
    }

    public function getIsCompleted(): ?bool
    {
        return $this->isCompleted;
    }

    public function setIsCompleted(bool $isCompleted): self
    {
        $this->isCompleted = $isCompleted;

        return $this;
    }

    public function getIsSent(): ?bool
    {
        return $this->isSent;
    }

    public function setIsSent(bool $isSent): self
    {
        $this->isSent = $isSent;

        return $this;
    }
}
