<?php

namespace App\Controller;

use App\Entity\Allocation;
use App\Entity\ContactRelation;
use App\Entity\Inventory;
use App\Entity\Role;
use App\Entity\User;
use Gedmo\Sluggable\Util\Urlizer;
use App\Entity\UserInfo;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Utils\ValidateUtils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use App\Services\Email;
use App\Services\CustomSerializer;
use App\Services\FileUploader;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class UserController extends AbstractController
{
    use ValidateUtils;
    use CustomSerializer;
    /**
     * @Route("/api/users", name="create_user", methods={"POST"})
     */
    public function createUser(Request $request, UserPasswordHasherInterface $encoder, Email $email)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
        $input = json_decode($request->getContent(), 1);


        //check if email already exists
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $input['email']]);
        if ($user) {
            $response = array(
                'code' => 401,
                'errors' => ['email' => 'email already exists'],
                'result' => null
            );
            return new JsonResponse($response, 401);
        }

        //check if employeeId already exists
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['employeeId' => $input['employeeId']]);
        if ($user) {
            $response = array(
                'code' => 401,
                'errors' => ['employeeId' => 'employee id already exists'],
                'result' => null
            );
            return new JsonResponse($response, 401);
        }
        $user = new User();
        $user->setEmail($input['email']);
        $user->setFirstName($input['firstName']);
        $user->setLastName($input['lastName']);
        $user->setCreatedAt(new \DateTimeImmutable('NOW'));
        $user->setEmployeeId($input['employeeId']);
        $user->setDateOfBirth(new \DateTime($input['dateOfBirth']));
        $user->setGender($input['gender']);
        $user->setAddedBy($this->getUser()->getFirstName() . ' ' . $this->getUser()->getLastName());
        $user->setJoinedAt(new DateTime($input['joinedAt']));
        $user->setHasLeft(false);
        $user->setPassword($encoder->hashPassword($user, 'wolfmatrix@123'));
        $user->setDepartment($input['department']);

        if ($input['probationEndAt'] !== "") {
            $user->setProbationEndAt(new \DateTime($input['probationEndAt']));
        }
        $em = $this->getDoctrine()->getManager();
        // Prevent creating new CEO
        if ($input['designation'] !== 'CEO') {
            $user->setDesignation($input['designation']);
        } else {
            $user->setDesignation('Engineer');
        }

        // set user info
        $userInfo = new UserInfo();
        $input['citizenshipNumber'] && $userInfo->setCitizenshipNumber($input['citizenshipNumber']);
        $input['panNumber'] && $userInfo->setPanNumber($input['panNumber']);
        $input['accountNumber'] && $userInfo->setAccountNumber($input['accountNumber']);
        $userInfo->setPhoneNumber($input['phoneNumber']);
        $input['temporaryAddress'] && $userInfo->setTemporaryAddress($input['temporaryAddress']);
        $input['permanentAddress'] && $userInfo->setPermanentAddress($input['permanentAddress']);
        $input['notes'] && $userInfo->setNotes($input['notes']);
        $userInfo->setUser($user);

        // Set contact relation
        if (
            $input['relation'] &&
            $input['relationPhoneNumber']
        ) {
            $relation = new ContactRelation();
            $relation->setRelation($input['relation']);
            $input['relationName'] && $relation->setName($input['relationName']);
            $relation->setPhoneNumber($input['relationPhoneNumber']);
            $user->setContactRelation($relation);
        }

        $roles = $this->getDoctrine()->getRepository(Role::class)->findAll();
        $check_role = '';
        foreach ($roles as $role) {
            if ($role->getName() === [$input['roles']]) {
                $check_role = $role;
            }
        }
        $user->setRoles($check_role);

        // Validation
        $err = $this->validateInput($user);
        $userInfoErr = $this->validateInput($userInfo);
        if (!empty($userInfoErr)) {
            $err['phoneNumber'] = $userInfoErr['phoneNumber'];
        }
        if (!empty($err)) {
            $response = array(
                'code' => 422,
                'errors' => $err,
                'result' => null
            );
            return new JsonResponse($response, 422);
        }

        $em->persist($userInfo);
        $em->persist($user);
        $em->flush();

        // Generate link
        $generated_token = uniqid();
        $portalurl = $this->getParameter('app.portalurl');
        $link =  "$portalurl/resetpassword/$generated_token";
        $currentDateTime = new \DateTime('NOW');
        $expires_at = $currentDateTime->add(new \DateInterval('PT1H'));

        $user->setRecoveryToken($generated_token);
        $user->setTokenExpiresAt($expires_at);

        $em->persist($user);
        $em->flush();

        // Send email
        $email->sendAccountCreationLink($input['email'], $link);

        return $this->fetchUser($user->getEmployeeId());
    }

    /**
     * @Route("/api/users/{id}/inventories", name="get_user_inventories", methods={"GET"})
     */
    public function getUserInventories($id, EntityManagerInterface $em)
    {
        $user = $em->getRepository(User::class)->findOneBy(['employeeId' => $id]);
        $allocations = $user->getAllocations()->toArray();
        $current_allocations = array_filter($allocations, function ($allocation) {
            return $allocation->getIsCurrentAllocation() === true;
        });
        $current_inventories = array_map(function ($allocation) {
            return $allocation->getInventory()->getInventoryId();
        }, $current_allocations);
        $inventories = $em->getRepository(Inventory::class)->findBy(['inventoryId' => $current_inventories]);
        $data = $this->serializeInventories($inventories);
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/api/users/{id}", name="edit_user", methods={"PUT"})
     */
    public function editUser($id, Request $request)
    {
        // Check if current user is admin
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['employeeId' => $id, 'hasLeft' => false]);
        $isLoginInUserCEO = $this->getUser()->getDesignation() === "CEO";

        // check if user is 'CEO'
        if ($isLoginInUserCEO === false) {
            if ($user->getDesignation() === 'CEO') {
                $response = array(
                    'code' => 401,
                    'errors' => 'unauthorized',
                    'result' => null
                );
                return new JsonResponse($response, 401);
            }
        }

        $parameters = json_decode($request->getContent(), true);

        //check if email already exists
        $checkUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $parameters['email']]);
        if ($checkUser && $checkUser !== $user) {
            $response = array(
                'code' => 401,
                'errors' => ['email' => 'email already exists'],
                'result' => null
            );
            return new JsonResponse($response, 401);
        }
        //check if employeeId already exists
        $checkUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(['employeeId' => $parameters['employeeId']]);
        if ($checkUser && $checkUser !== $user) {
            $response = array(
                'code' => 401,
                'errors' => ['employeeId' => 'employee id already exists'],
                'result' => null
            );
            return new JsonResponse($response, 401);
        }

        $user->setEmail($parameters['email']);
        $user->setFirstName($parameters['firstName']);
        $user->setLastName($parameters['lastName']);
        $user->setEmployeeId($parameters['employeeId']);
        $user->setDateOfBirth(new \DateTime($parameters['dateOfBirth']));
        $user->setGender($parameters['gender']);
        $user->setJoinedAt(new DateTime($parameters['joinedAt']));
        $user->setProbationEndAt(new DateTime($parameters['probationEndAt']));
        $user->setDepartment($parameters['department']);
        $em = $this->getDoctrine()->getManager();
        // Prevent creating new CEO
        if ($parameters['designation'] !== 'CEO') {
            $user->setDesignation($parameters['designation']);
        } else {
            $user->setDesignation('Engineer');
        }

        // set user info
        $userInfo = $user->getUserInfo();
        $parameters['citizenshipNumber'] && $userInfo->setCitizenshipNumber($parameters['citizenshipNumber']);
        $parameters['panNumber'] && $userInfo->setPanNumber($parameters['panNumber']);
        $parameters['accountNumber'] && $userInfo->setAccountNumber($parameters['accountNumber']);
        $userInfo->setPhoneNumber($parameters['phoneNumber']);
        $parameters['temporaryAddress'] && $userInfo->setTemporaryAddress($parameters['temporaryAddress']);
        $parameters['permanentAddress'] && $userInfo->setPermanentAddress($parameters['permanentAddress']);
        $parameters['notes'] && $userInfo->setNotes($parameters['notes']);
        $userInfo->setUser($user);
        $em->persist($userInfo);

        // Set contact relation
        if (
            $parameters['relation'] &&
            $parameters['relationPhoneNumber']
        ) {
            $relation = $user->getContactRelation();
            $relation->setRelation($parameters['relation']);
            $parameters['relationName'] && $relation->setName($parameters['relationName']);
            $relation->setPhoneNumber($parameters['relationPhoneNumber']);
            $user->setContactRelation($relation);
        }

        // CEO only can create another admin 
        $designation = $this->getUser()->getDesignation();
        if ($designation === 'CEO') {
            $role = $user->getRole();
            $role->setName([$parameters['roles']]);
            // $role->setUser($user);
            $user->setRoles($role);
            $em->persist($role);
        } else {
            $role = $user->getRole();
            $role->setName(['ROLE_USER']);
            $user->setRoles($role);
            $em->persist($role);
        }

        // Validation
        $err = $this->validateInput($user);
        $userInfoErr = $this->validateInput($userInfo);
        if (!empty($userInfoErr)) {
            $err['phoneNumber'] = $userInfoErr['phoneNumber'];
        }
        if (!empty($err)) {
            $response = array(
                'code' => 422,
                'errors' => $err,
                'result' => null
            );
            return new JsonResponse($response, 422);
        }

        $em->persist($user);
        $em->flush();
        return $this->fetchUser($user->getEmployeeId());
    }

    /**
     * @Route("/api/users", name="get_users", methods={"GET"})
     */
    public function getUsers(EntityManagerInterface $em)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        // total inventory count
        $query = $em->createQuery(
            'SELECT COUNT(u.id) 
            FROM APP\ENTITY\USER u'
        );
        $totalCount = $query->getResult();
        $totalCount = $totalCount[0][1];

        // pagination & query paramteters from api
        $page = 1;
        parse_str($_SERVER['QUERY_STRING'], $result);
        $per_page = $totalCount;
        if ($result && $result['per_page']) {
            $per_page = $result['per_page'];
        }
        if ($result && $result['page']) {
            $page = $result['page'];
        }
        $offset = $per_page * ($page - 1);

        // Filters
        $fullName = null;
        if (array_key_exists('fullName', $result)) {
            $fullName = $result['fullName'];
        }
        $designation = null;
        if (array_key_exists('designation', $result)) {
            $designation = $result['designation'];
        }
        // return filtered id's
        $sql = 'SELECT user.id 
                FROM `user`
                WHERE (user.first_name LIKE :fullName or user.last_name LIKE :fullName) 
                and user.designation LIKE :designation';

        $conn = $em->getConnection();
        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery([
            'fullName' => '%' . $fullName . '%',
            'designation' => '%' . $designation . '%',
        ]);
        $id_arr =  $result->fetchAllAssociative();

        // convert 2d array to 1d
        $id_arr = array_map(function ($d) {
            return $d['id'];
        }, $id_arr);

        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findBy(['id' => $id_arr], ['firstName' => 'ASC'], $per_page, $offset);
        if (empty($users)) {
            $response = array(
                'code' => 404,
                'errors' => 'No users',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        $data = $this->serializeUsers($users); // class to json

        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data),
            'page' => (int)$page,
            'totalCount' => $totalCount
        );
        return new JsonResponse($response, 200);
    }


    // Remove/Revive user
    /**
     * @Route("/api/admin/users/{id}/{action}", name="manage_user", methods={"PUT"})
     */
    public function manageUser($id, $action)
    {
        // Check if current user is admin
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['employeeId' => $id]);

        if (empty($user)) {
            $response = array(
                'code' => 404,
                'errors' => 'No user',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        if ($user->getDesignation() === 'CEO' || ($action !== 'remove' && $action !== 'revive')) {
            $response = array(
                'code' => 404,
                'errors' => 'Bad request',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }

        if ($action === 'remove') {
            if ($user->getHasLeft() === false) {
                $user->setHasLeft(true);
                $user->setLeftAt(new \DateTime('NOW'));

                //unallocate inventory
                $allocation = $this->getDoctrine()
                    ->getRepository(Allocation::class)
                    ->findOneBy(['user' => $user->getId(), 'isCurrentAllocation' => true]);
                if (!empty($allocation)) {
                    // set unallocated timestamp and remove current allocation pointer
                    $allocation->setUnallocatedAt(new DateTime('NOW'));
                    $allocation->setIsCurrentAllocation(false);
                    //Change inventory allocation status
                    $inventory = $allocation->getInventory();
                    $inventory->setIsAllocated(false);
                    $em->persist($allocation);
                    $em->persist($inventory);
                }
            }
        } else if ($action === 'revive') {
            if ($user->getHasLeft() === true) {
                $user->setHasLeft(false);
                $user->removeLeftAt();
            }
        }

        $em->persist($user);
        $em->flush();
        return $this->fetchUser($user->getEmployeeId());
    }

    /**
     * @Route("/api/users/{id}", name="get_user", methods={"GET"})
     */
    public function fetchUser($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['employeeId' => $id]);
        $roles = $this->getUser()->getRoles()[0];
        if (empty($user)) {
            $response = array(
                'code' => 404,
                'errors' => 'No user',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        if (!($roles === 'ROLE_ADMIN' || ($this->getUser() === $user))) {
            $response = array(
                'code' => 401,
                'errors' => 'unauthorized',
                'result' => null
            );
            return new JsonResponse($response, 401);
        }
        $data = $this->serializeUser($user);
        // dd($data);
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data)
        );

        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/forgotpassword", name="forgot_password", methods={"POST"})
     */
    public function forgotPassword(Request $request, Email $email)
    {
        $em = $this->getDoctrine()->getManager();
        $input = json_decode($request->getContent(), 1);
        $portalurl = $this->getParameter('app.portalurl');
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $input['email']]);

        if (empty($user)) {
            $response = array(
                'code' => 404,
                'errors' => ['email' => 'we couldn’t find that email. Please try again.'],
                'result' => null
            );
            return new JsonResponse($response, 404);
        }

        $generated_token = uniqid();
        $link =  "$portalurl/resetpassword/$generated_token";
        $currentDateTime = new \DateTime('NOW');
        $expires_at = $currentDateTime->add(new \DateInterval('PT1H'));

        $user->setRecoveryToken($generated_token);
        $user->setTokenExpiresAt($expires_at);

        $em->persist($user);
        $em->flush();

        //Send email
        $email->sendLink($input['email'], $link);

        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => 'Mail sent'
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/resetpassword/{token}", name="get_reset_password", methods={"GET"})
     */
    public function getResetPassword($token)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['recoveryToken' => $token]);
        if (empty($user)) {
            $response = array(
                'code' => 404,
                'errors' => 'Invalid url',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        $expires_at = $user->getTokenExpiresAt();
        $currentDateTime = new \DateTime();
        if ($expires_at < $currentDateTime) {
            $response = array(
                'code' => 404,
                'errors' => 'Expired link',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => ['email' => $user->getEmail()]
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/resetpassword/{token}", name="reset_password", methods={"POST"})
     */
    public function resetPassword($token, Request $request, UserPasswordHasherInterface $encoder, Email $email)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['recoveryToken' => $token]);
        $parameters = json_decode($request->getContent(), 1);
        $password = $parameters['password'];
        $confirmPassword = $parameters['confirmPassword'];

        $user->setPassword($password); //storing plain password for validation
        // Validation
        $err = $this->validateInput($user);
        if (!empty($err)) {
            $response = array(
                'code' => 422,
                'errors' => $err,
                'result' => null
            );
            return new JsonResponse($response, 422);
        }

        if ($password !== $confirmPassword) {
            $response = array(
                'code' => 422,
                'errors' => ['mismatch' => 'password mismatch'],
                'result' => null
            );
            return new JsonResponse($response, 422);
        }
        $user->setPassword($encoder->hashPassword($user, $password));
        $em->persist($user);
        $em->flush($user);

        // Send email
        $email->sendResetMessage($user->getEmail());

        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => 'Password reset success'
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/api/admin/users/{id}/upload", name="change_image", methods={"POST"})
     */
    public function changeImage($id, Request $request, FileUploader $uploader)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['employeeId' => $id]);
        $userInfo = $user->getUserInfo();
        $previousImagePath = $userInfo->getPpImagePath();
        //File upload
        $file = $request->files->get('image');
        $path = $uploader->upload($file);

        if ($previousImagePath) {
            // delete previous file
            $filesystem = new Filesystem();
            $filesystem->remove($previousImagePath);
        }

        $userInfo->setPpImagePath($path);
        $em->persist($userInfo);
        $em->flush();

        return new JsonResponse([
            'code' => 200,
            'errors' => null,
            'result' => ['imagePath' => $path]
        ], 200);
    }
    /**
     * @Route("/api/get/approvers", name="get_approvers", methods="GET")
     */

    public function getApprovers()
    {
        $id = $this->getUser()->getId();
        $roles = $this->getUser()->getRoles();

        $approvers = $this->getDoctrine()->getRepository(User::class)->findApprovers($id, $roles[0]);
        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($approvers, 'json');

        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }
}
