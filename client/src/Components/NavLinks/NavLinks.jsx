import React from "react"
import { logoutUser } from "../Redux/actions/authActions"
import ExitToAppIcon from "@material-ui/icons/ExitToApp"
import HowToRegIcon from "@material-ui/icons/HowToReg"
import { useDispatch, useSelector } from "react-redux"
import { Link, useLocation } from "react-router-dom"
import { Nav } from "react-bootstrap"
import "./NavLinks.css"
import Avatar from "@material-ui/core/Avatar"
import NavigationMenu from "../NavigationMenu/NavigationMenu"

const NavLinks = () => {
	const me = useSelector((store) => store.authReducer)
	const { isAuthenticated } = me
	const firstName = me.user?.firstName
	const role = me.user?.roles[0]
	const employeeId = me.user?.employeeId
	const ppImagePath = me.user?.userInfo.ppImagePath || null

	const dispatch = useDispatch()
	const location = useLocation()
	const path = location.pathname
	function handleLogout() {
		dispatch(logoutUser())
	}
	let activeStyle = {
		color: "#ff6600",
	}
	return (
		<div className="topRight">
			{isAuthenticated ? (
				<>
					{role === "ROLE_USER" && (
						<Nav>
							<NavigationMenu path={path} activeStyle={activeStyle} />
							<Nav.Link
								as={Link}
								to="/login"
								style={path === "/inventories" ? activeStyle : null}
							>
								Home
							</Nav.Link>
							<Nav.Link
								as={Link}
								to="/requests"
								style={path === "/requests" ? activeStyle : null}
							>
								My requests
							</Nav.Link>
						</Nav>
					)}
					<Link
						to={
							role === "ROLE_USER"
								? "/users/" + employeeId
								: "/dashboard/users/" + employeeId
						}
						style={{ textDecoration: "none", cursor: "pointer" }}
					>
						<Avatar
							alt={firstName}
							src={process.env.REACT_APP_API_URL + ppImagePath}
							style={{ width: "30px", height: "30px" }}
						/>
					</Link>
					<Link
						to="/login"
						onClick={handleLogout}
						className="text-danger nav-link"
					>
						<ExitToAppIcon />
						Logout
					</Link>
				</>
			) : (
				path !== "/login" && (
					<Link
						to="/login"
						className="text-primary"
						style={{ textDecoration: "none", color: "red" }}
					>
						<HowToRegIcon style={{ color: "green", marginBottom: "2px" }} />
						<span style={{ color: "green", marginLeft: "2px" }}>Login</span>
					</Link>
				)
			)}
		</div>
	)
}

export default NavLinks
