import React from "react"
import CollapsePanel from "./CollapsePanel/CollapsePanel"
import Collapse from "@material-ui/core/Collapse"
import TableRow from "@material-ui/core/TableRow"
import { IconButton, makeStyles } from "@material-ui/core"
import { Chip } from "@material-ui/core"
import { Button } from "@material-ui/core"
import { TableCell } from "@material-ui/core"
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp"
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown"
import BorderColorIcon from "@material-ui/icons/BorderColor"
import CancelIcon from "@material-ui/icons/Cancel"
import { Tooltip } from "@material-ui/core"
import axios from "../utils/axios"
import { notify } from "../utils/toast"
import ApproverAvatar from "../ApproverAvatar/ApproverAvatar"
import { dateFormat, timeFormat } from "../utils/dateTimeFormat"
import { useHistory } from "react-router-dom"
import { withStyles } from "@material-ui/styles"

const useRowStyles = makeStyles({
	root: {
		"& > *": {
			borderBottom: "unset",
		},
	},
})
const StyledTableCell = withStyles((theme) => ({
	body: {
		textAlign: "center",
	},
}))(TableCell)

export default function CollapsiableTable({
	ld,
	setLeaveDetails,
	page,
	rowsPerPage,
	setCount,
	findBy,
	showHour,
}) {
	const [open, setOpen] = React.useState()
	React.useEffect(() => {
		setOpen(false)
	}, [page])
	const classes = useRowStyles()
	const DOL = dateFormat(ld.date_of_leave)
	const DOA = dateFormat(ld.date_of_arrival)
	const LT = ld.leave_time
	const AT = ld.arrival_time
	const history = useHistory()
	const handleClick = () => {
		setOpen(!open)
	}

	const handleCancel = async (id, page, rowsPerPage) => {
		const responseData = await axios
			.delete(
				`api/leaveRequest/cancel/${id}?page=${page}&rowsPerPage=${rowsPerPage}&findBy=${JSON.stringify(
					findBy
				)}`
			)
			.then((res) => {
				return res.data
			})
			.catch((err) => console.log(err))
		setLeaveDetails(responseData.result)
		setCount(responseData.count)
	}
	const handleClickEdit = (id) => {
		history.push(`/leave/myLeaveDetails/edit/${id}`)
	}

	return (
		<>
			<TableRow className={classes.root}>
				<StyledTableCell>
					<IconButton onClick={() => handleClick()}>
						{open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
					</IconButton>
				</StyledTableCell>
				<StyledTableCell>{ld.leave_type_name}</StyledTableCell>
				<StyledTableCell>{DOL}</StyledTableCell>
				<StyledTableCell>{LT}</StyledTableCell>
				<StyledTableCell>{DOA}</StyledTableCell>
				<StyledTableCell>{AT}</StyledTableCell>
				<StyledTableCell>
					{showHour == true ? ld.durationHour : ld.durationDay}
				</StyledTableCell>
				<StyledTableCell>
					<Chip
						label={ld.name}
						style={{
							backgroundColor:
								(ld.name == "Pending" && "#1769aa") ||
								(ld.name == "Rejected" && "red") ||
								(ld.name == "Approved" && "green"),
							color: "white",
						}}
					/>
				</StyledTableCell>
				<StyledTableCell >
					{ld.approvers.map((ad) => (
						<ApproverAvatar ad={ad} declineMessage={ld.decline_message} />
					))}
				</StyledTableCell>
				<StyledTableCell>
					{ld.name == "Pending" && (
						<IconButton
							style={{ color: "#ba000d" }}
							onClick={() => handleCancel(ld.id, page, rowsPerPage)}
						>
							<Tooltip title="Cancel">
								<CancelIcon />
							</Tooltip>
						</IconButton>
					)}
					{ld.name == "Pending" && (
						<IconButton onClick={() => handleClickEdit(ld.id)}>
							<BorderColorIcon style={{ color: "#ff6600" }} />
						</IconButton>
					)}
				</StyledTableCell>
			</TableRow>
			<TableRow>
				<StyledTableCell
					style={{ paddingBottom: 0, paddingTop: 0 }}
					colSpan={11}
				>
					<Collapse in={open} timeout="auto" unmountOnExit>
						<CollapsePanel
							leaveDescription={ld.leave_description}
							leaveDoc={ld.leave_documents}
							status={ld.name}
							declineMessage={ld.decline_message}
							approvers={ld.first_name + " " + ld.last_name}
							requestId={ld.id}
						/>
					</Collapse>
				</StyledTableCell>
			</TableRow>
		</>
	)
}
