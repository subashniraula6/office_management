<?php

namespace App\Controller;

use App\Entity\Allocation;
use App\Entity\Dispose;
use App\Entity\Inventory;
use App\Entity\Servicing;
use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use App\Utils\ValidateUtils;
use App\Entity\Category;
use Symfony\Component\Filesystem\Filesystem;
use App\Services\CustomSerializer;
use App\Services\FileUploader;
use Doctrine\ORM\EntityManagerInterface;

class InventoryController extends AbstractController
{
    use ValidateUtils;
    use CustomSerializer;
    /**
     * @Route("/api/admin/inventories", name="fetch_All_Inventories", methods={"GET"})
     */
    public function getAllInventories(EntityManagerInterface $em)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        // Parse query string
        parse_str($_SERVER['QUERY_STRING'], $result);
        // Check if unallocated inventory requested
        if ($result && array_key_exists('unallocated', $result) && $result['unallocated'] === "true") {
            $inventories = $this->getDoctrine()
                ->getRepository(Inventory::class)
                ->findBy(['isAllocatable' => true, 'isAllocated' => false], ['id' => 'DESC']);

            $data = $this->serializeInventories($inventories);

            $response = array(
                'code' => 200,
                'errors' => null,
                'result' => json_decode($data)
            );
            return new JsonResponse($response, 200);
        }

        // total inventory count
        $query = $em->createQuery(
            'SELECT COUNT(i.id)
            FROM APP\ENTITY\INVENTORY i'
        );
        $totalCount = $query->getResult();
        $totalCount = $totalCount[0][1];

        // pagination & query paramteters from api
        $page = 1;
        $per_page = $totalCount;
        if ($result && $result['per_page']) {
            $per_page = $result['per_page'];
        }
        if ($result && $result['page']) {
            $page = $result['page'];
        }
        $offset = $per_page * ($page - 1);

        $category = null;
        if (array_key_exists('category', $result)) {
            $category = $result['category'];
        }
        $owner = null;
        if (array_key_exists('owner', $result)) {
            $owner = $result['owner'];
        }
        // return filtered id's
        if ($owner) {
            $sql = 'SELECT inventory.id FROM inventory 
                    left join allocation on inventory.id=allocation.inventory_id 
                    JOIN category ON inventory.category_id = category.id
                    left join user on user.id=allocation.user_id 
                    WHERE (category.name LIKE :category) 
                    and (allocation.is_current_allocation=true 
                    or allocation.is_current_allocation is NULL)
                    and (user.first_name like :owner)';
        } else {
            $sql = 'SELECT inventory.id FROM inventory 
                JOIN category ON inventory.category_id = category.id
                WHERE (category.name LIKE :category)';
        }

        $conn = $em->getConnection();
        $stmt = $conn->prepare($sql);
        $params = [
            'category' => '%' . $category . '%'
        ];
        if ($owner) {
            $params['owner'] = '%' . $owner . '%';
        }
        $result = $stmt->executeQuery($params);
        $id_arr =  $result->fetchAllAssociative();

        // convert 2d array to 1d
        $id_arr = array_map(function ($d) {
            return $d['id'];
        }, $id_arr);


        //Extract filtered id's inventories
        $inventories = $this->getDoctrine()
            ->getRepository(Inventory::class)
            ->findBy(['id' => $id_arr], ['id' => 'DESC'], $per_page, $offset);

        $data = $this->serializeInventories($inventories);

        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data),
            'page' => (int)$page,
            'totalCount' => $totalCount,
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/api/inventories/{id}", name="get_inventory", methods={"GET"})
     */
    public function getInventory($id)
    {
        $inventory = $this->getDoctrine()
            ->getRepository(Inventory::class)
            ->findOneBy(['inventoryId' => $id]);

        if (empty($inventory)) {
            $response = array(
                'code' => 404,
                'errors' => 'no inventory',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }

        $roles = $this->getUser()->getRoles();
        $allocations = $inventory->getAllocations()->toArray();
        $current_allocation = array_filter(
            $allocations,
            function ($allocation) {
                return $allocation->getIsCurrentAllocation();
            }
        );
        if ($current_allocation) {
            $current_allocation = reset($current_allocation);
        };
        if (!($roles[0] === 'ROLE_ADMIN' ||
            ($current_allocation->getUser() === $this->getUser()))) {
            $response = array(
                'code' => 200,
                'errors' => 'Unauthorized',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        $data = $this->serializeInventory($inventory);
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }


    /**
     * @Route("/api/admin/inventories", name="create_inventory", methods={"POST"})
     */
    public function createNewInventory(Request $request, SerializerInterface $serializer, FileUploader $uploader)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $input = $_POST;
        $input['cost'] = (int)$input['cost'];
        $input['servicingDuration'] = (int)$input['servicingDuration'];
        $input['isAllocatable'] = (bool)$input['isAllocatable'];
        $input['requiresServicing'] = ($input['requiresServicing'] === 'true') ? true : false;
        $input['procurredAt'] = $input['procurredAt'] ? $input['procurredAt'] : "NOW";

        $em = $this->getDoctrine()->getManager();
        $inventory = $serializer->deserialize(json_encode($input), Inventory::class, 'json');

        // check if inventory id is present in DB
        $checkInventory = $this->getDoctrine()->getRepository(Inventory::class)->findOneBy(['inventoryId' => $input['inventoryId']]);
        if ($checkInventory) {
            $response = array(
                'code' => 401,
                'errors' => ['inventoryId' => 'Inventory id already exists'],
                'result' => null
            );
            return new JsonResponse($response, 401);
        }

        $cost = $input['cost'];
        $procurredAt = $input['procurredAt'];
        $notes = $input['notes'];
        $isAllocatable = $input['isAllocatable'];
        $requiresServicing = $input['requiresServicing'];


        // set category
        $category = $input['category'];
        $category = $em->getRepository(Category::class)->findOneBy(['name' => $category]);
        if (empty($category)) {
            return new JsonResponse([
                'code' => 404,
                'errors' => 'category doesnt exist',
                'result' => null
            ]);
        };
        $inventory->setCategory($category);

        $inventory->setCost($cost);
        $inventory->setProcurredAt(new DateTime(empty($procurredAt) ? 'NOW' : $procurredAt));

        $inventory->setNotes($notes);
        $inventory->setInRepair(false);
        $inventory->setInServicing(false);
        $inventory->setIsAllocatable($isAllocatable);

        //Adding Servicing
        if ($requiresServicing) {
            $servicingDuration = $input['servicingDuration'];
            $servicing = new Servicing();
            $servicing->setDurationInMonth($servicingDuration);
            $date = new DateTime('NOW');
            $date->add(new DateInterval('P' . $servicingDuration . 'M'));
            $servicing->setIsSent(false);
            $servicing->setServiceAt($date);
            $servicing->setInventory($inventory);
            $servicing->setIsCurrentServicing(true);
            $servicing->setIsCompleted(false);
            $em->persist($servicing);
        }
        //File upload
        // $file = $request->files->get('image');

        // // Validation   
        $err = $this->validateInput($inventory);
        // if ($file) {
        //     $fileErr = $this->validateFile($file);
        //     $err = array_merge($err, $fileErr);
        // }
        if (!empty($err)) {
            $response = array(
                'code' => 422,
                'errors' => $err,
                'result' => null
            );
            return new JsonResponse($response, 422);
        }

        // $path = $uploader->upload($file);
        // $inventory->setImagePath($path);
        $em->persist($inventory);
        $em->flush();
        // return new created inventory
        return $this->getInventory($inventory->getInventoryId());
    }

    /**
     * @Route("/api/admin/inventories/{id}", name="edit_inventory", methods={"PUT"})
     */
    public function editInventory($id, Request $request, SerializerInterface $serializer)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $entityManager = $this->getDoctrine()->getManager();
        $inventory = $entityManager->getRepository(Inventory::class)
            ->findOneBy(['inventoryId' => $id]);
        if (empty($inventory)) {
            $response = array(
                'code' => 404,
                'errors' => 'No Inventory',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        $body = $request->getContent();
        $parameters = json_decode($body, true);

        // check if inventory id is present in DB
        $checkInventory = $this->getDoctrine()->getRepository(Inventory::class)->findOneBy(['inventoryId' => $parameters['inventoryId']]);
        if ($checkInventory && ($checkInventory->getId() !== $inventory->getId())) {
            $response = array(
                'code' => 401,
                'errors' => ['inventoryId' => 'Inventory id already exists'],
                'result' => null
            );
            return new JsonResponse($response, 401);
        }

        $inventory->setName($parameters['name']);
        $inventory->setBrand($parameters['brand']);
        $category = $entityManager->getRepository(Category::class)->findOneBy(['name' => $parameters['category']]);
        if (empty($category)) {
            return new JsonResponse([
                'code' => 404,
                'errors' => 'category doesnt exist',
                'result' => null
            ]);
        };
        $inventory->setCategory($category);
        $inventory->setModel($parameters['model']);
        $inventory->setInventoryId($parameters['inventoryId']);
        $inventory->setCost($parameters['cost']);
        $inventory->setNotes($parameters['notes']);
        $inventory->setProcurredAt(new \DateTime($parameters['procurredAt']));

        //Change servicing period
        if ($inventory->getRequiresServicing()) {
            $servicing = $entityManager->getRepository(Servicing::class)->findOneBy(['inventory' => $inventory, 'isCurrentServicing' => true]);
            $duration = $servicing->getDurationInmonth();
            $servicing->setDurationInMonth($parameters['servicingDuration']);
            $date = new \DateTime($servicing->getServiceAt());
            if (($parameters['servicingDuration'] - $duration) > 0) {
                $date->add(new DateInterval('P' . ($parameters['servicingDuration'] - $duration) . 'M'));
            } else {
                $date->sub(new DateInterval('P' . ($duration - $parameters['servicingDuration']) . 'M'));
            }
            $servicing->setServiceAt($date);
            $entityManager->persist($servicing);
        }
        // Validation
        $err = $this->validateInput($inventory);
        if (!empty($err)) {
            $response = array(
                'code' => 422,
                'errors' => $err,
                'result' => null
            );
            return new JsonResponse($response, 422);
        }


        $entityManager->persist(($inventory));
        $entityManager->flush();

        // return new created inventory
        return $this->getInventory($inventory->getInventoryId());
    }

    /**
     * @Route("/api/admin/inventories/{id}/{action}", name="manage_inventory", methods={"PUT"})
     */
    public function manageInventory($id, $action)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $inventory = $this->getDoctrine()
            ->getRepository(Inventory::class)
            ->findOneBy(['inventoryId' => $id]);
        if (empty($inventory)) {
            $response = array(
                'code' => 404,
                'errors' => 'no inventory',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }

        $em = $this->getDoctrine()->getManager();

        if ($action === 'dispose') {
            if ($inventory->getIsDisposed()) {
                $response = array(
                    'code' => 404,
                    'errors' => 'Already disposed',
                    'result' => null
                );
                return new JsonResponse($response, 404);
            }
            $inventory->setIsDisposed(true);
            $em->persist($inventory);
            // create new Dispose entity
            $dispose = new Dispose();
            $dispose->setDisposedAt(new \DateTime('NOW'));
            $dispose->setIsCurrent(true);
            $dispose->setInventory($inventory);
            $dispose->setIsRecycled(false);
            $em->persist($dispose);

            //unallocate inventory
            $allocation = $this->getDoctrine()
                ->getRepository(Allocation::class)
                ->findOneBy(['inventory' => $inventory->getId(), 'isCurrentAllocation' => true]);

            if (!empty($allocation)) {
                // set unallocated timestamp and remove current allocation pointer
                $allocation->setUnallocatedAt(new DateTime('NOW'));
                $allocation->setIsCurrentAllocation(false);
                //Change inventory allocation status
                $inventory->setIsAllocated(false);
                $em->persist($allocation);
                $em->persist($inventory);
            }

            if ($inventory->getRequiresServicing()) {
                // remove servicing
                $servicing = $this->getDoctrine()
                    ->getRepository(Servicing::class)
                    ->findOneBy(['inventory' => $inventory->getId(), 'isCurrentServicing' => true]);
                $servicing->setIsCurrentServicing(false);
                $servicing->setRemarks('Inventory was disposed');
                $em->persist($servicing);
            }
        } else if ($action === 'revive') {
            if ($inventory->getIsDisposed() === false) {
                $response = array(
                    'code' => 404,
                    'errors' => 'Already active',
                    'result' => null
                );
                return new JsonResponse($response, 404);
            }
            $inventory->setIsDisposed(false);
            $em->persist($inventory);
            // modify existing entity
            $dispose = $em->getRepository(Dispose::class)
                ->findOneBy(['inventory' => $inventory->getId(), 'isCurrent' => true, 'isRecycled' => false]);
            $dispose->setIsCurrent(false);
            $dispose->setIsRecycled(true);
            $dispose->setRecycledAt(new DateTime('NOW'));
            $em->persist($dispose);

            // set new servicing
            if ($inventory->getRequiresServicing()) {
                $previousServicing = $inventory->getServicings()[0];
                $servicingDuration = $previousServicing->getDurationInMonth();
                $servicing = new Servicing();
                $servicing->setIsSent(false);
                $servicing->setIsCompleted(false);
                $servicing->setDurationInMonth($servicingDuration);
                $date = new DateTime('NOW');
                $date->add(new DateInterval('P' . $servicingDuration . 'M'));
                $servicing->setServiceAt($date);
                $servicing->setInventory($inventory);
                $servicing->setIsCurrentServicing(true);
                $em->persist($servicing);
            }
        }
        $em->flush();
        return $this->getInventory($inventory->getInventoryId());

        $response = array(
            'code' => 401,
            'errors' => 'unauthorized',
            'result' => null
        );
        return new JsonResponse($response, 401);
    }

    /**
     * @Route("/api/admin/images/inventories/{id}/upload", name="change_inventory_image", methods={"POST"})
     */
    public function changeInventoryImage($id, Request $request, FileUploader $uploader)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findOneBy(['inventoryId' => $id]);
        $previousImagePath = $inventory->getImagePath();

        $file = $request->files->get('image');
        //upload file
        $path = $uploader->upload($file);

        if ($previousImagePath) {
            // delete previous file
            $filesystem = new Filesystem();
            $filesystem->remove($previousImagePath);
        }

        //set new path
        $inventory->setImagePath($path);
        $em->persist($inventory);
        $em->flush();

        return new JsonResponse([
            'code' => 200,
            'errors' => null,
            'result' => ['imagePath' => $path]
        ], 200);
    }
}
