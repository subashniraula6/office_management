import React, {useEffect, useState} from 'react'
import NotificationItem from './NotificationItem'

const NotificationBanner = ({servicings}) => {
    const [criticalServicings, setCriticalServicings] = useState([])
    useEffect(()=> {
        const leastServicings = servicings.filter(serv => {
            const remainingDays = Math.round((new Date(serv.serviceAt) - new Date())/(1000*60*60*24))
            if(remainingDays < 30){
                return true
            }
        })
        setCriticalServicings(leastServicings)
        // eslint-disable-next-line
    }, [])
    
    function removeCriticalServicing(id){
        const updatedCriticalServicings = criticalServicings.filter(cs => {
            return cs.id !== id
        })
        setCriticalServicings(updatedCriticalServicings)
    }
    if (criticalServicings.length) return(
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading text-center">Checkout some inventories that needs servicing soon!</h4>
            {
                criticalServicings.map(cs => <NotificationItem key={cs.id} servicing={cs} removeCriticalServicing={removeCriticalServicing}/>)
            }
        </div>
    )
    return null
}

export default NotificationBanner
