<?php

namespace App\DataFixtures;

use App\Entity\ContactRelation;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\UserInfo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {
        $this->loadUser($manager);
    }
    public function loadUser(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('nirajan.panthee@gmail.com');
        $user->setFirstName('Nirajan');
        $user->setLastName('Panthee');
        $user->setJoinedAt(new \DateTime('2014-01-01'));
        $user->setCreatedAt(new \DateTimeImmutable('NOW'));
        $user->setPassword($this->encoder->hashPassword($user, 'nirajanpanthee@123'));
        $user->setDesignation('CEO');
        $user->setEmployeeId('wm-01');
        $user->setDateOfBirth(new \DateTime('2040-12-12'));
        $user->setGender('male');
        $user->setAddedBy('GOD');
        $user->setJoinedAt(new \DateTime('2014-01-01'));
        $user->setHasLeft(false);
        $user->setDepartment('Engineering');

        $userInfo = new UserInfo();
        $userInfo->setAccountNumber('accno123');
        $userInfo->setCitizenshipNumber('12345');
        $userInfo->setPanNumber('pan-3452');
        $userInfo->setPhoneNumber(9801212129);
        $userInfo->setTemporaryAddress('Kathmandu');
        $userInfo->setPermanentAddress('Kathmandu');
        $userInfo->setUser($user);
        $manager->persist($userInfo);

        $relation = new ContactRelation();
        $relation->setRelation('father');
        $relation->setName('Anonymous');
        $relation->setPhoneNumber(9812753847);
        $user->setContactRelation($relation);

        $roles = new Role();
        $roles->setName(['ROLE_ADMIN']);
        $user->setRoles($roles);
        $manager->persist($roles);
        $manager->persist($user);
        $manager->flush();

        //create another role
        $roles = new Role();
        $roles->setName(['ROLE_USER']);
        $manager->persist($roles);
        $manager->persist($user);
        $manager->flush();
    }
}
