import React, { useEffect } from "react";
import InventoryCard from "../inventoryCard/InventoryCard";
import { fetchUserInventories } from "../Redux/actions/userActions";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";

const UserInventories = () => {
  const dispatch = useDispatch();
  const params = useParams();
  useEffect(() => {
    dispatch(fetchUserInventories(params.id));
  }, []);
  const inventories = useSelector((store) => store.userReducer.userInventories);
  return (
    <div>
      {inventories.length ? (
        <>
          <div className="flex-md-wrap d-flex justify-content-start align-items-center">
            {inventories.map((inventory) => (
              <InventoryCard key={inventory.id} inventory={inventory} />
            ))}
          </div>
        </>
      ) : (
        <div className="container alert alert-warning">
          <h5 className="m-2">No inventories to display</h5>
        </div>
      )}
    </div>
  );
};

export default UserInventories;
