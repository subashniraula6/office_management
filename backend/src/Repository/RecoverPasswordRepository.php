<?php

namespace App\Repository;

use App\Entity\RecoverPassword;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RecoverPassword|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecoverPassword|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecoverPassword[]    findAll()
 * @method RecoverPassword[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecoverPasswordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecoverPassword::class);
    }

    // /**
    //  * @return RecoverPassword[] Returns an array of RecoverPassword objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RecoverPassword
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
