import React from "react"

import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableHead from "@material-ui/core/TableHead"
import TableContainer from "@material-ui/core/TableContainer"
import TableRow from "@material-ui/core/TableRow"

import { withStyles } from "@material-ui/styles"
import { makeStyles, Tab } from "@material-ui/core"
import axios from "../../utils/axios"
import ApproverAvatar from "../../ApproverAvatar/ApproverAvatar"
import { dateFormat } from "../../utils/dateTimeFormat"

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: "#ff6600",
		color: theme.palette.common.white,
		textAlign: "center",
	},
	body: {
		textAlign: "center",
	},
}))(TableCell)

const useStyles = makeStyles({
	table: {
		minWidth: 900,
	},
	tableContainer: {
		width: "100%",

		marginTop: "1%",
	},
})

export default function CurrentLeaveTable() {
	const classes = useStyles()
	const [todaysLeave, setTodaysLeave] = React.useState([])

	React.useEffect(() => {
		axios
			.get("api/todays/leave")
			.then((res) => setTodaysLeave(res.data))
			.catch((err) => console.log(err))
	}, [])

	return (
		<div>
			<TableContainer className={classes.tableContainer}>
				<h4 style={{ color: "#ff6600" }}>Employees on Leave</h4>
				<Table className={classes.table} aria-label="customized table">
					<TableHead>
						<TableRow>
							<StyledTableCell></StyledTableCell>
							<StyledTableCell>Employee</StyledTableCell>
							<StyledTableCell>Date Of Leave</StyledTableCell>
							<StyledTableCell>LeaveType</StyledTableCell>
							<StyledTableCell>Approvers</StyledTableCell>
							<StyledTableCell>Date Of Arrival</StyledTableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{todaysLeave.map((stl) => (
							<TableRow key={stl.id}>
								<StyledTableCell></StyledTableCell>
								<StyledTableCell>
									{stl.first_name + " " + stl.last_name}
								</StyledTableCell>
								<StyledTableCell>
									{dateFormat(stl.date_of_leave)}
								</StyledTableCell>
								<StyledTableCell>{stl.leave_type_name}</StyledTableCell>
								<StyledTableCell>
									{stl.approvers.map((ad) => (
										<ApproverAvatar ad={ad} />
									))}
									{stl.adminApprover && (
										<ApproverAvatar ad={stl.adminApprover} />
									)}
								</StyledTableCell>
								<StyledTableCell>
									{dateFormat(stl.date_of_arrival)}
								</StyledTableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
		</div>
	)
}
