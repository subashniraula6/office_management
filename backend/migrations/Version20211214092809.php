<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211214092809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE approvers ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE approvers ADD CONSTRAINT FK_64EC41DDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_64EC41DDA76ED395 ON approvers (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE approvers DROP FOREIGN KEY FK_64EC41DDA76ED395');
        $this->addSql('DROP INDEX IDX_64EC41DDA76ED395 ON approvers');
        $this->addSql('ALTER TABLE approvers DROP user_id');
    }
}
