<?php

namespace App\Entity;

use App\Repository\PublicHolidayRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PublicHolidayRepository::class)
 */
class PublicHoliday
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $holidayDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $holidayOccassion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHolidayDate(): ?\DateTimeInterface
    {
        return $this->holidayDate;
    }

    public function setHolidayDate(\DateTimeInterface $holidayDate): self
    {
        $this->holidayDate = $holidayDate;

        return $this;
    }

    public function getHolidayOccassion(): ?string
    {
        return $this->holidayOccassion;
    }

    public function setHolidayOccassion(string $holidayOccassion): self
    {
        $this->holidayOccassion = $holidayOccassion;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }
}
