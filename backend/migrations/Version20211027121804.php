<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211027121804 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE allocation (id INT AUTO_INCREMENT NOT NULL, inventory_id INT NOT NULL, user_id INT NOT NULL, allocated_at DATETIME NOT NULL, allocated_by VARCHAR(255) DEFAULT NULL, is_current_allocation TINYINT(1) NOT NULL, unallocated_at DATETIME DEFAULT NULL, unallocated_by VARCHAR(255) DEFAULT NULL, notes VARCHAR(1000) DEFAULT NULL, INDEX IDX_5C44232A9EEA759 (inventory_id), INDEX IDX_5C44232AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_relation (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, relation VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, phone_number BIGINT DEFAULT NULL, UNIQUE INDEX UNIQ_F8ACDBDE6B01BC5B (phone_number), UNIQUE INDEX UNIQ_F8ACDBDEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dispose (id INT AUTO_INCREMENT NOT NULL, inventory_id INT NOT NULL, disposed_at DATETIME NOT NULL, is_recycled TINYINT(1) NOT NULL, recycled_at DATETIME DEFAULT NULL, is_current TINYINT(1) NOT NULL, INDEX IDX_6262E0669EEA759 (inventory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventory (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, brand VARCHAR(50) DEFAULT NULL, model VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, inventory_id VARCHAR(50) NOT NULL, cost INT DEFAULT NULL, procurred_at DATETIME DEFAULT NULL, notes VARCHAR(255) NOT NULL, is_allocatable TINYINT(1) NOT NULL, is_allocated TINYINT(1) NOT NULL, is_disposed TINYINT(1) NOT NULL, requires_servicing TINYINT(1) NOT NULL, image_path VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_B12D4A369EEA759 (inventory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE repair (id INT AUTO_INCREMENT NOT NULL, inventory_id INT NOT NULL, sent_at DATETIME DEFAULT NULL, returned_at DATETIME DEFAULT NULL, is_in_progress TINYINT(1) DEFAULT NULL, notes VARCHAR(1000) NOT NULL, cost INT DEFAULT NULL, INDEX IDX_8EE434219EEA759 (inventory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE request (id INT AUTO_INCREMENT NOT NULL, inventory_id INT DEFAULT NULL, user_id INT NOT NULL, type VARCHAR(255) NOT NULL, message VARCHAR(255) DEFAULT NULL, status VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, response VARCHAR(1000) DEFAULT NULL, INDEX IDX_3B978F9F9EEA759 (inventory_id), INDEX IDX_3B978F9FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, name JSON NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE servicing (id INT AUTO_INCREMENT NOT NULL, inventory_id INT NOT NULL, service_at DATETIME DEFAULT NULL, duration_in_month INT NOT NULL, is_current_servicing TINYINT(1) NOT NULL, sent_at DATETIME DEFAULT NULL, returned_at DATETIME DEFAULT NULL, cost INT DEFAULT NULL, is_in_progress TINYINT(1) NOT NULL, remarks VARCHAR(1000) DEFAULT NULL, INDEX IDX_C3BF65139EEA759 (inventory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, roles_id INT NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(250) NOT NULL, has_left TINYINT(1) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', employee_id VARCHAR(255) NOT NULL, date_of_birth DATETIME DEFAULT NULL, gender VARCHAR(10) NOT NULL, added_by VARCHAR(255) NOT NULL, joined_at DATETIME NOT NULL, left_at DATETIME DEFAULT NULL, designation VARCHAR(255) NOT NULL, department VARCHAR(100) NOT NULL, recovery_token VARCHAR(255) DEFAULT NULL, token_expires_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D6498C03F15C (employee_id), INDEX IDX_8D93D64938C751C4 (roles_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_info (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, citizenship_number VARCHAR(255) DEFAULT NULL, pan_number VARCHAR(255) DEFAULT NULL, phone_number BIGINT NOT NULL, pp_image_path VARCHAR(255) DEFAULT NULL, ct_image_path VARCHAR(255) DEFAULT NULL, pan_image_path VARCHAR(255) DEFAULT NULL, temporary_address VARCHAR(255) DEFAULT NULL, permanent_address VARCHAR(255) DEFAULT NULL, account_number VARCHAR(255) DEFAULT NULL, notes VARCHAR(1000) DEFAULT NULL, UNIQUE INDEX UNIQ_B1087D9EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE allocation ADD CONSTRAINT FK_5C44232A9EEA759 FOREIGN KEY (inventory_id) REFERENCES inventory (id)');
        $this->addSql('ALTER TABLE allocation ADD CONSTRAINT FK_5C44232AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE contact_relation ADD CONSTRAINT FK_F8ACDBDEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE dispose ADD CONSTRAINT FK_6262E0669EEA759 FOREIGN KEY (inventory_id) REFERENCES inventory (id)');
        $this->addSql('ALTER TABLE repair ADD CONSTRAINT FK_8EE434219EEA759 FOREIGN KEY (inventory_id) REFERENCES inventory (id)');
        $this->addSql('ALTER TABLE request ADD CONSTRAINT FK_3B978F9F9EEA759 FOREIGN KEY (inventory_id) REFERENCES inventory (id)');
        $this->addSql('ALTER TABLE request ADD CONSTRAINT FK_3B978F9FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE servicing ADD CONSTRAINT FK_C3BF65139EEA759 FOREIGN KEY (inventory_id) REFERENCES inventory (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64938C751C4 FOREIGN KEY (roles_id) REFERENCES role (id)');
        $this->addSql('ALTER TABLE user_info ADD CONSTRAINT FK_B1087D9EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE allocation DROP FOREIGN KEY FK_5C44232A9EEA759');
        $this->addSql('ALTER TABLE dispose DROP FOREIGN KEY FK_6262E0669EEA759');
        $this->addSql('ALTER TABLE repair DROP FOREIGN KEY FK_8EE434219EEA759');
        $this->addSql('ALTER TABLE request DROP FOREIGN KEY FK_3B978F9F9EEA759');
        $this->addSql('ALTER TABLE servicing DROP FOREIGN KEY FK_C3BF65139EEA759');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64938C751C4');
        $this->addSql('ALTER TABLE allocation DROP FOREIGN KEY FK_5C44232AA76ED395');
        $this->addSql('ALTER TABLE contact_relation DROP FOREIGN KEY FK_F8ACDBDEA76ED395');
        $this->addSql('ALTER TABLE request DROP FOREIGN KEY FK_3B978F9FA76ED395');
        $this->addSql('ALTER TABLE user_info DROP FOREIGN KEY FK_B1087D9EA76ED395');
        $this->addSql('DROP TABLE allocation');
        $this->addSql('DROP TABLE contact_relation');
        $this->addSql('DROP TABLE dispose');
        $this->addSql('DROP TABLE inventory');
        $this->addSql('DROP TABLE repair');
        $this->addSql('DROP TABLE request');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE servicing');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_info');
    }
}
