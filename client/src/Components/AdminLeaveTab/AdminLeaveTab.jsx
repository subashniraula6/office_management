import React, { useEffect, useContext, useState } from "react"
import AppBar from "@material-ui/core/AppBar"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import Typography from "@material-ui/core/Typography"
import Box from "@material-ui/core/Box"
import PropTypes from "prop-types"
import SwipeableViews from "react-swipeable-views"
import { makeStyles, useTheme } from "@material-ui/core/styles"
import Paper from "@material-ui/core/Paper"
import AllLeaveRequestTable from "./AdminLeaveTables/AllLeaveRequestTable"
import axios from "../utils/axios"
import LeaveHistoryTable from "./AdminLeaveTables/LeaveHistoryTable"
import CurrentLeaveTable from "./AdminLeaveTables/CurrentLeaveTable"
import AllLeaveStatistics from "./AllLeaveStatistics/AllLeaveStatistics"

function TabPanel(props) {
	const { children, value, index, ...other } = props

	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`full-width-tabpanel-${index}`}
			aria-labelledby={`full-width-tab-${index}`}
			{...other}
		>
			{value === index && (
				<Box p={3}>
					<Typography>
						<Paper>{children}</Paper>
					</Typography>
				</Box>
			)}
		</div>
	)
}

TabPanel.propTypes = {
	children: PropTypes.node,
	index: PropTypes.any.isRequired,
	value: PropTypes.any.isRequired,
}

function a11yProps(index) {
	return {
		id: `full-width-tab-${index}`,
		"aria-controls": `full-width-tabpanel-${index}`,
	}
}

const useStyles = makeStyles((theme) => ({
	root: {
		backgroundColor: theme.palette.background.paper,
		width: "98%",
		marginLeft: "1%",
	},
}))

export function AdminLeaveTab() {
	const classes = useStyles()
	const theme = useTheme()
	const [value, setValue] = useState(0)
	const [allLeaveData, setAllLeaveData] = useState([])

	const [pendingCount, setPendingCount] = useState()
	const [notPendingCount, setNotPendingCount] = useState()
	const [page, setPage] = React.useState(0)
	const [rowsPerPage, setRowsPerPage] = React.useState(5)

	const handleChange = (event, newValue) => {
		setValue(newValue)
	}

	const handleChangeIndex = (index) => {
		setValue(index)
	}

	return (
		<div className={classes.root}>
			<AppBar position="static" color="default">
				<Tabs
					value={value}
					onChange={handleChange}
					indicatorColor="primary"
					textColor="primary"
					variant="fullWidth"
					aria-label="full width tabs example"
				>
					<Tab label="Leave Requests" {...a11yProps(0)} />
					<Tab label="Today's Leave" {...a11yProps(1)} />
					<Tab label="Leave History" {...a11yProps(2)} />
					<Tab label="Leave Statistics" {...a11yProps(3)} />
				</Tabs>
			</AppBar>
			<SwipeableViews
				axis={theme.direction === "rtl" ? "x-reverse" : "x"}
				index={value}
				onChangeIndex={handleChangeIndex}
			>
				<TabPanel value={value} index={0} dir={theme.direction}>
					<AllLeaveRequestTable />
				</TabPanel>
				<TabPanel value={value} index={1} dir={theme.direction}>
					<CurrentLeaveTable />
				</TabPanel>
				<TabPanel value={value} index={2} dir={theme.direction}>
					<LeaveHistoryTable />
				</TabPanel>
				<TabPanel value={value} index={3} dir={theme.direction}>
					<AllLeaveStatistics />
				</TabPanel>
			</SwipeableViews>
		</div>
	)
}
