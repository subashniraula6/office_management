import React from 'react'

const NotificationItem = ({servicing, removeCriticalServicing}) => {
    return (
        <div>
            <hr />
            <div className='row' style={{fontFamily: "'Scheherazade New', 'serif'", fontSize: '1.8rem'}}>
                <h4  className="col-lg-3">{servicing.inventory.name}</h4>
                <h5 className="col-lg-3">({servicing.inventory.inventoryId})</h5>
                <i 
                    className="fa fa-times col-lg-2" 
                    onClick={()=> removeCriticalServicing(servicing.id)}
                    style={{cursor: 'pointer'}}
                ></i>
            </div>
        </div>
    )
}

export default NotificationItem
