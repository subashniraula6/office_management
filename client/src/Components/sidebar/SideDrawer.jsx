import React from "react"
import clsx from "clsx"
import { makeStyles, useTheme } from "@material-ui/core/styles"
import Drawer from "@material-ui/core/Drawer"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import List from "@material-ui/core/List"
import CssBaseline from "@material-ui/core/CssBaseline"
import Typography from "@material-ui/core/Typography"
import Divider from "@material-ui/core/Divider"
import IconButton from "@material-ui/core/IconButton"
import MenuIcon from "@material-ui/icons/Menu"
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft"
import ChevronRightIcon from "@material-ui/icons/ChevronRight"
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ListItemText from "@material-ui/core/ListItemText"
import { Switch, Link } from "react-router-dom"
import Inventories from "../Pages/inventories/Inventories"
import UserList from "../Pages/user/UserList"
import ServicingList from "../servicings/ServicingList"
import EditUser from "../EditUser"
import Tabpanel from "../Tabs/Tabpanel"
import EditInventory from "../EditInventory"
import AddInventory from "../AddInventoryForm/AddInventoryForm"
import AddUser from "../Pages/AddUser/AddUser"
import UserProfile from "../Users/profile/UserProfile"
import MyLeaveDetailsTable from "../AdminLeaveTab/AdminLeaveTables/MyLeaveDetailsTable"
import PeopleAltIcon from "@material-ui/icons/PeopleAlt"
import { Timer } from "@material-ui/icons"
import DashboardIcon from "@material-ui/icons/Dashboard"
import { useHistory } from "react-router"
import NavLinks from "../NavLinks/NavLinks"
import RequestTable from "../Request/RequestTable"
import Charts from "../Charts/Charts"
import AdminRoute from "../routing/AdminRoute"
import ContactMailIcon from "@material-ui/icons/ContactMail"
import MeetingRoomIcon from "@material-ui/icons/MeetingRoom"
import { AdminLeaveTab } from "../AdminLeaveTab/AdminLeaveTab"
import EditLeaveForm from "../AdminLeaveTab/LeaveFormAdmin/EditLeaveForm"
import LeaveTypeTable from "../AdminLeaveTab/LeaveTypeTable/LeaveTypeTable"
import LeaveApplyForm from "../LeaveApplyForm/LeaveApplyForm"
import GroupIcon from "@material-ui/icons/Group"
import LeaveNavigationMenu from "./LeaveNavigationMenu/LeaveNavigaitonMenu"

const drawerWidth = 220

export default function SideDrawer() {
	const history = useHistory()
	const pathName = history.location.pathname
	const classes = useStyles()
	const theme = useTheme()
	const [open, setOpen] = React.useState(false)
	const [anchorEl, setAnchorEl] = React.useState(null)

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget)
	}

	const handleClose = () => {
		setAnchorEl(null)
	}

	const handleDrawerOpen = () => {
		setOpen(true)
	}

	const handleDrawerClose = () => {
		setOpen(false)
	}

	return (
		<div className={classes.root}>
			<CssBaseline />
			<AppBar
				position="fixed"
				className={clsx(classes.appBar, {
					[classes.appBarShift]: open,
				})}
			>
				<Toolbar>
					<div className="d-flex justify-content-between align-items-center w-100">
						<div>
							<div className="d-flex justify-content-start align-items-center">
								<div>
									<IconButton
										color="inherit"
										aria-label="open drawer"
										onClick={handleDrawerOpen}
										edge="start"
										className={clsx(classes.menuButton, {
											[classes.hide]: open,
										})}
									>
										<MenuIcon color="primary" />
									</IconButton>
								</div>
								<div>
									<Typography
										variant="h6"
										noWrap
										color="primary"
										component={"span"}
									>
										Office Management System
									</Typography>
								</div>
							</div>
						</div>
						<div>
							<NavLinks />
						</div>
					</div>
				</Toolbar>
			</AppBar>
			<Drawer
				variant="permanent"
				className={clsx(classes.drawer, {
					[classes.drawerOpen]: open,
					[classes.drawerClose]: !open,
				})}
				classes={{
					paper: clsx({
						[classes.drawerOpen]: open,
						[classes.drawerClose]: !open,
					}),
				}}
			>
				<div className={classes.toolbar}>
					<IconButton onClick={handleDrawerClose}>
						{theme.direction === "rtl" ? (
							<ChevronRightIcon />
						) : (
							<ChevronLeftIcon />
						)}
					</IconButton>
				</div>
				<Divider />
				<List>
					<>
						<Link to="/dashboard" className={classes.link}>
							<ListItem
								button
								className={pathName === "/dashboard" ? classes.active : ""}
							>
								<ListItemIcon>
									<DashboardIcon />
								</ListItemIcon>
								<ListItemText primary="Dashboard" />
							</ListItem>
						</Link>
						<Link to="/dashboard/users" className={classes.link}>
							<ListItem
								button
								className={
									/\/dashboard\/users/.test(pathName) ? classes.active : ""
								}
							>
								<ListItemIcon>
									<PeopleAltIcon />
								</ListItemIcon>
								<ListItemText primary="Users" />
							</ListItem>
						</Link>
						<Link to="/dashboard/inventories" className={classes.link}>
							<ListItem
								button
								className={
									/\/dashboard\/inventories/.test(pathName)
										? classes.active
										: ""
								}
							>
								<ListItemIcon>
									<i class="fas fa-boxes" style={{ fontSize: "1.5rem" }} />
								</ListItemIcon>
								<ListItemText primary="Inventories" />
							</ListItem>
						</Link>
						<Link to="/dashboard/requests" className={classes.link}>
							<ListItem
								button
								className={
									/\/dashboard\/requests/.test(pathName) ? classes.active : ""
								}
							>
								<ListItemIcon>
									<ContactMailIcon />
								</ListItemIcon>
								<ListItemText primary="Requests" />
							</ListItem>
						</Link>
						<Link to="/dashboard/servicings" className={classes.link}>
							<ListItem
								button
								className={
									/\/dashboard\/servicings/.test(pathName) ? classes.active : ""
								}
							>
								<ListItemIcon>
									<Timer />
								</ListItemIcon>
								<ListItemText primary="Servicing" />
							</ListItem>
						</Link>
						<LeaveNavigationMenu
							handleClick={handleClick}
							handleClose={handleClose}
							anchorEl={anchorEl}
							setAnchorEl={setAnchorEl}
						/>
						<Link className={classes.link} onMouseEnter={handleClick}>
							<ListItem
								button
								className={
									/\/dashboard\/leaveTab/.test(pathName) ||
									/\/dashboard\/myLeaveDetails/.test(pathName)
										? classes.active
										: ""
								}
							>
								<ListItemIcon>
									<MeetingRoomIcon style={{ fontSize: "1.7rem" }} />
								</ListItemIcon>
								<ListItemText primary="Leave" />
							</ListItem>
						</Link>
					</>
				</List>
			</Drawer>
			<main className={classes.content}>
				<Switch>
					<AdminRoute exact path="/dashboard" component={Charts} />
					<AdminRoute exact path="/dashboard/users" component={UserList} />
					<AdminRoute
						exact
						path="/dashboard/inventories"
						component={Inventories}
					/>
					<AdminRoute
						exact
						path="/dashboard/inventories/add"
						component={AddInventory}
					/>
					<AdminRoute
						exact
						path="/dashboard/servicings"
						component={ServicingList}
					/>
					<AdminRoute
						exact
						path="/dashboard/users/edit/:id"
						component={EditUser}
					/>
					<AdminRoute exact path="/dashboard/users/add" component={AddUser} />
					<AdminRoute
						exact
						path="/dashboard/users/:id"
						component={UserProfile}
					/>
					<AdminRoute
						exact
						path="/dashboard/inventories/edit/:id"
						component={EditInventory}
					/>
					<AdminRoute
						exact
						path="/dashboard/inventories/:id"
						component={Tabpanel}
					/>
					<AdminRoute
						exact
						path="/dashboard/leaveTab"
						component={AdminLeaveTab}
					/>
					<AdminRoute
						exact
						path="/dashboard/myLeaveDetails"
						component={MyLeaveDetailsTable}
					/>
					<AdminRoute
						exact
						path="/dashboard/myLeaveDetails/applyLeave"
						component={LeaveApplyForm}
					/>
					<AdminRoute
						exact
						path="/dashboard/leaveTab/edit/:id"
						component={EditLeaveForm}
					/>
					<AdminRoute
						exact
						path="/dashboard/myLeaveDetails/edit/:id"
						component={EditLeaveForm}
					/>
					<AdminRoute
						exact
						path="/dashboard/leaveTab/leaveTypes"
						component={LeaveTypeTable}
					/>
					<AdminRoute
						exact
						path="/dashboard/requests"
						render={(props) => <RequestTable {...props} role="ROLE_ADMIN" />}
					/>
				</Switch>
			</main>
		</div>
	)
}

const useStyles = makeStyles((theme) => ({
	root: {
		display: "flex",
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		transition: theme.transitions.create(["width", "margin"], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		height: "73px",
		backgroundColor: "#ffff",
	},
	appBarShift: {
		marginLeft: drawerWidth,
		width: `calc(100% - ${drawerWidth}px)`,
		transition: theme.transitions.create(["width", "margin"], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	menuButton: {
		marginRight: 36,
	},
	hide: {
		display: "none",
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
		whiteSpace: "nowrap",
	},
	drawerOpen: {
		width: drawerWidth,
		transition: theme.transitions.create("width", {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	drawerClose: {
		transition: theme.transitions.create("width", {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		overflowX: "hidden",
		width: theme.spacing(7) + 1,
		[theme.breakpoints.up("sm")]: {
			width: theme.spacing(9) + 1,
		},
	},
	toolbar: {
		display: "flex",
		alignItems: "center",
		justifyContent: "flex-end",
		padding: theme.spacing(0, 1),
		// necessary for content to be below app bar
		...theme.mixins.toolbar,
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3),
	},
	active: {
		backgroundColor: "#ff6600",
	},
	link: {
		textDecoration: "none",
		color: "inherit",
	},
	navLinkContainer: {
		marginRight: "15px",
	},
}))
