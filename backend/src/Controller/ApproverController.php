<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Utils\Query;

class ApproverController extends AbstractController
{
    use Query;
    /**
     * @Route("/approver", name="approver")
     */
    public function index(): Response
    {
        return $this->render('approver/index.html.twig', [
            'controller_name' => 'ApproverController',
        ]);
    }

    /**
     * @Route("api/approver/leaveRequest/{id}", name="get_approverof_leave_request", methods={"GET"})
     */
    public function getApproverOfLeaveRequest($id, EntityManagerInterface $em)
    {
        if ($id) {

            $leaveRequest = $this->fetchApprovers($id);
            $normalizers = [new ObjectNormalizer()];
            $encoders = [new JsonEncoder()];
            $serializer = new Serializer($normalizers, $encoders);
            $data = $serializer->serialize($leaveRequest, 'json');

            return new Response($data, 200, ['Content-Type' => 'application/json']);
        } else {
            return new Response("there is no request selected to edit", 400);
        }
    }
}
