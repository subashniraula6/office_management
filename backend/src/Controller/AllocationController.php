<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Allocation;
use App\Entity\Inventory;
use App\Entity\User;
use App\Services\CustomSerializer;
use DateTime;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class AllocationController extends AbstractController
{
    use CustomSerializer;
    /**
     * @Route("/api/admin/allocations", name="allocate_inventory", methods={"POST"})
     */
    public function allocateInventory(Request $request, SerializerInterface $serializer)
    {
        $em = $this->getDoctrine()->getManager();
        $parameters = json_decode($request->getContent(), true);
        $employeeId = $parameters['employeeId'];
        $notes = $parameters['notes'];
        $allocatedAt = $parameters['allocatedAt'];
        $inventoryId = $parameters['inventoryId'];

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['employeeId' => $employeeId]);
        if ($user->getHasLeft()) {
            $response = array(
                'code' => 400,
                'errors' => 'Inventory cannot be allocated to left users',
                'result' => null
            );
            return new JsonResponse($response, 400);
        }

        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findOneBy(['inventoryId' => $inventoryId]);
        // Check if requested allocation is current allocation
        $checkAllocation = $this->getDoctrine()
            ->getRepository(Allocation::class)
            ->findOneBy(['user' => $user->getId(), 'inventory' => $inventory->getId(), 'isCurrentAllocation' => true]);
        if (!empty($checkAllocation)) {
            $response = array(
                'code' => 400,
                'errors' => 'No allocation changes made',
                'result' => null
            );
            return new JsonResponse($response);
        }
        // Check if current inventory has allocation
        $previousAllocation = $this->getDoctrine()
            ->getRepository(Allocation::class)
            ->findOneBy(['inventory' => $inventory->getId(), 'isCurrentAllocation' => true]);

        if (!empty($previousAllocation)) {
            // Modify previous allocation
            $previousAllocation->setIsCurrentAllocation(false);
            $previousAllocation->setUnAllocatedAt(new DateTime('NOW'));
            $previousAllocation->setUnallocatedBy($this->getUser()->getFirstName() . ' ' . $this->getUser()->getLastname());
            $em->persist($previousAllocation);
        }

        //Make new allocation
        $newAllocation = new Allocation();
        $newAllocation->setUser($user);
        $newAllocation->setInventory($inventory);
        $newAllocation->setAllocatedBy($this->getUser()->getFirstName() . ' ' . $this->getUser()->getLastname());
        $newAllocation->setIsCurrentAllocation(true);
        if (!empty($allocatedAt)) {
            $newAllocation->setAllocatedAt(new DateTime($allocatedAt));
        } else {
            $newAllocation->setAllocatedAt(new DateTime('NOW'));
        }
        $notes && $newAllocation->setNotes($notes);
        $em->persist($newAllocation);

        //Change inventory allocation status
        $inventory->setIsAllocated(true);
        $em->persist($inventory);

        $em->flush();

        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::ATTRIBUTES => [
                'allocations', 'user', 'email', 'firstName', 'lastName', 'employeeId',
                'notes', 'allocatedAt', 'isDisposed', 'unallocatedAt', 'isCurrentAllocation', 'inventory', 'inventoryId', 'isAllocated'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);
        $previousData = $serializer->serialize($previousAllocation, 'json');
        $newAllocation = $serializer->serialize($newAllocation, 'json');


        // // Return updated user inventories
        // $user = $em->getRepository(User::class)->findOneBy(['employeeId' => $employeeId]);
        // $allocations = $user->getAllocations()->toArray();
        // $current_allocations = array_filter($allocations, function ($allocation) {
        //     return $allocation->getIsCurrentAllocation() === true;
        // });
        // $current_inventories = array_map(function ($allocation) {
        //     return $allocation->getInventory();
        // }, $current_allocations);
        // $user_inventories = $this->serializeInventories($current_inventories);

        $response = array(
            'code' => 200,
            'errors' => null,
            'previousAllocation' => json_decode($previousData),
            'newAllocation' => json_decode($newAllocation),
            'employeeId' => $employeeId
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/api/admin/inventories/{id}/allocations/unallocate", name="unallocate_inventory", methods={"PUT"})
     */
    public function unAllocateInventory($id, SerializerInterface $serializer)
    {
        $em = $this->getDoctrine()->getManager();

        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findOneBy(['inventoryId' => $id]);
        // Check if requested allocation is current allocation
        $allocation = $this->getDoctrine()
            ->getRepository(Allocation::class)
            ->findOneBy(['inventory' => $inventory->getId(), 'isCurrentAllocation' => true]);

        // prevent multiple unallocation
        if (empty($allocation)) {
            $response = array(
                'code' => 400,
                'errors' => 'Inventory already unallocated',
                'result' => null
            );
            return new JsonResponse($response);
        }
        // set unallocated timestamp and remove current allocation pointer
        $allocation->setUnallocatedAt(new DateTime('NOW'));
        $allocation->setIsCurrentAllocation(false);

        //Change inventory allocation status
        $inventory->setIsAllocated(false);

        $em->persist($allocation);
        $em->persist($inventory);

        $em->flush();

        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::ATTRIBUTES => [
                'allocations', 'user', 'email', 'firstName', 'lastName', 'userId',
                'notes', 'allocatedAt', 'isDisposed', 'unallocatedAt', 'isCurrentAllocation', 'inventory', 'inventoryId', 'isAllocated'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);

        $allocation = $serializer->serialize($allocation, 'json');
        $response = array(
            'code' => 200,
            'errors' => null,
            'allocation' => json_decode($allocation),
            'inventoryId' => $inventory->getInventoryId()
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/api/admin/inventories/{id}/allocations", name="fetch_inventory_allocations", methods={"GET"})
     */
    public function getInventoryAllocations($id)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        // find inventory by slug(inventoryId)
        $inventory = $this->getDoctrine()
            ->getRepository(Inventory::class)
            ->findOneBy(['inventoryId' => $id]);

        $allocations = $this->getDoctrine()
            ->getRepository(Allocation::class)
            ->findBy(['inventory' => $inventory]);

        if (empty($allocations)) {
            $response = array(
                'code' => 404,
                'errors' => 'no allocations',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }

        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::ATTRIBUTES => [
                'allocations', 'user', 'email', 'firstName', 'lastName', 'employeeId', 'inventory', 'imagePath',
                'inventoryId', 'notes', 'isDisposed', 'allocatedAt', 'isCurrentAllocation', 'unallocatedAt', 'isCurrentAllocation'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($allocations, 'json');
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }
}
