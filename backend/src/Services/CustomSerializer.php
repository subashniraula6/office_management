<?php

namespace App\Services;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

trait CustomSerializer
{
    public function serializeUser($user)
    {
        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'email', 'designation', 'joinedAt', 'probationEndAt', 'userInfo',
                'hasLeft', 'firstName', 'lastName', 'leftAt', 'requests', 'roles', 'employeeId', 'gender',
                'dateOfBirth', 'citizenshipNumber', 'panNumber', 'gender', 'createdAt', 'addedBy',
                'phoneNumber', 'contactRelation', 'name', 'relation', 'temporaryAddress', 'permanentAddress',
                'inventory', 'name', 'brand', 'accountNumber', 'model', 'status', 'description', 'category',
                'inventoryId', 'servicing', 'serviceAt', 'userId', 'department', 'notes', 'user', 'ctImagePath',
                'panImagePath', 'ppImagePath', 'documents', 'category', 'imagePath'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($user, 'json');
        return $data;
    }

    public function serializeUsers($users)
    {
        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'email', 'designation', 'status', 'phoneNumber', 'hasLeft', 'userInfo',
                'firstName', 'lastName', 'employeeId', 'requests', 'roles', 'department',
                'ppImagePath', 'dateOfBirth'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);

        $data = $serializer->serialize($users, 'json');

        return $data;
    }

    public function serializeInventory($inventory)
    {
        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'name', 'brand', 'model', 'isDisposed', 'category',
                'user', 'email', 'firstName', 'lastName', 'inventoryId', 'createdAt', 'disposeAt', 'designation', 'servicings', 'isCurrentServicing',
                'serviceAt', 'durationInMonth', 'isInProgress', 'cost', 'procurredAt', 'notes', 'isAllocatable', 'requiresServicing', 'allocations',
                'allocatedAt', 'isAllocated', 'isDisposed', 'disposes', 'disposedAt', 'isRecycled', 'recycledAt', 'isCurrent', 'imagePath', 'requests',
                'status', 'type', 'inRepair'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($inventory, 'json');
        return $data;
    }

    public function serializeInventories($inventories)
    {
        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return null;
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'name', 'brand', 'model', 'isDisposed',
                'category', 'user', 'email', 'firstName', 'lastName', 'inventoryId',
                'createdAt', 'cost', 'procurredAt', 'notes', 'isAllocatable', 'allocations',
                'isAllocated', 'isDisposed', 'disposes', 'disposedAt', 'isRecycled', 'recycledAt',
                'isCurrent', 'requiresServicing', 'imagePath', 'allocations', 'isCurrentAllocation'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($inventories, 'json');
        return $data;
    }
}
