import React from "react";

const NoRoute = () => {
	return (
		<div className="container image">
			<img
				src="https://i.pinimg.com/originals/4b/c2/d7/4bc2d788f3a0cce14414936f4bc1ba7d.gif"
				alt=""
				srcset=""
			/>
		</div>
	);
};

export default NoRoute;
