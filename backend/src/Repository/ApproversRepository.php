<?php

namespace App\Repository;

use App\Entity\Approvers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use PDO;

/**
 * @method Approvers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Approvers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Approvers[]    findAll()
 * @method Approvers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApproversRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Approvers::class);
    }

    // /**
    //  * @return Approvers[] Returns an array of Approvers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Approvers
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findApproverForOneRequest($id)
    {
        $con = $this->getEntityManager()->getConnection();
        $sql = "SELECT
                    approvers.id,
                    approvers.user_id,
                    approvers.is_approved
                FROM
                    approvers
                WHERE
                    approvers.leave_request_id = :id";
        $stmt = $con->prepare($sql);
        $params = array('id' => $id);
        $result = $stmt->executeQuery($params);
        return $result->fetchAllAssociative();
    }

    public function findTotalStatus($id)
    {
        $con = $this->getEntityManager()->getConnection();
        $sql = "SELECT
                approvers.is_approved,
                approvers.is_admin_approved
                FROM
                    approvers
                WHERE
                    approvers.leave_request_id = :id";
        $stmt = $con->prepare($sql);
        $params = array('id' => $id);
        $result = $stmt->executeQuery($params);
        return $result->fetchAllAssociative();
    }

    public function findSupervisorApproveStatus($approverId)
    {
        $con = $this->getEntityManager()->getConnection();
        $sql = "SELECT
                    is_approved
                FROM
                    approvers
                WHERE
                    approvers.id = :approverId";
        $stmt = $con->prepare($sql);
        $stmt->bindParam('approverId', $approverId, PDO::PARAM_INT);
        $result = $stmt->executeQuery()->fetchAllAssociative();
        return $result;
    }
}
