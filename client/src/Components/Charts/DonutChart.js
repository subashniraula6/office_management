import React from "react";
import { Pie, Doughnut } from "react-chartjs-2";

let regionalData = [
  {
    "Region Arms Sales Dashboard": "East Asia and the Pacific",
    Notifications: "$28,131,565,903",
  },
  {
    "Region Arms Sales Dashboard": "Europe",
    Notifications: "$16,491,553,204",
  },
  {
    "Region Arms Sales Dashboard": "Latin America and the Caribbean",
    Notifications: "$91,660,488",
  },
  {
    "Region Arms Sales Dashboard": "Middle East and North Africa",
    Notifications: "$26,850,287,394",
  },
  {
    "Region Arms Sales Dashboard": "South and Central Asia",
    Notifications: "$4,678,000,500",
  },
];

let labels = regionalData.map((el) => el["Region Arms Sales Dashboard"]);
// console.log(labels);
let numbers = regionalData.map((el) =>
  el.Notifications.match(/[0-9]/g).join("")
);

const createData = (data) => {
  return {
    labels: data && data.categories.map((c) => c.name),
    datasets: [
      {
        label: data && data.categories.map((c) => c.name),
        data: data && data.categories.map((c) => c.count),
        backgroundColor: [
          "#F54EA2",
          "#41b6e6",
          "#FE9000",
          "#7ebc59",
          "#8134af",
        ],
        hoverBackgroundColor: [
          "#b9006e",
          "#005792",
          "#C1292E",
          "#2b9464",
          "#42218E",
        ],
      },
    ],
  };
};
export default class App extends React.Component {
  render() {
    const formatNumber = (num) => {
      return num.toString("").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
    return (
      <>
      <p className='text-center label' style={{fontSize: '0.75rem'}}>Employees by designation</p>
      <Pie
        data={createData(this.props.data)}
        width={100}
        height={100}
        options={{
          title: {
            display: true,
            text: "Arm Sales Notifications by Region",
            fontSize: 25,
          },
          // responsive: true,
          legend: {
            display: true,
            position: "bottom",
            labels: {
              fontSize: 20, //labels font size
              fontColor: "#000",
            },
          },
          plugins: {
            datalabels: {
              font: {
                size: 300,
              },
            },
          },
          tooltips: {
            bodyFontSize: 20,
            callbacks: {
              label: function (tooltipItem, data) {
                // console.log({ tooltipItem, data });
                const label = data.labels[tooltipItem.index]; //index gives the the index of this data item in the dataset
                // console.log(data.labels[2])
                const value = formatNumber(
                  data.datasets[tooltipItem.datasetIndex].data[
                    tooltipItem.index
                  ] //finding the matching data item in dataset
                );

                return `${label}: $${value}`;
              },
            },
          },
        }}
      />
      </>
    );
  }
}
