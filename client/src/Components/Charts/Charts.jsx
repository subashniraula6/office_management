import React, { useState } from "react"
import DonutChart from "./DonutChart"
import { Container, Row, Col } from "react-bootstrap"
import axios from "../utils/axios"
import Spinner from "../Spinner/Spinner"
import BarDiagram from "./BarDiagram"
import CountPaper from "../Common/CountPaper/CountPaper"
import PeopleAltIcon from "@material-ui/icons/PeopleAlt"
import BirthdayAlert from "../BirthdayAlert/BirthdayAlert"

import TodaysLeavePaper from "../TodaysLeavePaper/TodaysLeavePaper"
import MyLeaveStatistics from "../MyLeaveStatistics/MyLeaveStatistics"
import PublicHolidays from "../PublicHolidays/PublicHolidays"

const Charts = () => {
	const [loading, setLoading] = useState(false)
	const [data, setData] = React.useState({})
	React.useEffect(() => {
		setLoading(true)
		axios.get("/api/dashboard").then((res) => {
			setLoading(false)
			setData(res.data.result)
		})
	}, [])
	if (loading) return <Spinner />
	return (
		<Container>
			<h3 className="mb-4 mx-2" style={{ color: "#ff6600" }}>
				Dashboard Overview
			</h3>
			<Row>
				<Col xl={3} lg={4} md={6}>
					<CountPaper
						icon={
							<PeopleAltIcon style={{ fontSize: "2.5rem", color: "#6F462B" }} />
						}
						label="Total Employees"
						value={data.user?.total}
					/>
				</Col>
				<Col xl={3} lg={4} md={6}>
					<CountPaper
						icon={
							<i
								className="fas fa-boxes text-info"
								style={{ fontSize: "1.8rem" }}
							/>
						}
						label="Total Inventories"
						value={data.inventory?.total}
					/>
				</Col>
				<Col xl={3} lg={4} md={6}>
					<CountPaper
						icon={
							<i
								className="fas fa-stopwatch text-warning"
								style={{ fontSize: "2rem" }}
							></i>
						}
						label="Total Servicings"
						value={data.servicing?.completed + data.servicing?.ongoing}
					/>
				</Col>
				<Col xl={3} lg={4} md={6}>
					<CountPaper
						icon={
							<i
								className="fas fa-tools text-primary"
								style={{ fontSize: "1.8rem" }}
							></i>
						}
						label="Total Repairs"
						value={data.repair?.total}
					/>
				</Col>
			</Row>
			<Row className="m-5">
				<Col xl={3} lg={6} xs={12} className="my-3">
					<BarDiagram data={data.inventory} label="Inventories by category" />
				</Col>
				<Col xl={2} lg={5} xs={10} className="my-3">
					<DonutChart data={data.user} />
				</Col>
			</Row>
			<hr />
			<Row className="justify-content-between">
				<BirthdayAlert birthdays={data.birthdays} />
				<TodaysLeavePaper />
			</Row>
			<Row className="justify-content-between">
				<MyLeaveStatistics />
				<PublicHolidays />
			</Row>
		</Container>
	)
}

export default Charts
