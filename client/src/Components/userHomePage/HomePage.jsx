import React, { useEffect } from "react"
import "./HomePage.css"
import { useDispatch, useSelector } from "react-redux"
import Spinner from "../Spinner/Spinner"
import { fetchCurrentUser } from "../Redux/actions/authActions"
import UserInventories from "./UserInventories"
import { Container } from "react-bootstrap"
import PublicHolidays from "../PublicHolidays/PublicHolidays"
import Card from "@material-ui/core/Card"
import MyLeaveStatistics from "../MyLeaveStatistics/MyLeaveStatistics"

export default function UserHomePage() {
	const dispatch = useDispatch()
	useEffect(() => {
		dispatch(fetchCurrentUser())
	}, [dispatch])
	const { isLoading, user } = useSelector((store) => store.authReducer)
	if (isLoading) return <Spinner />
	return (
		<Container>
			<Container
				className="container my-1 d-flex"
				style={{
					textTransform: "uppercase",
					fontFamily: "ubuntu",
					color: "grey",
					letterSpacing: "0.01rem",
				}}
			>
				<span className="welcome">Welcome,</span>
				<h3 className="mt-2 ms-2">{user && user.firstName}</h3>
			</Container>
			<hr />
			<Container>
				<h4 style={{ color: "#ff6600" }}>Your Inventories</h4>
				<UserInventories />
				<br />
				<br />
				<div className="d-flex justify-content-between">
					<MyLeaveStatistics id={user.id} />

					<PublicHolidays role={user.roles[0]} />
				</div>
				<br />
			</Container>
		</Container>
	)
}
