<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211201085355 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE leave_documents ADD leave_request_id INT NOT NULL');
        $this->addSql('ALTER TABLE leave_documents ADD CONSTRAINT FK_ED51A8F2F2E1C15D FOREIGN KEY (leave_request_id) REFERENCES leave_request (id)');
        $this->addSql('CREATE INDEX IDX_ED51A8F2F2E1C15D ON leave_documents (leave_request_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE leave_documents DROP FOREIGN KEY FK_ED51A8F2F2E1C15D');
        $this->addSql('DROP INDEX IDX_ED51A8F2F2E1C15D ON leave_documents');
        $this->addSql('ALTER TABLE leave_documents DROP leave_request_id');
    }
}
