<?php

namespace App\Utils;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;


trait ValidateUtils
{
    private $validator;
    private $serializer;

    public function __construct(ValidatorInterface $validator, SerializerInterface $serializer)
    {
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    public function validateInput($input)
    {
        $errors = $this->validator->validate($input);

        $err = [];
        if (count($errors) > 0) {
            $errorsString = $this->serializer->serialize($errors, 'json');
            $errArr = json_decode($errorsString, 1)['violations'];

            foreach ($errArr as $key => $item) {
                $err[$item['propertyPath']] = $item['title'];
            }
            return $err;
        }

        return $err;
    }
    
    public function validateFile($file)
    {
        $err = [];
        $mimeTypes = ["image/jpg", "image/jpeg", "image/png", "image/svg+xml"];
        $maxSize = 2;
        if (!in_array($file->getClientMimeType(), $mimeTypes)) {
            $err['mimeType'] = "Invalid format";
        }
        return $err;
    }
}
