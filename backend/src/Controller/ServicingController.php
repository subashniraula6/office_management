<?php

namespace App\Controller;

use App\Entity\Inventory;
use App\Entity\Servicing;
use App\Repository\AllocationRepository;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class ServicingController extends AbstractController
{
    /**
     * @Route("/api/admin/servicings", methods={"GET"}, name="get_servicings")
     */
    public function getAllServicing(EntityManagerInterface $em)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        parse_str($_SERVER['QUERY_STRING'], $result);
        $per_page = $result['per_page'];
        $page = $result['page'];
        $offset = $per_page * ($page - 1);
        $totalCount = count($this->getDoctrine()->getRepository(Servicing::class)->findBy(['isCurrentServicing' => true]));

        $servicings = $this->getDoctrine()
            ->getRepository(Servicing::class)
            ->findBy(['isCurrentServicing' => true], ['serviceAt' => 'ASC'], $per_page, $offset);
        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return null;
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'inventory', 'name', 'brand ', 'model', 'description', 'cost', 'remarks',
                'category', 'user', 'email', 'fullName', 'durationInMonth', 'serviceAt', 'inventoryId',
                'isCurrentServicing', 'inServicing', 'isCompleted', 'imagePath'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($servicings, 'json');
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data),
            'page' => $page,
            'totalCount' => $totalCount
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/api/inventories/{id}/servicings", methods={"GET"}, name="get_inventory_servicings")
     */
    public function getInventoryServicings($id, AllocationRepository $allocationRepository)
    {
        //Authorize admin or owner only
        $roles = $this->getUser()->getRoles();
        $allocation = $allocationRepository->findOneBy(['user' => $this->getUser()->getId(), 'inventory' => $id, 'isCurrentAllocation' => true]);
        // Check if current user is admin/owner
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)
            ->findOneBy(['inventoryId' => $id]);
        if (in_array('ROLE_ADMIN', $roles) || !empty($allocation)) {
            $servicings = $this->getDoctrine()
                ->getRepository(Servicing::class)
                ->findBy(['inventory' => $inventory->getId()], ['id' => 'DESC']);

            if (empty($servicings)) {
                $response = array(
                    'code' => 404,
                    'errors' => 'No Servicing',
                    'result' => null
                );
                return new JsonResponse($response, 404);
            }
            $encoders = [new JsonEncoder()];
            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    return null;
                },
                AbstractNormalizer::ATTRIBUTES => [
                    'id', 'inventory', 'name', 'brand ', 'model', 'description', 'cost', 'category',
                    'user', 'email', 'fullName', 'durationInMonth', 'serviceAt', 'inventoryId', 'isSent',
                    'sentAt', 'returnedAt', 'remarks', 'isCurrentServicing', 'inServicing', 'isCompleted'
                ]
            ];
            $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

            $serializer = new Serializer($normalizers, $encoders);
            $data = $serializer->serialize($servicings, 'json');
            $response = array(
                'code' => 200,
                'errors' => null,
                'result' => json_decode($data)
            );
            return new JsonResponse($response, 200);
        }
        $response = array(
            'code' => 401,
            'errors' => 'unauthorized',
            'result' => null
        );
        return new JsonResponse($response, 401);
    }

    /**
     * @Route("/api/admin/inventories/{id}/servicings", methods={"POST"}, name="new_servicing")
     */
    public function createServicing($id, Request $request)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $parameters = json_decode($request->getContent(), 1);

        $em = $this->getDoctrine()->getManager();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findOneBy(['inventoryId' => $id, 'isDisposed' => false]);

        if (empty($inventory)) {
            $response = array(
                'code' => 404,
                'errors' => 'No inventory',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }

        $servicing = $this->getDoctrine()
            ->getRepository(Servicing::class)->findOneBy(['inventory' => $inventory->getId(), 'isCurrentServicing' => true]);
        $servicing->setIsSent(true);
        $servicing->setSentAt($parameters['sentAt'] ? new \DateTime($parameters['sentAt']) : new \DateTime('NOW'));
        $inventory->setInServicing(true);
        $em->persist($inventory);
        $em->persist($servicing);
        $em->flush();

        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return null;
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'inventory', 'name', 'brand ', 'model', 'description', 'cost', 'remarks',
                'category', 'user', 'email', 'fullName', 'durationInMonth', 'serviceAt', 'inventoryId', 'sentAt', 'returnedAt',
                'isCurrentServicing', 'inServicing', 'isCompleted', 'isSent'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($servicing, 'json');
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/api/admin/inventories/{id}/servicings/return", methods={"POST"}, name="return_from_servicing")
     */
    public function returnFromServicing(Request $request, $id)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findOneBy(['inventoryId' => $id, 'isDisposed' => false]);

        if (empty($inventory)) {
            $response = array(
                'code' => 404,
                'errors' => 'No inventory',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        $parameters = json_decode($request->getContent(), true);
        $cost = $parameters['cost'];
        $remarks = $parameters['remarks'];

        $servicing = $this->getDoctrine()
            ->getRepository(Servicing::class)->findOneBy(['inventory' => $inventory->getId(), 'isCurrentServicing' => true]);
        $servicing->setReturnedAt($parameters['returnedAt'] ? new \DateTime($parameters['returnedAt']) : new \DateTime('NOW'));
        $inventory->setInServicing(false);
        $servicing->setCost($cost);
        $servicing->setIscurrentServicing(false);
        $servicing->setRemarks($remarks);
        $servicing->setIsCompleted(true);
        $em->persist($inventory);
        $em->persist($servicing);

        // Make new servicing               
        $servicingDuration = $servicing->getDurationInMonth();
        $newServicing = new Servicing();
        $newServicing->setDurationInMonth($servicingDuration);
        $date = new DateTime('NOW');
        $date->add(new DateInterval('P' . $servicingDuration . 'M'));
        $newServicing->setIsSent(false);
        $newServicing->setServiceAt($date);
        $newServicing->setInventory($inventory);
        $newServicing->setIsCurrentServicing(true);
        $newServicing->setIsCompleted(false);

        $em->persist($newServicing);
        $em->flush();

        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return null;
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'inventory', 'name', 'brand ', 'model', 'description', 'cost', 'remarks',
                'category', 'user', 'email', 'fullName', 'durationInMonth', 'serviceAt', 'inventoryId', 'sentAt', 'returnedAt',
                'isCurrentServicing', 'inServicing', 'isCompleted'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);
        $servicing = $serializer->serialize($servicing, 'json');
        $newServicing = $serializer->serialize($newServicing, 'json');

        $response = array(
            'code' => 200,
            'errors' => null,
            'servicing' => json_decode($servicing),
            'newServicing' => json_decode($newServicing)
        );
        return new JsonResponse($response, 200);
    }
}
