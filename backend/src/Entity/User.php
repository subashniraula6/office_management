<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\Email(message="Please enter valid email")
     * @Assert\Regex(
     * pattern="/@wolfmatrix.com/",
     * message="Email must have wolfmatrix domain"
     * )
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;
    /**
     * @ORM\ManyToOne(targetEntity=Role::class, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $roles;

    /**
     * @Assert\NotBlank
     * @Assert\Regex(
     *  pattern="/(?=.*[A-Z](?=.*[a-z])(?=.*[0-9]).{7,})/",
     *  message="Password must be 7 characters long and contain atleast one digit, uppercase letter, lowercase letter"
     * )
     * @ORM\Column(type="string", length=250)
     * @var string The hashed password
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $hasLeft;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;


    /**
     * @ORM\OneToMany(targetEntity=Allocation::class, mappedBy="user", orphanRemoval=true)
     */
    private $allocations;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $employeeId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateOfBirth;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=10)
     */
    private $gender;

    /**
     * @ORM\OneToOne(targetEntity=ContactRelation::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $contactRelation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $addedBy;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="datetime")
     */
    private $joinedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $leftAt;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $designation;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=100)
     */
    private $department;

    /**
     * @ORM\OneToOne(targetEntity=UserInfo::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $userInfo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $recoveryToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $tokenExpiresAt;

    /**
     * @ORM\OneToMany(targetEntity=Request::class, mappedBy="user")
     */
    private $requests;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="user")
     */
    private $documents;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $probationEndAt;





    public function __construct()
    {
        $this->inventories = new ArrayCollection();
        $this->allocations = new ArrayCollection();
        $this->requests = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getHasLeft(): bool
    {
        return $this->hasLeft;
    }

    public function setHasLeft(bool $hasLeft): self
    {
        $this->hasLeft = $hasLeft;
        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getLeftAt(): ?string
    {
        if ($this->leftAt) {
            return $this->leftAt->format("F j, Y");
        } else return null;
    }

    public function setLeftAt(?\Datetime $leftAt): self
    {
        $this->leftAt = $leftAt;

        return $this;
    }
    public function removeLeftAt(): self
    {
        $this->leftAt = null;

        return $this;
    }

    // Userinterface and PasswordAuthenticatedUserInterface
    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }


    /**
     * @see UserInterface
     */
    public function getRole(): ?Role
    {
        return $this->roles;
    }

    public function getRoles(): ?array
    {
        return $this->roles->getName();
    }

    public function setRoles(?Role $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getCreatedAt(): ?string
    {
        return $this->createdAt->format("F j, Y");
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAddedBy(): ?string
    {
        return $this->addedBy;
    }

    public function setAddedBy(string $addedBy): self
    {
        $this->addedBy = $addedBy;

        return $this;
    }

    /**
     * @return Collection|Allocation[]
     */
    public function getAllocations(): Collection
    {
        return $this->allocations;
    }

    public function addAllocation(Allocation $allocation): self
    {
        if (!$this->allocations->contains($allocation)) {
            $this->allocations[] = $allocation;
            $allocation->setUser($this);
        }

        return $this;
    }

    public function removeAllocation(Allocation $allocation): self
    {
        if ($this->allocations->removeElement($allocation)) {
            // set the owning side to null (unless already changed)
            if ($allocation->getUser() === $this) {
                $allocation->setUser(null);
            }
        }

        return $this;
    }

    public function getEmployeeId(): ?string
    {
        return $this->employeeId;
    }

    public function setEmployeeId(string $employeeId): self
    {
        $this->employeeId = $employeeId;

        return $this;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth->format("F j, Y");
    }

    public function setDateOfBirth(\DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getContactRelation(): ?ContactRelation
    {
        return $this->contactRelation;
    }

    public function setContactRelation(ContactRelation $contactRelation): self
    {
        // set the owning side of the relation if necessary
        if ($contactRelation->getUser() !== $this) {
            $contactRelation->setUser($this);
        }

        $this->contactRelation = $contactRelation;

        return $this;
    }

    public function getJoinedAt(): ?string
    {
        return $this->joinedAt->format("F j, Y");
    }

    public function setJoinedAt(\DateTimeInterface $joinedAt): self
    {
        $this->joinedAt = $joinedAt;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getDepartment(): ?string
    {
        return $this->department;
    }

    public function setDepartment(string $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getUserInfo(): ?UserInfo
    {
        return $this->userInfo;
    }

    public function setUserInfo(UserInfo $userInfo): self
    {
        // set the owning side of the relation if necessary
        if ($userInfo->getUser() !== $this) {
            $userInfo->setUser($this);
        }

        $this->userInfo = $userInfo;

        return $this;
    }

    public function getRecoveryToken(): ?string
    {
        return $this->recoveryToken;
    }

    public function setRecoveryToken(?string $recoveryToken): self
    {
        $this->recoveryToken = $recoveryToken;

        return $this;
    }

    public function getTokenExpiresAt(): ?\DateTime
    {
        return $this->tokenExpiresAt;
    }

    public function setTokenExpiresAt(?\DateTime $tokenExpiresAt): self
    {
        $this->tokenExpiresAt = $tokenExpiresAt;

        return $this;
    }

    /**
     * @return Collection|Request[]
     */
    public function getRequests(): Collection
    {
        return $this->requests;
    }

    public function addRequest(Request $request): self
    {
        if (!$this->requests->contains($request)) {
            $this->requests[] = $request;
            $request->setUser($this);
        }

        return $this;
    }

    public function removeRequest(Request $request): self
    {
        if ($this->requests->removeElement($request)) {
            // set the owning side to null (unless already changed)
            if ($request->getUser() === $this) {
                $request->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setUser($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getUser() === $this) {
                $document->setUser(null);
            }
        }

        return $this;
    }

    public function getProbationEndAt(): ?string
    {
        if ($this->probationEndAt) {
            return $this->probationEndAt->format("Y-m-d");
        } else {
            return $this->probationEndAt;
        }
    }

    public function setProbationEndAt(?\DateTimeInterface $probationEndAt): self
    {
        $this->probationEndAt = $probationEndAt;

        return $this;
    }
}
