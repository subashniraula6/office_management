<?php

namespace App\Repository;

use App\Entity\LeaveDocuments;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LeaveDocuments|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeaveDocuments|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeaveDocuments[]    findAll()
 * @method LeaveDocuments[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeaveDocumentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeaveDocuments::class);
    }

    // /**
    //  * @return LeaveDocuments[] Returns an array of LeaveDocuments objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LeaveDocuments
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function getDocuments($id)
    {
        $con = $this->getDoctrine()->getConnection();
        $sql = "SELECT `leave_documents`.`id`, `leave_documents`.`documents_path`, `leave_documents`.`leave_request_id` FROM `leave_documents` JOIN `leave_request` ON `leave_documents`.`leave_request_id` = `leave_request`.`id` WHERE `leave_documents`.`leave_request_id` = :id";
        $stmt = $con->prepare($sql);
        $params = array('id' => $id);
        $result = $stmt->executeQuery($params);
        return $result->fetchAllAssociative();
    }
}
