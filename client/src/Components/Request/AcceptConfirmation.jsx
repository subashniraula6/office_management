import React from "react";
import ConfirmModal from "../ConfirmModal/ConfirmModal";
import axios from "../utils/axios";
import { notify } from "../utils/toast";
import { Button } from "@material-ui/core";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";

const AcceptConfirmation = ({
  id,
  type,
  tableRef,
  inventoryId,
  setCurrentRowId,
}) => {
  const [showConfirmation, setShowConfirmation] = React.useState(false);
  function respondRequest(id, action, inventoryId, tableRef) {
    axios
      .put(
        `/api/admin/requests/${id}/${action}`,
        JSON.stringify({ inventoryId })
      )
      .then((res) => {
        setTimeout(() => {
          console.log(tableRef);
          tableRef.current && tableRef.current.onQueryChange();
        }, 400);
        if (action === "accept") {
          notify("success", "Request accepted!");
        } else if (action === "reject") {
          notify("success", "Request rejected!");
        }
      })
      .catch((err) => console.log(err));
  }
  return (
    <>
      <ConfirmModal
        content={`Are you sure you want to ${
          type === "repair"
            ? "send to repair"
            : type === "repair"
            ? "send to repair"
            : "provide new inventory"
        } ?`}
        open={showConfirmation}
        setOpen={setShowConfirmation}
        onConfirm={() => respondRequest(id, "accept", inventoryId)}
      />
    </>
  );
};

export default AcceptConfirmation;
