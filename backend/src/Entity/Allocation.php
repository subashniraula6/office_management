<?php

namespace App\Entity;

use App\Repository\AllocationRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AllocationRepository::class)
 */
class Allocation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Inventory::class, inversedBy="allocation")
     * @ORM\JoinColumn(nullable=false, name="inventory_id", referencedColumnName="id")
     */
    private $inventory;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="allocation")
     * @ORM\JoinColumn(nullable=false, name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $allocatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $allocatedBy;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCurrentAllocation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $unallocatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unallocatedBy;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $notes;

    public function __construct()
    {
        $this->allocatedAt = new DateTime('NOW');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): self
    {
        $this->inventory = $inventory;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAllocatedAt(): ?string
    {
        return $this->allocatedAt->format("F j, Y");
    }

    public function setAllocatedAt(\DateTimeInterface $allocatedAt): self
    {
        $this->allocatedAt = $allocatedAt;

        return $this;
    }

    public function getAllocatedBy(): ?string
    {
        return $this->allocatedBy;
    }

    public function getIsCurrentAllocation(): ?bool
    {
        return $this->isCurrentAllocation;
    }

    public function setIsCurrentAllocation(?bool $isCurrentAllocation): self
    {
        $this->isCurrentAllocation = $isCurrentAllocation;
        
        return $this;
    }

    public function setAllocatedBy(?string $allocatedBy): self
    {
        $this->allocatedBy = $allocatedBy;

        return $this;
    }

    public function getUnallocatedAt(): ?string
    {
        return $this->unallocatedAt ? $this->unallocatedAt->format("F j, Y"): null;
    }

    public function setUnallocatedAt(?\DateTimeInterface $unallocatedAt): self
    {
        $this->unallocatedAt = $unallocatedAt;

        return $this;
    }

    public function getUnallocatedBy(): ?string
    {
        return $this->unallocatedBy;
    }

    public function setUnallocatedBy(string $unallocatedBy): self
    {
        $this->unallocatedBy = $unallocatedBy;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }
}
