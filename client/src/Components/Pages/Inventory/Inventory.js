import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getInventory } from "../../Redux/actions/inventoryactions";
import { useParams } from "react-router-dom";
import Spinner from "../../Spinner/Spinner";
import "react-toastify/dist/ReactToastify.css";
import InventoryDetails from "./InventoryDetails";
import InventoryProfile from "./InventoryProfile";
import { Grid } from "@material-ui/core";

export default function Inventory() {
  const dispatch = useDispatch();
  const inventoryReducer = useSelector((store) => store.inventoryReducer);
  const { isLoading, inventory } = inventoryReducer;
  const params = useParams();
  const id = params.id;

  useEffect(() => {
    dispatch(getInventory(id));
  }, [dispatch, id]);
  if (isLoading || !inventory) return <Spinner />;
  return (
    <>
      <div className="p-4">
        <Grid container spacing={3} justifyContent="center">
          <Grid item lg={3} md={5} xs={12}>
            <InventoryProfile inventory={inventory} />
          </Grid>
          <Grid item lg={9} md={7} xs={12}>
            <InventoryDetails inventoryItem={inventory} />
          </Grid>
        </Grid>
      </div>
    </>
  );
}
