import React from "react"
import "./Login.css"
import { useState } from "react"
import { Link } from "react-router-dom"
import "react-toastify/dist/ReactToastify.css"
import { loginUser } from "../Redux/actions/authActions"
import { useDispatch, useSelector } from "react-redux"
import Redirector from "../Redirector/Redirector"
import { Container, Row, Col, Image, Form, FormGroup } from "react-bootstrap"
import { Button } from "@material-ui/core"
import Zoho from "./Zoho"
import PasswordInput from "../Common/Form/PasswordInput/PasswordInput"

const Login = function () {
	const [user, setUser] = useState({
		email: "",
		password: "",
	})

	const error = useSelector((store) => store.authReducer.error)

	const dispatch = useDispatch()
	const isAuthenticated = useSelector(
		(state) => state.authReducer.isAuthenticated
	)

	function handleChange(e) {
		const { name, value } = e.target
		setUser({ ...user, [name]: value })
	}

	function handleSubmit(e) {
		e.preventDefault()
		try {
			const credentials = { username: user.email, password: user.password }
			dispatch(loginUser(credentials))
		} catch (e) {
			console.log(e.message)
		}
	}
	if (isAuthenticated) return <Redirector />
	return (
		<>
			<Container className="leftContainer">
				<Row className="align-items-center g-5">
					<Col lg={6} className="left d-none d-lg-block">
						<Image className="img-fluid" src="./ims.png" />
						<p className="lead">
							WOMS keeps track of inventories assigined to our employees. Keeps
							employees info. Notifies inventories serviceing.
						</p>
					</Col>

					<Col lg={6} className="mt-lg-5 pt-5">
						<Form className="formContainer" onSubmit={handleSubmit} noValidate>
							<h3 className="h3" style={{ color: "#ff6600" }}>
								Log in
							</h3>
							<div className="form-group mt-2">
								<label>Email</label>
								<input
									type="email"
									className="form-control"
									placeholder="Enter email"
									name="email"
									value={user.email}
									onChange={handleChange}
								/>
							</div>
							<FormGroup className="mt-2">
								<PasswordInput
									value={user.password}
									handleChange={handleChange}
								/>
							</FormGroup>
							{error ? (
								<div className="form-group mt-2 text-danger">
									<i className="fas fa-exclamation-triangle m-2"></i>
									<label style={{ fontSize: "14px" }}>{error}</label>
								</div>
							) : null}
							<FormGroup className="my-3">
								<div className="form-group d-flex justify-content-start align-items-center">
									<input type="checkbox" className="form-check-input" />
									<label
										className="custom-control-label m-2"
										htmlFor="customCheck1"
									>
										Remember me
									</label>
								</div>
							</FormGroup>

							<Button
								type="submit"
								color="primary"
								variant="contained"
								style={{ color: "white" }}
							>
								Sign in
							</Button>
						</Form>
						<p className="forgot-password text-center mt-1">
							<Link to="/forgotpassword" className="forgotPW">
								Forgot password?
							</Link>
						</p>
						<Zoho />
					</Col>
				</Row>
			</Container>
		</>
	)
}

export default Login
