<?php

namespace App\Services;

use Gedmo\Sluggable\Util\Urlizer;

class FileUploader
{
    public function upload($file)
    {
        if ($file) {
            $validTypes = ["image/jpg", "image/jpeg", 'image/png'];
            $destination = 'images/';
            $originalFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $newFileName = Urlizer::urlize($originalFileName) . '-' . uniqid() . '.' . $file->guessExtension();
            $path = $file->move($destination, $newFileName);
            return $path->getPathName();
        }
    }
}
