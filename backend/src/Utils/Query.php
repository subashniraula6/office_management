<?php

namespace App\Utils;

use Doctrine\ORM\EntityManagerInterface;

trait Query
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    public function fetchInventoryCategories()
    {
        $con = $this->em->getConnection();
        $query = 'SELECT category.name as name, COUNT(category.name) as count
                    FROM inventory JOIN category ON inventory.category_id=category.id
                    GROUP BY category.name';
        $stmt = $con->prepare($query);
        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative();
    }
    public function fetchUserCategories()
    {
        $con = $this->em->getConnection();
        $query = 'SELECT user.designation as name, COUNT(user.designation) as count
                FROM user GROUP BY user.designation';
        $stmt = $con->prepare($query);
        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative();
    }

    public function fetchApprovers($id)
    {
        $con = $this->em->getConnection();
        $sql = "SELECT
        user.id as supervisorId,
        user.first_name,
        user.last_name,
        user.employee_id,
        approvers.id,
        approvers.admin_id,
        approvers.is_admin_approved,
        approvers.is_approved,
        approvers.leave_request_id,
        user_info.pp_image_path
        FROM
            approvers
        JOIN user ON approvers.user_id = user.id
        JOIN leave_request ON approvers.leave_request_id = leave_request.id
        JOIN user_info ON user.id = user_info.user_id
        WHERE
            (leave_request.id = :id)";
        $stmt = $con->prepare($sql);
        $params = array('id' => $id);
        $result = $stmt->executeQuery($params);
        $leaveRequest = $result->fetchAllAssociative();
        return $leaveRequest;
    }

    public function fetchDocuments($id)
    {
        $con = $this->em->getConnection();
        $sql = "SELECT
            ld.id,
            ld.documents_path
        from
            leave_documents ld
        right join leave_request lr on
            ld.leave_request_id = lr.id
        where
            lr.id = :id";
        $stmt = $con->prepare($sql);
        $params = array('id' => $id);
        $result = $stmt->executeQuery($params);
        $leaveDocuments = $result->fetchAllAssociative();
        return $leaveDocuments;
    }
}
