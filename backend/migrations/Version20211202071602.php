<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211202071602 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE approvers (id INT AUTO_INCREMENT NOT NULL, leave_request_id INT NOT NULL, designation VARCHAR(100) NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_64EC41DDF2E1C15D (leave_request_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE approvers ADD CONSTRAINT FK_64EC41DDF2E1C15D FOREIGN KEY (leave_request_id) REFERENCES leave_request (id)');
        $this->addSql('ALTER TABLE leave_request DROP approvers');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE approvers');
        $this->addSql('ALTER TABLE leave_request ADD approvers LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\'');
    }
}
