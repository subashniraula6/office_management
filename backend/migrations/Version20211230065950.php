<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211230065950 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE leave_request DROP start_first_half, DROP start_second_half, DROP end_first_half, DROP end_second_half');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE leave_request ADD start_first_half TINYINT(1) DEFAULT NULL, ADD start_second_half TINYINT(1) DEFAULT NULL, ADD end_first_half TINYINT(1) DEFAULT NULL, ADD end_second_half TINYINT(1) DEFAULT NULL');
    }
}
