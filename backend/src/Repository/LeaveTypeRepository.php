<?php

namespace App\Repository;

use App\Entity\LeaveType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LeaveType|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeaveType|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeaveType[]    findAll()
 * @method LeaveType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeaveTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeaveType::class);
    }

    // /**
    //  * @return LeaveType[] Returns an array of LeaveType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LeaveType
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    // public function findIdAndName()
    // {
    //     $qb = $this->createQueryBuilder('l')->select('l.id', 'l.leaveTypeName');
    //     $query = $qb->getQuery();
    //     $result = $query->getResult();
    //     return $result;
    // }

    public function findLeaveBeforeProbation()
    {
        $con = $this->getEntityManager()->getConnection();
        $sql = "SELECT
        lt.id,
        lt.leave_type_name,
        lt.leave_type_total_days,
        lt.is_payable
        FROM
            leave_type lt
        WHERE
            lt.is_payable = FALSE";
        $stmt = $con->prepare($sql);
        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative();
    }

    public function findLeaveAfterProbation($gender, $diffMonths)
    {

        $con = $this->getEntityManager()->getConnection();
        if ($diffMonths < 18) {
            $sql = "SELECT
                        lt.id,
                        lt.leave_type_name,
                        lt.leave_type_total_days,
                        lt.is_payable
                    FROM
                        leave_type lt
                    WHERE
                        lt.is_yearly_renewed = true
                        AND NOT (lt.leave_type_total_days = 0
                            AND lt.is_payable = 1)";
        } else {
            if ($gender == "female") {
                $sql = "SELECT
                lt.id,
                lt.leave_type_name,
                lt.leave_type_total_days,
                lt.is_payable
                FROM
                    leave_type lt
                WHERE
                    lt.leave_type_name != 'Paternity Leave'
                    AND NOT (lt.leave_type_total_days = 0
                            AND lt.is_payable = 1)";
            } else {
                $sql = "SELECT
                lt.id,
                lt.leave_type_name,
                lt.leave_type_total_days,
                lt.is_payable
                FROM
                    leave_type lt
                WHERE
                    lt.leave_type_name != 'Maternity Leave'
                    AND NOT (lt.leave_type_total_days = 0
                            AND lt.is_payable = 1)";
            }
        }


        $stmt = $con->prepare($sql);
        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative();
    }

    public function findComplementaryLeave()
    {
        $con = $this->getEntityManager()->getConnection();
        $sql = "SELECT
                    lt.id,
                    lt.leave_type_name,
                    lt.leave_type_total_days,
                    lt.is_payable
                FROM
                    leave_type lt
                WHERE
                    lt.leave_type_total_days = 0
                    AND lt.is_payable = 1";
        $stmt = $con->prepare($sql);
        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative()[0];
    }
}
