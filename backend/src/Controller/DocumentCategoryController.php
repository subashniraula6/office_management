<?php

namespace App\Controller;

use App\Entity\DocumentCategory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class DocumentCategoryController extends AbstractController
{
    /**
     * @Route("/api/users/documents/categories", name="get_document_categories", methods={"GET"})
     */
    public function getCategories(SerializerInterface $serializer): Response
    {
        $categories = $this->getDoctrine()->getRepository(DocumentCategory::class)->findAll();
        return new JsonResponse([
            'code' => 200,
            'errors' => null,
            'result' => json_decode($serializer->serialize($categories, 'json'))
        ]);
    }

    /**
     * @Route("/api/users/documents/categories", name="create_document_category", methods={"POST"})
     */
    public function createDocumentCategory(Request $request, SerializerInterface $serializer): Response
    {
        $em = $this->getDoctrine()->getManager();
        $parameters = json_decode($request->getContent(), 1);

        //Check existing Documentcategory
        $category = $em->getRepository(DocumentCategory::class)->findOneBy(['category' => $parameters['category']]);
        if (empty($category)) {
            $category = new DocumentCategory();
            $category->setCategory($parameters['category']);
            $em->persist($category);
            $em->flush();
        }
        return new JsonResponse([
            'code' => 200,
            'errors' => null,
            'result' => json_decode($serializer->serialize($category, 'json'))
        ]);
    }
}
