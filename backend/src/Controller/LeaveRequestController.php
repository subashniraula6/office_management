<?php

namespace App\Controller;

use App\Entity\Approvers;
use App\Entity\LeaveDocuments;
use App\Entity\LeaveRequest;
use App\Entity\LeaveStatus;
use App\Entity\LeaveType;
use App\Entity\NotifyLeave;
use App\Services\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Entity\UserInfo;
use App\Utils\LeaveCalculation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use App\Utils\Query;
use App\Services\Email;
use DateTime;
use Gedmo\Mapping\Driver\File;
use PDO;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\VarDumper\Cloner\Data;

// use Psr\Log\LoggerInterface;

class LeaveRequestController extends AbstractController
{
    use Query;
    use LeaveCalculation;
    /**
     * @Route("/leave/request", name="leave_request")
     */

    public function index(): Response
    {
        return $this->render('leave_request/index.html.twig', [
            'controller_name' => 'LeaveRequestController',
        ]);
    }
    /**
     * @Route("/api/leave/requests", name="get_leave_request_user", methods={"GET"})
     */

    public function getLeaveRequestUser()
    {
        parse_str($_SERVER['QUERY_STRING'], $result);
        $rowsPerPage = (int)$result['rowsPerPage'];
        $page = (int)$result['page'];
        $findBy = $result['findBy'];
        $findBy = json_decode($findBy, true);
        $startFindDate = $findBy['startDate'];
        $endFindDate = $findBy['endDate'];
        $offset = $rowsPerPage * $page;
        $userId = $this->getUser()->getId();

        $con = $this->getDoctrine()->getConnection();
        if ($startFindDate != "" || $endFindDate != "") {
            $sql = "SELECT COUNT(r.id) 
            FROM leave_request r WHERE r.user_id = :id
            AND r.date_of_leave BETWEEN :startFindDate AND :endFindDate";
            $stmt = $con->prepare($sql);
            $stmt->bindParam('id', $userId, PDO::PARAM_INT);
            $stmt->bindParam('startFindDate', $startFindDate);
            $stmt->bindParam('endFindDate', $endFindDate);
        } else {
            $sql = "SELECT COUNT(r.id) 
            FROM leave_request r WHERE r.user_id = :id";
            $stmt = $con->prepare($sql);
            $stmt->bindParam('id', $userId, PDO::PARAM_INT);
        }

        $count = $stmt->executeQuery()->fetchOne();

        $leaveDetails = $this->getDoctrine()->getRepository(LeaveRequest::class)->findLeaveDetailsUser($rowsPerPage, $offset, $userId, $startFindDate, $endFindDate);

        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->normalize($leaveDetails);

        $result = array_map(function ($a) {
            $id = $a['id'];
            $leaveTime = $a['leave_time'];
            $arrivalTime = $a['arrival_time'];
            $dateOfLeave = $a['date_of_leave'];
            $dateOfArrival = $a['date_of_arrival'];
            $durationHour = $this->calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
            $durationDay = $this->calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
            $approvers = $this->fetchApprovers($id);
            $leaveDocuments = $this->fetchDocuments($id);
            $a['leave_documents'] = $leaveDocuments;
            $a['approvers'] = $approvers;
            $apprId = [];
            foreach ($approvers as $appr) {
                array_push($apprId, $appr['supervisorId']);
            }
            $checkSupervisorLogin = in_array($approvers[0]['admin_id'], $apprId);
            if ($approvers[0]['admin_id']) {
                if ($checkSupervisorLogin == false) {
                    $adminInfo = $this->getDoctrine()->getRepository(UserInfo::class)->findOneBy(['user' => $approvers[0]['admin_id']]);
                    $admin = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $approvers[0]['admin_id']]);
                    $a['adminApprover'] = array(
                        'first_name' => $admin->getFirstName(),
                        'last_name' => $admin->getLastName(),
                        'pp_image_path' => $adminInfo->getPpImagePath(),
                        'is_approved' => $approvers[0]['is_admin_approved']
                    );
                }
            }
            $a['durationHour'] = $durationHour;
            $a['durationDay'] = $durationDay;
            return $a;
        }, $data);
        $arr_result = array('result' => $result, 'count' => $count);
        $arr_result = json_encode($arr_result);


        return new Response($arr_result, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/api/leave/request", name="make_leave_request", methods={"POST"})
     */

    public function makeLeaveRequest(Request $request, FileUploader $uploader, Email $email)
    {

        $input = $_POST;
        $holidayDates = $this->getHolidayDate();
        $isDateOnHoliday = in_array($input['dateOfLeave'], $holidayDates) || in_array($input['dateOfArrival'], $holidayDates);

        $leaveDuration =  $this->calculateByDays($input['leaveTime'], $input['arrivalTime'], $input['dateOfLeave'], $input['dateOfArrival']);
        $userId = $this->getUser()->getId();

        $applyLeaveRange = $this->getDatesFromRange($input['dateOfLeave'], $input['dateOfArrival']);




        $input['dateOfLeave'] = new \DateTime($input['dateOfLeave']);
        $input['dateOfArrival'] = new \DateTime($input['dateOfArrival']);
        $input['leaveTime'] = new \DateTime($input['leaveTime']);
        $input['arrivalTime'] = new \DateTime($input['arrivalTime']);
        $input['leaveType'] = (int)$input['leaveType'];
        $leaveStat = $this->findLeaveStatOfUser($input['leaveType'], $input['dateOfLeave'], $userId, 0);

        $leaveDates = $this->getDoctrine()->getRepository(LeaveRequest::class)->findLeaveDatesOfUser($userId, 0);

        for ($val = 0, $commonDates = array(); $val < count($leaveDates); $val++) {
            $range = $this->getDatesFromRange($leaveDates[$val]['date_of_leave'], $leaveDates[$val]['date_of_arrival']);
            array_push($commonDates, array_intersect($range, $applyLeaveRange));
        }

        $checkLeaveAlreadyApplied = (!empty(array_filter($commonDates)));
        $isYearlyRenewed = (bool)$this->getDoctrine()->getRepository(LeaveType::class)->findOneBy(['leaveTypeName' => $leaveStat['leaveTypeName']])->getIsYearlyRenewed();

        if ($isYearlyRenewed == false) {
            $checkLeaveFinished = $leaveDuration > $leaveStat['remainingDay'];
        } else {
            $checkLeaveFinished = false;
        }

        $newFY = clone $input['dateOfArrival'];

        $newFY->setDate($newFY->format('Y'), 07, 01);


        $isDateOnTwoFiscalYear = $input["dateOfLeave"] < $newFY && $input['dateOfArrival'] >= $newFY;

        $isDateOnWeekend = $input['dateOfLeave']->format('w') == 0 || $input['dateOfLeave']->format('w') == 6 || $input['dateOfArrival']->format('w') == 0 || $input['dateOfArrival']->format('w') == 6;

        if ($isDateOnHoliday == true || $isDateOnWeekend == true) {
            return new JsonResponse("Date lies on holiday or weekend", 400);
        } else {
            if ($isDateOnTwoFiscalYear == true) {
                return new Response("Dates can't lie on two fiscal year", 400);
            } else {
                if ($input['dateOfLeave'] > $input['dateOfArrival']) {
                    return new Response("Invalid Date", 400);
                } else {

                    if ($checkLeaveAlreadyApplied == true) {
                        return new Response("Leave had already been applied by you for same date", 400);
                    } else {

                        if ($checkLeaveFinished == true) {
                            return new Response('Selected leave might be finished or duration greater than remaining', 400);
                        } else {
                            $em = $this->getDoctrine()->getManager();
                            $leaveRequest = new LeaveRequest();

                            $leaveDescription = $input['leaveDescription'];
                            $dateOfLeave = $input['dateOfLeave'];
                            $dateOfArrival = $input['dateOfArrival'];
                            $leaveTime = $input['leaveTime'];
                            $arrivalTime = $input['arrivalTime'];
                            $periodOfDay = $input['periodOfDay'];
                            $leaveMode = $input['leaveMode'];

                            $filesCount = (int)$input['filesCount'];


                            $leaveRequest->setLeaveDescription($leaveDescription);
                            $leaveRequest->setDateOfLeave($dateOfLeave);
                            $leaveRequest->setDateOfArrival($dateOfArrival);
                            $leaveRequest->setLeaveTime($leaveTime);
                            $leaveRequest->setArrivalTime($arrivalTime);
                            $leaveRequest->setPeriodOfDay($periodOfDay);
                            $leaveRequest->setLeaveMode($leaveMode);


                            $leaveRequest->setUser($this->getUser());

                            $leaveType = $this->getDoctrine()->getRepository(LeaveType::class)->findOneBy(['id' => $input['leaveType']]);
                            $leaveRequest->setLeaveType($leaveType);

                            $leaveStatus = $this->getDoctrine()->getRepository((LeaveStatus::class))->findOneBy(['id' => '1']);
                            $leaveRequest->setLeaveStatus($leaveStatus);


                            for ($index = 0; $index < $filesCount; $index++) {
                                $leaveDocuments = new LeaveDocuments();
                                $leaveDoc = $request->files->get('leaveDocuments' . $index);
                                $path = $uploader->upload($leaveDoc);
                                $leaveDocuments->setDocumentsPath($path);
                                $leaveDocuments->setLeaveRequest($leaveRequest);
                                $em->persist($leaveDocuments);
                            }

                            $em->persist($leaveRequest);

                            if ($leaveRequest) {
                                $appr = $input['approvers'];
                                $appr_array = explode(',', $appr);
                                // dd($appr_array);
                                // $empIdCEO = $this->getDoctrine()->getRepository(User::class)->findOneBy(['designation' => 'CEO'])->getEmployeeId();
                                // $CTO = $this->getDoctrine()->getRepository(User::class)->findOneBy(['designation' => 'CTO']);
                                // if ($CTO) {
                                //     $empIdCTO = $CTO->getEmployeeId();
                                //     $checkCTO = in_array($empIdCTO, $appr_array);
                                // } else {
                                //     $checkCTO = false;
                                // }
                                // $checkSupervisor = in_array($empIdCEO, $appr_array) || $checkCTO;
                                // if ($checkSupervisor !== true) {
                                //     array_push($appr_array, $empIdCEO);
                                // }

                                for ($index = 0; $index < count($appr_array); $index++) {
                                    $approvers = new Approvers();

                                    $approvers->setLeaveRequest($leaveRequest);
                                    $approvers->setIsApproved(false);
                                    $approvers->setIsAdminApproved(false);
                                    $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['employeeId' => $appr_array[$index]]);
                                    $approvers->setUser($user);
                                    $em->persist($approvers);
                                }
                            }
                            $em->flush();

                            $userName = $this->getUser()->getFirstName() . " " . $this->getUser()->getLastName();
                            $supervisorForMail = explode(',', $input['approvers']);
                            $approversForMail = [];
                            $adminMail = $this->getDoctrine()->getRepository(User::class)->findBy(['roles' => 1]);
                            foreach ($adminMail as $am) {
                                array_push($approversForMail, $am->getEmail());
                            }
                            foreach ($supervisorForMail as $s) {
                                $emailAppr = $this->getDoctrine()->getRepository(User::class)->findOneBy(['employeeId' => $s])->getEmail();
                                array_push($approversForMail, $emailAppr);
                            }
                            $to = array_unique($approversForMail);

                            $email->sendLeaveRequestMail($userName, $to);

                            return new JsonResponse($leaveRequest);
                        }
                    }
                }
            }
        }
    }
    /**
     * @Route("api/allLeave/pendingRequests", name="get_all_leave_pending_requests", methods="GET" )
     */

    public function getAllLeavePendingRequest(EntityManagerInterface $em)
    {
        parse_str($_SERVER['QUERY_STRING'], $result);
        $rowsPerPage = (int)$result['rowsPerPage'];
        $page = (int)$result['page'];

        $offset = $rowsPerPage * $page;

        $con = $em->getConnection();
        $sql = 'SELECT
        COUNT(lr.id)
        FROM
        leave_request lr
        JOIN leave_status ls ON
        ls.id = lr.leave_status_id
        WHERE
        ls.name = "Pending"';
        $stmt = $con->prepare($sql);
        $count_res = $stmt->executeQuery();
        $count = $count_res->fetchOne();

        $userId = $this->getUser()->getEmployeeId();

        $allLeaveDetails = $this->getDoctrine()->getRepository(LeaveRequest::class)->findAllLeavePendingRequests($offset, $rowsPerPage);

        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data =  $serializer->normalize($allLeaveDetails);

        $resultData = array_map(function ($a) {
            $id = $a['id'];
            $leaveTime = $a['leave_time'];
            $arrivalTime = $a['arrival_time'];
            $dateOfLeave = $a['date_of_leave'];
            $dateOfArrival = $a['date_of_arrival'];
            $durationHour = $this->calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
            $durationDay = $this->calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
            $approvers = $this->fetchApprovers($id);
            $leaveDocuments = $this->fetchDocuments($id);
            $a['leave_documents'] = $leaveDocuments;
            $a['approvers'] = $approvers;
            $a['durationHour'] = $durationHour;
            $a['durationDay'] = $durationDay;
            return $a;
        }, $data);

        $arr_result = array('result' => $resultData, 'userId' => $userId, 'count' => $count);
        $arr_result = json_encode($arr_result);



        return new Response($arr_result, 200, ["Content-Type" => "application/json"]);
    }
    /**
     * @Route("api/pending/leaveRequest/userProfile", name="get_pending_leave_request_for_supervisor", methods="GET")
     */

    public function getPendingLeaveRequestInUserProfile()
    {
        $userId = $this->getUser()->getId();
        $con = $this->getDoctrine()->getConnection();
        $sql = 'SELECT
                    leave_request.id,
                    leave_request.leave_description,
                    leave_request.date_of_leave,
                    leave_request.leave_time,
                    leave_request.arrival_time,
                    leave_request.date_of_arrival,
                    leave_request.decline_message,
                    leave_type.leave_type_name,
                    leave_type.leave_type_total_days,
                    leave_status.name,
                    user.first_name,
                    user.last_name
                FROM
                    leave_request
                JOIN leave_type ON
                    leave_request.leave_type_id = leave_type.id
                JOIN leave_status ON
                    leave_request.leave_status_id = leave_status.id
                JOIN user ON
                    leave_request.user_id = user.id
                JOIN approvers ON
                    leave_request.id = approvers.leave_request_id
                WHERE
                    leave_status.name = "Pending"
                    AND approvers.user_id = :userId';
        $stmt =  $con->prepare($sql);
        $stmt->bindParam('userId', $userId);
        $result = $stmt->executeQuery()->fetchAllAssociative();

        $resultData = array_map(function ($a) {
            $id = $a['id'];
            $leaveTime = $a['leave_time'];
            $arrivalTime = $a['arrival_time'];
            $dateOfLeave = $a['date_of_leave'];
            $dateOfArrival = $a['date_of_arrival'];
            $durationHour = $this->calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
            $durationDay = $this->calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
            $approvers = $this->fetchApprovers($id);
            $leaveDocuments = $this->fetchDocuments($id);
            $a['leave_documents'] = $leaveDocuments;
            $a['approvers'] = $approvers;
            $apprId = [];
            foreach ($approvers as $appr) {
                array_push($apprId, $appr['supervisorId']);
            }
            $checkSupervisorLogin = in_array($approvers[0]['admin_id'], $apprId);
            if ($approvers[0]['admin_id']) {
                if ($checkSupervisorLogin == false) {
                    $adminInfo = $this->getDoctrine()->getRepository(UserInfo::class)->findOneBy(['user' => $approvers[0]['admin_id']]);
                    $admin = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $approvers[0]['admin_id']]);
                    $a['adminApprover'] = array(
                        'first_name' => $admin->getFirstName(),
                        'last_name' => $admin->getLastName(),
                        'pp_image_path' => $adminInfo->getPpImagePath(),
                        'is_approved' => $approvers[0]['is_admin_approved']
                    );
                }
            }
            $a['durationHour'] = $durationHour;
            $a['durationDay'] = $durationDay;
            return $a;
        }, $result);

        $response = json_encode($resultData);
        return new Response($response, 200, ['Content-Type' => 'application/json']);
    }
    /**
     * @Route("api/allLeave/noPendingRequests", name="get_all_no_pending_request", methods="GET")
     */

    public function getAllLeaveNoPendingRequest(EntityManagerInterface $em)
    {
        parse_str($_SERVER['QUERY_STRING'], $result);
        $rowsPerPage = (int)$result['rowsPerPage'];
        $page = (int)$result['page'];
        $findBy = $result['findBy'];
        $findBy = json_decode($findBy, true);
        $startFindDate = $findBy['startDate'];
        $endFindDate = $findBy['endDate'];

        $offset = $rowsPerPage * $page;

        $con = $em->getConnection();
        if ($startFindDate != "" || $endFindDate != "") {
            $sql = 'SELECT
                    COUNT(lr.id)
                    FROM
                    leave_request lr
                    JOIN leave_status ls ON
                    ls.id = lr.leave_status_id
                    WHERE
                    ls.name != "Pending" AND
                    lr.date_of_leave BETWEEN :startFindDate AND :endFindDate';
            $stmt = $con->prepare($sql);
            $stmt->bindParam('startFindDate', $startFindDate);
            $stmt->bindParam('endFindDate', $endFindDate);
        } else {
            $sql = 'SELECT
                    COUNT(lr.id)
                    FROM
                    leave_request lr
                    JOIN leave_status ls ON
                    ls.id = lr.leave_status_id
                    WHERE
                    ls.name != "Pending"';
            $stmt = $con->prepare($sql);
        }
        $count_res = $stmt->executeQuery();
        $count = $count_res->fetchOne();


        $userId = $this->getUser()->getEmployeeId();

        $allLeaveDetails = $this->getDoctrine()->getRepository(LeaveRequest::class)->findAllLeaveNoPendingRequests($offset, $rowsPerPage, $startFindDate, $endFindDate);
        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data =  $serializer->normalize($allLeaveDetails);

        $resultData = array_map(function ($a) {
            $id = $a['id'];
            $leaveTime = $a['leave_time'];
            $arrivalTime = $a['arrival_time'];
            $dateOfLeave = $a['date_of_leave'];
            $dateOfArrival = $a['date_of_arrival'];
            $durationHour = $this->calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
            $durationDay = $this->calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
            $approvers = $this->fetchApprovers($id);
            $leaveDocuments = $this->fetchDocuments($id);
            $a['leave_documents'] = $leaveDocuments;
            $a['approvers'] = $approvers;
            $apprId = [];
            foreach ($approvers as $appr) {
                array_push($apprId, $appr['supervisorId']);
            }
            $checkSupervisorLogin = in_array($approvers[0]['admin_id'], $apprId);
            if ($approvers[0]['admin_id']) {
                if ($checkSupervisorLogin == false) {
                    $adminInfo = $this->getDoctrine()->getRepository(UserInfo::class)->findOneBy(['user' => $approvers[0]['admin_id']]);
                    $admin = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $approvers[0]['admin_id']]);
                    $a['adminApprover'] = array(
                        'first_name' => $admin->getFirstName(),
                        'last_name' => $admin->getLastName(),
                        'pp_image_path' => $adminInfo->getPpImagePath(),
                        'is_approved' => $approvers[0]['is_admin_approved']
                    );
                }
            }
            $a['durationHour'] = $durationHour;
            $a['durationDay'] = $durationDay;
            return $a;
        }, $data);

        $arr_result = array('result' => $resultData, 'userId' => $userId, 'count' => $count);
        $arr_result = json_encode($arr_result);



        return new Response($arr_result, 200, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("api/leaveReject/{id}", name="post_leaveReject", methods={"PUT"})
     */

    public function postDeclineMessage($id, Request $request, EntityManagerInterface $em)
    {
        $message = $request->getContent();
        if ($message) {
            $userId = $this->getUser()->getId();
            $approverId = $this->getDoctrine()->getRepository(Approvers::class)->findApproverForOneRequest($id, $userId);
            if ($approverId) {
                $approvers = $this->getDoctrine()->getRepository(Approvers::class)->findBy(['leaveRequest' => $id]);
                foreach ($approvers as $a) {
                    $a->setIsApproved(false);
                    $a->setIsAdminApproved(false);
                    $a->setAdminId(null);
                    $em->persist($a);
                }


                $parameter = json_decode($message, true);

                $leaveRequest = $this->getDoctrine()->getRepository(LeaveRequest::class)->findOneBy(['id' => $id]);
                $leaveStatus = $this->getDoctrine()->getRepository(LeaveStatus::class)->findOneBy(['id' => '3']);

                $leaveRequest->setDeclineMessage($parameter['declineMessage']);
                $leaveRequest->setLeaveStatus($leaveStatus);

                $em->persist($leaveRequest);
                $em->flush();
                if ($this->getUser()->getRoles()[0] == 'ROLE_ADMIN') {
                    parse_str($_SERVER['QUERY_STRING'], $result);

                    $rowsPerPage = (int)$result['rowsPerPage'];
                    $page = (int)$result['page'];

                    $offset = $rowsPerPage * $page;

                    $con = $em->getConnection();
                    $sqlp = 'SELECT
                            COUNT(lr.id)
                            FROM
                            leave_request lr
                            JOIN leave_status ls ON
                            ls.id = lr.leave_status_id
                            WHERE
                            ls.name = "Pending"';
                    $stmt = $con->prepare($sqlp);
                    $pending_count_res = $stmt->executeQuery();
                    $pending_count = $pending_count_res->fetchOne();

                    $sqlnp = 'SELECT
                            COUNT(lr.id)
                            FROM
                            leave_request lr
                            JOIN leave_status ls ON
                            ls.id = lr.leave_status_id
                            WHERE
                            ls.name != "Pending"';
                    $not_pending_count = $con->prepare($sqlnp)->executeQuery()->fetchOne();

                    $userId = $this->getUser()->getEmployeeId();




                    $allLeaveDetails = $this->getDoctrine()->getRepository(LeaveRequest::class)->findAllLeavePendingRequests($offset, $rowsPerPage);

                    $normalizers = [new ObjectNormalizer()];
                    $encoders = [new JsonEncoder()];
                    $serializer = new Serializer($normalizers, $encoders);
                    $data =  $serializer->normalize($allLeaveDetails);

                    $resultData = array_map(function ($a) {
                        $id = $a['id'];
                        $leaveTime = $a['leave_time'];
                        $arrivalTime = $a['arrival_time'];
                        $dateOfLeave = $a['date_of_leave'];
                        $dateOfArrival = $a['date_of_arrival'];
                        $durationHour = $this->calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                        $durationDay = $this->calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                        $approvers = $this->fetchApprovers($id);
                        $leaveDocuments = $this->fetchDocuments($id);
                        $a['leave_documents'] = $leaveDocuments;
                        $a['approvers'] = $approvers;
                        $a['durationHour'] = $durationHour;
                        $a['durationDay'] = $durationDay;
                        return $a;
                    }, $data);

                    $arr_result = array('result' => $resultData, 'userId' => $userId, 'count_pending' => $pending_count, 'count_not_pending' => $not_pending_count);
                    $arr_result = json_encode($arr_result);



                    return new Response($arr_result, 200, ["Content-Type" => "application/json"]);
                } else {

                    return $this->getPendingLeaveRequestInUserProfile();
                }
            } else {
                return new Response("you are not assigned for this request", 500);
            }
        } else {
            return new Response("if you reject, you should leave reject or decline message", 400);
        }
    }

    /**
     * @Route("api/leaveApprove/{id}", name="approve_leave_requests", methods={"PUT"})
     */

    public function approveLeaveRequest($id, EntityManagerInterface $em)
    {
        $leaveRequest = $this->getDoctrine()->getRepository(LeaveRequest::class)->findOneBy(['id' => $id]);
        $leaveStatusA = $this->getDoctrine()->getRepository(LeaveStatus::class)->findOneBy(['id' => '2']);
        $leaveStatusP = $this->getDoctrine()->getRepository(LeaveStatus::class)->findOneBy(['id' => '1']);
        $leaveTypeId = $leaveRequest->getLeaveType()->getId();
        $requestUserId = $leaveRequest->getUser()->getId();
        $dateOfLeave = $leaveRequest->getDateOfLeave();
        $dateOfArrival = $leaveRequest->getDateOfArrival();
        $leaveTime = $leaveRequest->getLeaveTime();
        $arrivalTime = $leaveRequest->getArrivalTime();

        $leaveDuration = $this->calculateByDays($leaveTime->format('H:i:s'), $arrivalTime->format('H:i:s'), $dateOfLeave->format('Y-m-d'), $dateOfArrival->format('Y-m-d'));
        $leaveStat = $this->findLeaveStatOfUser($leaveTypeId, $dateOfLeave, $requestUserId, 0);

        $isYearlyRenewed = $this->getDoctrine()->getRepository(LeaveType::class)->findOneBy(['id' => $leaveTypeId])->getIsYearlyRenewed();


        if ($isYearlyRenewed == false) {
            $checkLeaveFinished = $leaveDuration > $leaveStat['remainingDay'];
        } else {
            $checkLeaveFinished = false;
        }

        $logId = $this->getUser()->getId();
        $logRole = $this->getUser()->getRoles()[0];

        $supervisor = $this->getDoctrine()->getRepository(Approvers::class)->findApproverForOneRequest($id);
        for ($i = 0, $supervisorStatus = [], $supervisorId = []; $i < count($supervisor); $i++) {
            array_push($supervisorStatus, $supervisor[$i]['is_approved']);
            array_push($supervisorId, $supervisor[$i]['user_id']);
        }

        $checkSupervisorApprove = true;
        foreach ($supervisorStatus as $ss) {
            if (!($ss == true)) {
                $checkSupervisorApprove = false;
                break;
            }
        }

        $checkSupervisorLogin  = in_array($logId, $supervisorId);




        if ($checkSupervisorLogin === false && $checkSupervisorApprove === false) {
            return new Response("cant approve before supervisor", 400);
        } else {
            if ($checkLeaveFinished == true) {
                return new Response('Selected leave might be finished or duration greater than remaining', 400);
            }

            if ($logRole == 'ROLE_ADMIN') {
                $approvers = $this->getDoctrine()->getRepository(Approvers::class)->findBy(['leaveRequest' => $id]);
                foreach ($approvers as $a) {
                    $a->setIsAdminApproved(true);
                    $a->setAdminId($logId);
                    $em->persist($a);
                }
                if ($checkSupervisorLogin == true) {
                    $approvers = $this->getDoctrine()->getRepository(Approvers::class)->findOneBy(['leaveRequest' => $id, 'user' => $logId]);
                    $approvers->setIsApproved(true);
                    $em->persist($approvers);
                }

                $em->flush();
            } else {
                $approvers = $this->getDoctrine()->getRepository(Approvers::class)->findOneBy(['user' => $logId, 'leaveRequest' => $id]);
                $approvers->setIsApproved(true);
                $approvers->setIsAdminApproved(false);
                $em->persist($approvers);
                $em->flush();
            }


            $multiApproveStatus = $this->getDoctrine()->getRepository(Approvers::class)->findTotalStatus($id);

            for ($i = 0, $statusArray = []; $i < count($multiApproveStatus); $i++) {
                array_push($statusArray, $multiApproveStatus[$i]['is_approved'], $multiApproveStatus[$i]['is_admin_approved']);
            }

            if (in_array(false, $statusArray)) {

                $leaveRequest->setLeaveStatus($leaveStatusP);
                $em->persist($leaveRequest);
            } else {
                $leaveRequest->setLeaveStatus($leaveStatusA);
                $em->persist($leaveRequest);
            }

            $em->flush();
            if ($this->getUser()->getRoles()[0] == 'ROLE_ADMIN') {
                parse_str($_SERVER['QUERY_STRING'], $result);
                $rowsPerPage = (int)$result['rowsPerPage'];
                $page = (int)$result['page'];

                $offset = $rowsPerPage * $page;

                $con = $em->getConnection();
                $sqlp = 'SELECT
                                COUNT(lr.id)
                                FROM
                                leave_request lr
                                JOIN leave_status ls ON
                                ls.id = lr.leave_status_id
                                WHERE
                                ls.name = "Pending"';
                $stmt = $con->prepare($sqlp);
                $pending_count_res = $stmt->executeQuery();
                $pending_count = $pending_count_res->fetchOne();

                $sqlnp = 'SELECT
                                COUNT(lr.id)
                                FROM
                                leave_request lr
                                JOIN leave_status ls ON
                                ls.id = lr.leave_status_id
                                WHERE
                                ls.name != "Pending"';
                $not_pending_count = $con->prepare($sqlnp)->executeQuery()->fetchOne();

                $userId = $this->getUser()->getEmployeeId();




                $allLeaveDetails = $this->getDoctrine()->getRepository(LeaveRequest::class)->findAllLeavePendingRequests($offset, $rowsPerPage);

                $normalizers = [new ObjectNormalizer()];
                $encoders = [new JsonEncoder()];
                $serializer = new Serializer($normalizers, $encoders);
                $data =  $serializer->normalize($allLeaveDetails);

                $resultData = array_map(function ($a) {
                    $id = $a['id'];
                    $leaveTime = $a['leave_time'];
                    $arrivalTime = $a['arrival_time'];
                    $dateOfLeave = $a['date_of_leave'];
                    $dateOfArrival = $a['date_of_arrival'];
                    $durationHour = $this->calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                    $durationDay = $this->calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                    $approvers = $this->fetchApprovers($id);
                    $leaveDocuments = $this->fetchDocuments($id);
                    $a['leave_documents'] = $leaveDocuments;
                    $a['approvers'] = $approvers;
                    $apprId = [];
                    foreach ($approvers as $appr) {
                        array_push($apprId, $appr['supervisorId']);
                    }
                    $checkSupervisorLogin = in_array($approvers[0]['admin_id'], $apprId);
                    if ($approvers[0]['admin_id']) {
                        if ($checkSupervisorLogin == false) {
                            $adminInfo = $this->getDoctrine()->getRepository(UserInfo::class)->findOneBy(['user' => $approvers[0]['admin_id']]);
                            $admin = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $approvers[0]['admin_id']]);
                            $a['adminApprover'] = array(
                                'first_name' => $admin->getFirstName(),
                                'last_name' => $admin->getLastName(),
                                'pp_image_path' => $adminInfo->getPpImagePath(),
                                'is_approved' => $approvers[0]['is_admin_approved']
                            );
                        }
                    }
                    $a['durationHour'] = $durationHour;
                    $a['durationDay'] = $durationDay;
                    return $a;
                }, $data);

                $arr_result = array('result' => $resultData, 'userId' => $userId, 'count_pending' => $pending_count, 'count_not_pending' => $not_pending_count);
                $arr_result = json_encode($arr_result);



                return new Response($arr_result, 200, ["Content-Type" => "application/json"]);
            } else {
                return $this->getPendingLeaveRequestInUserProfile();
            }
        }
    }
    /**
     * @Route("api/leaveRequest/cancel/{id}", name="cancel_leave_request", methods={"DELETE"})
     */

    public function cancelRequest($id, EntityManagerInterface $em)
    {
        parse_str($_SERVER['QUERY_STRING'], $result);
        $rowsPerPage = (int)$result['rowsPerPage'];
        $page = (int)$result['page'];
        $findBy = $result['findBy'];
        $findBy = json_decode($findBy, true);
        $startFindDate = $findBy['startDate'];
        $endFindDate = $findBy['endDate'];

        $offset = $rowsPerPage * $page;
        if ($id) {
            $leaveDocuments = $em->getRepository(LeaveDocuments::class)->findBy(['leaveRequest' => $id]);

            $approvers = $em->getRepository(Approvers::class)->findBy(['leaveRequest' => $id]);
            $notifyLeave = $em->getRepository(NotifyLeave::class)->findOneBy(['leaveRequest' => $id]);
            if ($notifyLeave) {
                $em->remove($notifyLeave);
            }

            $leaveRequest = $this->getDoctrine()->getRepository(LeaveRequest::class)->findOneBy(['id' => $id]);
            foreach ($leaveDocuments as $key => $value) {
                $em->remove($value);
            }
            foreach ($approvers as $key => $value) {
                $em->remove($value);
            }


            $em->remove($leaveRequest);

            $em->flush();
            $userId = $this->getUser()->getId();
            $responseRequest = $this->getDoctrine()->getRepository(LeaveRequest::class)->findLeaveDetailsUser($rowsPerPage, $offset, $userId, $startFindDate, $endFindDate);
            $con = $this->getDoctrine()->getConnection();
            $sql = 'SELECT COUNT(leave_request.id) FROM leave_request WHERE leave_request.user_id = :userId';
            $stmt = $con->prepare($sql);
            $params = array('userId' => $userId);
            $count = $stmt->executeQuery($params)->fetchOne();

            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    return null;
                },
                AbstractNormalizer::ATTRIBUTES => [
                    'id', 'leaveDescription', 'leaveStatus', 'dateOfLeave', 'dateOfArrival', 'leaveType', 'declineMessage', 'leaveDocuments'
                ]
            ];
            $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];
            $encoders = [new JsonEncoder()];
            $serializer = new Serializer($normalizers, $encoders);
            $data = $serializer->normalize($responseRequest);
            $result = array_map(function ($a) {
                $id = $a['id'];
                $leaveTime = $a['leave_time'];
                $arrivalTime = $a['arrival_time'];
                $dateOfLeave = $a['date_of_leave'];
                $dateOfArrival = $a['date_of_arrival'];
                $durationHour = $this->calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                $durationDay = $this->calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                $approvers = $this->fetchApprovers($id);
                $leaveDocuments = $this->fetchDocuments($id);
                $a['leave_documents'] = $leaveDocuments;
                $a['approvers'] = $approvers;
                $apprId = [];
                foreach ($approvers as $appr) {
                    array_push($apprId, $appr['supervisorId']);
                }
                $checkSupervisorLogin = in_array($approvers[0]['admin_id'], $apprId);
                if ($approvers[0]['admin_id']) {
                    if ($checkSupervisorLogin == false) {
                        $adminInfo = $this->getDoctrine()->getRepository(UserInfo::class)->findOneBy(['user' => $approvers[0]['admin_id']]);
                        $admin = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $approvers[0]['admin_id']]);
                        $a['adminApprover'] = array(
                            'first_name' => $admin->getFirstName(),
                            'last_name' => $admin->getLastName(),
                            'pp_image_path' => $adminInfo->getPpImagePath(),
                            'is_approved' => $approvers[0]['is_admin_approved']
                        );
                    }
                }
                $a['durationHour'] = $durationHour;
                $a['durationDay'] = $durationDay;
                return $a;
            }, $data);
            $arr_result = array('result' => $result, 'count' => $count);
            $arr_result = json_encode($arr_result);


            return new Response($arr_result, 200, ['Content-Type' => 'application/json']);
        }
    }

    /**
     * @Route("api/leave/myLeaveDetails/edit/{id}", name="get_pending_leave_request_for_edit", methods="GET")
     */
    public function getPendingRequestForEditUser($id, EntityManagerInterface $em)
    {
        if ($id) {
            $con = $em->getConnection();
            $sql = "SELECT
            leave_request.id,
            leave_request.leave_description,
            leave_request.date_of_leave,
            leave_request.date_of_arrival,
            leave_request.decline_message,
            leave_request.leave_mode,
            leave_request.period_of_day,
            leave_request.leave_time,
            leave_request.arrival_time,
            leave_type.id AS leaveId,
            leave_type.leave_type_name,
            leave_type.leave_type_total_days,
            leave_status.name,
            user.id AS userId,
            user.first_name,
            user.last_name
            FROM
                leave_request
            INNER JOIN leave_type ON
                leave_request.leave_type_id = leave_type.id
            INNER JOIN leave_status ON
                leave_request.leave_status_id = leave_status.id
            INNER JOIN user ON
                leave_request.user_id = user.id
            WHERE
                leave_request.id = :id";
            $stmt = $con->prepare($sql);
            $params = array("id" => $id);
            $result =  $stmt->executeQuery($params);
            $leaveRequest = $result->fetchAllAssociative();



            $result = array_map(function ($a) {
                $id = $a['id'];
                $approvers = $this->fetchApprovers($id);
                $leaveDocuments = $this->fetchDocuments($id);
                $a['leave_documents'] = $leaveDocuments;
                $a['approvers'] = $approvers;
                return $a;
            }, $leaveRequest);


            $normalizers = [new ObjectNormalizer()];
            $encoders = [new JsonEncoder()];
            $serializer = new Serializer($normalizers, $encoders);

            $data = $serializer->serialize($result, 'json');


            return new Response($data, 200, ['Content-Type' => 'application/json']);
        }
    }

    /**
     * @Route("api/leaveTab/edit/{id}", name="get_leave_request_for_edit", methods={"GET"})
     */
    public function getRequestDetailsForEditAdmin($id, EntityManagerInterface $em)
    {
        if ($id) {
            $con = $em->getConnection();
            $sql = "SELECT
            leave_request.id,
            leave_request.leave_description,
            leave_request.date_of_leave,
            leave_request.date_of_arrival,
            leave_request.leave_time,
            leave_request.arrival_time,
            leave_request.decline_message,
            leave_request.leave_mode,
            leave_request.period_of_day,
            leave_type.id,
            leave_type.leave_type_name,
            leave_type.leave_type_total_days,
            leave_type.is_payable,
            leave_status.name,
            user.id AS userId,
            user.first_name,
            user.last_name,
            user.joined_at,
            user.probation_end_at,
            leave_documents.documents_path
            FROM
                leave_request
            INNER JOIN leave_type ON
                leave_request.leave_type_id = leave_type.id
            INNER JOIN leave_status ON
                leave_request.leave_status_id = leave_status.id
            INNER JOIN user ON
                leave_request.user_id = user.id
            LEFT JOIN leave_documents ON
                leave_documents.leave_request_id = leave_request.id
            WHERE
                leave_request.id = :id";
            $stmt = $con->prepare($sql);
            $params = array('id' => $id);
            $result = $stmt->executeQuery($params);
            $leaveRequest = $result->fetchAllAssociative()[0];
            $durationDay = $this->calculateByDays($leaveRequest['leave_time'], $leaveRequest['arrival_time'], $leaveRequest['date_of_leave'], $leaveRequest['date_of_arrival']);
            $result_arr = array('duration' => $durationDay, 'data' => $leaveRequest);



            $normalizers = [new ObjectNormalizer()];
            $encoders = [new JsonEncoder()];
            $serializer = new Serializer($normalizers, $encoders);
            $data = $serializer->serialize($result_arr, 'json');

            return new Response($data, 200, ['Content-Type' => 'application/json']);
        } else {
            return new Response("there is no request selected to edit", 400);
        }
    }
    /**
     * @Route("api/todays/leave", name="get_today_leave", methods="GET")
     */
    public function getTodaysLeave()
    {
        $todaysLeaves = $this->getDoctrine()->getRepository(LeaveRequest::class)->findByTodaysLeave();


        $result = array_map(function ($a) {
            $id = $a['id'];

            $approvers = $this->fetchApprovers($id);

            $a['approvers'] = $approvers;
            $apprId = [];
            foreach ($approvers as $appr) {
                array_push($apprId, $appr['supervisorId']);
            }
            $checkSupervisorLogin = in_array($approvers[0]['admin_id'], $apprId);
            if ($approvers[0]['admin_id']) {
                if ($checkSupervisorLogin == false) {
                    $adminInfo = $this->getDoctrine()->getRepository(UserInfo::class)->findOneBy(['user' => $approvers[0]['admin_id']]);
                    $admin = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $approvers[0]['admin_id']]);
                    $a['adminApprover'] = array(
                        'first_name' => $admin->getFirstName(),
                        'last_name' => $admin->getLastName(),
                        'pp_image_path' => $adminInfo->getPpImagePath(),
                        'is_approved' => $approvers[0]['is_admin_approved']
                    );
                }
            }
            return $a;
        }, $todaysLeaves);
        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data =  $serializer->serialize($result, "json");
        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("api/get/LeaveDoc/{requestId}", name="get_leave_doc_of_request", methods="GET")
     */
    public function getLeaveDocOfRequest($requestId)
    {
        $con = $this->getDoctrine()->getConnection();
        $sql = 'SELECT
        leave_documents.id, leave_documents.documents_path
        FROM
            leave_documents
        WHERE
            leave_documents.leave_request_id = :id';
        $stmt = $con->prepare($sql);
        $params = array('id' => $requestId);
        $resultResponse = $stmt->executeQuery($params)->fetchAllAssociative();

        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data =  $serializer->serialize($resultResponse, "json");

        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("api/add/LeaveDoc/{id}", name="add_leave_doc_after_request", methods="POST")
     */
    public function addLeaveDocAfterRequest($id, Request $request, FileUploader $uploader, EntityManagerInterface $em)
    {
        $leaveDoc = $request->files->get('leaveDocuments');
        $path = $uploader->upload($leaveDoc);
        $leaveDocuments = new LeaveDocuments();
        $leaveDocuments->setDocumentsPath($path);
        $leaveRequest = $this->getDoctrine()->getRepository(LeaveRequest::class)->findOneBy(['id' => $id]);
        $leaveDocuments->setLeaveRequest($leaveRequest);
        $em->persist($leaveDocuments);
        $em->flush();
        $con = $this->getDoctrine()->getConnection();
        $sql = 'SELECT
        leave_documents.id, leave_documents.documents_path
        FROM
            leave_documents
        WHERE
            leave_documents.leave_request_id = :id';
        $stmt = $con->prepare($sql);
        $params = array('id' => $id);
        $resultResponse = $stmt->executeQuery($params)->fetchAllAssociative();

        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data =  $serializer->serialize($resultResponse, "json");
        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("api/delete/LeaveDoc/{id}", name="delete_leave_doc_after_request", methods="DELETE")
     */
    public function deleteLeaveDocAfterRequest($id, Request $request, EntityManagerInterface $em)
    {
        $requestId = $request->getContent();

        $leaveDoc = $this->getDoctrine()->getRepository(LeaveDocuments::class)->findOneBy(['id' => $id]);
        $em->remove($leaveDoc);
        $em->flush();

        $con = $this->getDoctrine()->getConnection();
        $sql = 'SELECT
        leave_documents.id, leave_documents.documents_path
        FROM
            leave_documents
        WHERE
            leave_documents.leave_request_id = :id';
        $stmt = $con->prepare($sql);
        $params = array('id' => $requestId);
        $resultResponse = $stmt->executeQuery($params)->fetchAllAssociative();

        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data =  $serializer->serialize($resultResponse, "json");
        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("api/edit/leaveRequest/{id}", name="edit_leave_request", methods="PUT")
     */
    public function editLeaveRequest($id, Request $request, EntityManagerInterface $em)
    {
        $leaveDetails = $request->getContent();
        $leaveDetails = json_decode($leaveDetails, true);




        $ltID = (int)$leaveDetails['leaveTypeName'];
        $declineMessage = $leaveDetails['declineMessage'];
        $holidayDates = $this->getHolidayDate();
        $isDateOnHoliday = in_array($leaveDetails['dateOfLeave'], $holidayDates) || in_array($leaveDetails['dateOfArrival'], $holidayDates);
        $isDateOnWeekend = date_create($leaveDetails['dateOfLeave'])->format('w') == 0 || date_create($leaveDetails['dateOfLeave'])->format('w') == 6 || date_create($leaveDetails['dateOfArrival'])->format('w') == 0 || date_create($leaveDetails['dateOfArrival'])->format('w') == 6;

        $checkDateIsValid =  date_create($leaveDetails['dateOfLeave']) <= date_create($leaveDetails['dateOfArrival']);
        $leaveDuration =  $this->calculateByDays($leaveDetails['leaveTime'], $leaveDetails['arrivalTime'], $leaveDetails['dateOfLeave'], $leaveDetails['dateOfArrival']);
        $checkMidDateExists = array_key_exists('midDate', $leaveDetails);
        $checkLeaveTypeIIExists = array_key_exists('leaveTypeNameII', $leaveDetails);
        if ($checkMidDateExists == true) {
            $checkOnWeekend = date_create($leaveDetails['midDate'])->format('w') == 0 || date_create($leaveDetails['midDate'])->format('w') == 6;
            $checkOnHoliday = in_array($leaveDetails['midDate'], $holidayDates);
            $checkMidDateValid = date_create($leaveDetails['dateOfLeave']) < date_create($leaveDetails['midDate']) && date_create($leaveDetails['dateOfArrival']) > date_create($leaveDetails['midDate']);
        } else {
            $checkOnWeekend = false;
            $checkOnHoliday = false;
            $checkMidDateValid = true;
        }





        $userId = $leaveDetails['userId'];


        $leaveTypeExists = $this->getDoctrine()->getRepository(LeaveRequest::class)->findBy(['leaveType' => $ltID, 'user' => $userId, 'leaveStatus' => '2']);
        if (count($leaveTypeExists) > 1) {
            $leaveStat = $this->findLeaveStatOfUser($leaveDetails['leaveTypeName'], date_create($leaveDetails['dateOfLeave']), $userId, $id);
        } else {
            $leaveStat = $this->findLeaveStatOfUser($leaveDetails['leaveTypeName'], date_create($leaveDetails['dateOfLeave']), $userId, 0);
        }

        $leaveType = $this->getDoctrine()->getRepository(LeaveType::class)->findOneBy(['id' => $ltID]);
        $newFY = clone date_create($leaveDetails['dateOfArrival']);

        $newFY->setDate($newFY->format('Y'), 07, 01);
        $applyLeaveRange = $this->getDatesFromRange($leaveDetails['dateOfLeave'], $leaveDetails['dateOfArrival']);
        $leaveDates = $this->getDoctrine()->getRepository(LeaveRequest::class)->findLeaveDatesOfUser($userId, $id);

        for ($val = 0, $commonDates = array(); $val < count($leaveDates); $val++) {
            $range = $this->getDatesFromRange($leaveDates[$val]['date_of_leave'], $leaveDates[$val]['date_of_arrival']);
            array_push($commonDates, array_intersect($range, $applyLeaveRange));
        }


        $checkLeaveAlreadyApplied = (!empty(array_filter($commonDates)));



        $isDateOnTwoFiscalYear = date_create($leaveDetails["dateOfLeave"]) < $newFY && date_create($leaveDetails['dateOfArrival']) >= $newFY;
        $isYearlyRenewed = (bool)$this->getDoctrine()->getRepository(LeaveType::class)->findOneBy(['leaveTypeName' => $leaveStat['leaveTypeName']])->getIsYearlyRenewed();

        if ($isYearlyRenewed == false) {
            $checkLeaveFinished = $leaveDuration > $leaveStat['remainingDay'];
        } else {
            $checkLeaveFinished = false;
        }
        $requestToEdit  = $this->getDoctrine()->getRepository(LeaveRequest::class)->findOneBy(['id' => $id]);

        if ($checkDateIsValid  == false || $checkMidDateValid == false) {
            return new Response("Invalid Dates", 400);
        } else {
            if ($isDateOnHoliday == true || $checkOnHoliday == true) {
                return new Response("Date lies on holiday", 400);
            } else {
                if ($isDateOnWeekend == true || $checkOnWeekend == true) {
                    return new Response('Date lies in weekend', 400);
                } else {

                    if ($isDateOnTwoFiscalYear) {
                        return new Response("Date of Leave and Date of Arrival lies on two different fiscal year", 400);
                    } else {
                        if ($checkLeaveAlreadyApplied == true) {
                            return new Response("Another leave already been applied with same dates of this user", 400);
                        } else {
                            if ($checkLeaveFinished == true) {
                                return new Response("Selected leave might be finished or duration is greater than remaining!", 400);
                            } else {
                                $requestToEdit->setLeaveDescription($leaveDetails['leaveDescription']);
                                $requestToEdit->setDateOfLeave(new \DateTime($leaveDetails['dateOfLeave']));
                                if ($checkMidDateExists == true) {
                                    $requestToEdit->setDateOfArrival(new \DateTime($leaveDetails['midDate']));
                                    $requestToEdit->setArrivalTime(new \DateTime('09:00'));
                                } else {
                                    $requestToEdit->setDateOfArrival(new \DateTime($leaveDetails['dateOfArrival']));
                                    $requestToEdit->setArrivalTime(new \DateTime($leaveDetails['arrivalTime']));
                                }

                                if ($declineMessage) {
                                    $requestToEdit->setDeclineMessage($declineMessage);
                                }
                                $requestToEdit->setLeaveMode($leaveDetails['leaveMode']);
                                $requestToEdit->setPeriodOfDay($leaveDetails['periodOfDay']);
                                $requestToEdit->setLeaveTime(new \DateTime($leaveDetails['leaveTime']));

                                if ($ltID !== 0) {
                                    $requestToEdit->setLeaveType($leaveType);
                                }

                                $em->persist($requestToEdit);
                                $userId = $requestToEdit->getUser()->getId();




                                if ($checkMidDateExists == true) {
                                    $leaveRequest = new LeaveRequest();
                                    $leaveRequest->setLeaveDescription($leaveDetails['leaveDescription']);
                                    $leaveRequest->setDateOfLeave(new \DateTime($leaveDetails['midDate']));
                                    $leaveRequest->setDateOfArrival(new \DateTime($leaveDetails['dateOfArrival']));
                                    $leaveRequest->setLeaveTime(new \DateTime('09:00'));
                                    $leaveRequest->setArrivalTime(new \DateTime($leaveDetails['arrivalTime']));
                                    $leaveTypeNameII = $this->getDoctrine()->getRepository(LeaveType::class)->findOneBy(['id' => $leaveDetails['leaveTypeNameII']]);
                                    $leaveStatus = $this->getDoctrine()->getRepository(LeaveStatus::class)->findOneBy(['id' => '2']);
                                    $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $userId]);
                                    $leaveRequest->setLeaveType($leaveTypeNameII);
                                    $leaveRequest->setLeaveStatus($leaveStatus);
                                    $leaveRequest->setLeaveMode($leaveDetails['leaveMode']);
                                    $leaveRequest->setUser($user);
                                    $em->persist($leaveRequest);
                                    $prevApprovers = $this->getDoctrine()->getRepository(Approvers::class)->findBy(['leaveRequest' => $id]);
                                    // $prevLeaveDocuments = $this->getDoctrine()->getRepository(LeaveDocuments::class)->findBy(['leaveRequest' => $id]);

                                    foreach ($prevApprovers as $pa) {
                                        $approvers = new Approvers();
                                        $approvers->setUser($pa->getUser());
                                        $approvers->setIsApproved(true);
                                        $approvers->setAdminId($pa->getAdminId());
                                        $approvers->setIsAdminApproved(true);
                                        $approvers->setLeaveRequest($leaveRequest);
                                        $em->persist($approvers);
                                    }
                                    // foreach ($prevLeaveDocuments as $pld) {
                                    //     $leaveDocuments = new LeaveDocuments();
                                    //     $leaveDocuments->setDocumentsPath($pld->getDocumentsPath());
                                    //     $leaveDocuments->setLeaveRequest($leaveRequest);
                                    //     $em->persist($leaveDocuments);
                                    // }
                                }
                                $em->flush();



                                $defaultContext = [
                                    AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                                        return null;
                                    },
                                    AbstractNormalizer::ATTRIBUTES => [
                                        'id', 'leaveDescription', 'leaveStatus', 'dateOfLeave', 'dateOfArrival', 'leaveType'
                                    ]
                                ];
                                $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];
                                $encoders = [new JsonEncoder()];
                                $serializer = new Serializer($normalizers, $encoders);
                                $data =  $serializer->serialize($requestToEdit, "json");
                                return new Response($data, 200, ['Content-Type' => 'application/json']);
                            }
                        }
                    }
                }
            }
        }
    }
    /**
     * @Route("api/undo/leaveStatus/{id}", name="undo_leave_status", methods="PUT")
     */
    public function undoLeaveStatus($id, EntityManagerInterface $em)
    {

        $approvers = $this->getDoctrine()->getRepository(Approvers::class)->findBy(['leaveRequest' => $id]);
        foreach ($approvers as $a) {
            $a->setIsAdminApproved(false);
            $a->setAdminId(null);
            $a->setIsApproved(false);
            $em->persist($a);
        }
        $leaveRequest = $this->getDoctrine()->getRepository(LeaveRequest::class)->findOneBy(['id' => $id]);


        $leaveStatus = $this->getDoctrine()->getRepository(LeaveStatus::class)->findOneBy(['id' => '1']);
        $leaveRequest->setLeaveStatus($leaveStatus);
        $leaveRequest->setDeclineMessage("");



        $em->persist($leaveRequest);
        $em->flush();

        return new Response();
    }

    /**
     * @Route("api/leaveStat", name="get_leave_statistics_user", methods="POST")
     */
    public function getLeaveStatisticsOfUser(Request $request)
    {
        $leaveType = $request->getContent();
        $leaveType = json_decode($leaveType, true);

        $probationEndAt = $this->getUser()->getProbationEndAt();

        if ($probationEndAt === null) {
            $afterProbation = null;
        } else {
            $afterProbation = date_create($probationEndAt);
        }

        $userId = $this->getUser()->getId();
        $CFY = $this->findCurrentFiscalYear();
        $startFiscalYear = $CFY['startDate']->format('Y-m-d');
        $endFiscalYear = $CFY['endDate']->format('Y-m-d');
        $con =  $this->getDoctrine()->getConnection();

        $responseData = [];
        foreach ($leaveType as $lt) {
            $isYearlyRenewed = $this->getDoctrine()->getRepository(LeaveType::class)->findOneBy(['id' => $lt['id']])->getIsYearlyRenewed();
            if ($isYearlyRenewed == true) {
                $sql = 'SELECT
                leave_request.date_of_leave,
                leave_request.date_of_arrival,
                leave_request.leave_time,
                leave_request.arrival_time,
                leave_type.leave_type_name,
                leave_type.leave_type_total_days,
                leave_type.is_payable,
                leave_type.is_yearly_renewed
                FROM
                    leave_request
                INNER JOIN user ON
                    leave_request.user_id = user.id
                INNER JOIN leave_type ON
                    leave_request.leave_type_id = leave_type.id
                INNER JOIN leave_status ON
                    leave_request.leave_status_id = leave_status.id
                WHERE
                user.id = :userId
                AND leave_type.id = :id
                AND leave_request.date_of_arrival >= :startDate
                AND leave_request.date_of_leave >= :startDate
                AND leave_request.date_of_arrival <= :endDate
                AND leave_request.date_of_leave <=:endDate
                AND leave_status.name = "Approved"';
                $stmt = $con->prepare($sql);
                $params = array("id" => $lt['id'], "userId" => $userId, "startDate" => $startFiscalYear, "endDate" => $endFiscalYear);
            } else {
                $sql = 'SELECT
                leave_request.date_of_leave,
                leave_request.date_of_arrival,
                leave_request.leave_time,
                leave_request.arrival_time,
                leave_type.leave_type_name,
                leave_type.leave_type_total_days,
                leave_type.is_payable,
                leave_type.is_yearly_renewed
                FROM
                    leave_request
                INNER JOIN user ON
                    leave_request.user_id = user.id
                INNER JOIN leave_type ON
                    leave_request.leave_type_id = leave_type.id
                INNER JOIN leave_status ON
                    leave_request.leave_status_id = leave_status.id
                WHERE
                user.id = :userId
                AND leave_type.id = :id
                AND leave_status.name = "Approved"';
                $stmt = $con->prepare($sql);
                $params = array("id" => $lt['id'], "userId" => $userId);
            }
            $result = $stmt->executeQuery($params);
            $leaveStat = $result->fetchAllAssociative();
            if ($leaveStat) {
                $totalHour = 0;
                $totalDay = 0;

                foreach ($leaveStat as $ls) {

                    $leaveTime = $ls['leave_time'];
                    $arrivalTime = $ls['arrival_time'];
                    $dateOfArrival = $ls['date_of_arrival'];
                    $dateOfLeave = $ls['date_of_leave'];
                    $durationHour = $this->calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                    $durationDay = $this->calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                    $totalHour = $durationHour + $totalHour;
                    $totalDay = $durationDay + $totalDay;
                    $leaveName = $ls['leave_type_name'];
                    $leaveTotal = $ls['leave_type_total_days'];
                }
                $leaveTypeName = $leaveName;
                if ($afterProbation > $CFY['startDate'] && $afterProbation < $CFY['endDate']) {
                    $duration = $afterProbation->diff($CFY['endDate']);

                    // $usedDay = $duration->m * 0.5 + $totalDay;
                    // $usedHour = $duration->m * 4 + $totalHour;
                    $leaveTotalDays = $duration->m * ($leaveTotal / 12);
                    $leaveTotalHours = $leaveTotalDays * 8;
                    $remainingDay =  $leaveTotalDays - $totalDay;
                    $remainingHour = $leaveTotalHours - $totalHour;
                } else {
                    $leaveTotalDays = $leaveTotal;
                    $leaveTotalHours = $leaveTotal * 8;
                    $remainingDay =  $leaveTotalDays - $totalDay;
                    $remainingHour = $leaveTotalHours - $totalHour;
                }

                if ($leaveTotal == 0) {
                    $leaveTotalDays = $totalDay;
                    $leaveTotalHours = $totalHour;
                    $remainingDay = "NULL";
                    $remainingHour = "NULL";
                }

                $arr_data = array(
                    'usedDay' => round($totalDay, 3), 'usedHour' => round($totalHour, 3),
                    'leaveTypeName' => $leaveTypeName, 'remainingDay' => round($remainingDay, 3),
                    'remainingHour' => round($remainingHour, 3), 'totalDay' => round($leaveTotalDays, 3),
                    'totalHour' => round($leaveTotalHours, 3)
                );
                array_push($responseData, $arr_data);
            } else {
                $sql2 = 'SELECT * FROM leave_type WHERE leave_type.id=:id';
                $stmt2 = $con->prepare($sql2);
                $stmt2->bindParam("id", $lt['id'], PDO::PARAM_INT);
                $leaveType = $stmt2->executeQuery()->fetchAllAssociative()[0];
                if ($afterProbation > $CFY['startDate'] && $afterProbation < $CFY['endDate']) {
                    $duration = $afterProbation->diff($CFY['endDate']);

                    $leaveTotalDays =  $duration->m * ($leaveType['leave_type_total_days'] / 12);
                    $leaveTotalHours = $leaveTotalDays * 8;
                } else {
                    $leaveTotalDays = $leaveType['leave_type_total_days'];
                    $leaveTotalHours = $leaveType['leave_type_total_days'] * 8;
                }

                $arr_data = array(
                    'usedDay' => 0, 'usedHour' => 0,
                    'leaveTypeName' => $leaveType['leave_type_name'], 'remainingDay' =>  round($leaveTotalDays, 3),
                    'remainingHour' =>  round($leaveTotalHours, 3), 'totalDay' => round($leaveTotalDays, 3),
                    'totalHour' => round($leaveTotalHours, 3)
                );
                array_push($responseData, $arr_data);
            }
        }
        $normalizers = [new ObjectNormalizer()];
        $encoders =  [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($responseData, 'json');
        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("api/get/leaveStat/allUser", name="get_leave_stat_of_all_user", methods="POST")
     */
    public function getLeaveStatisticsOfAllUser(Request $request)
    {
        $searchBy = $request->getContent();
        $searchBy = json_decode($searchBy, "true");

        $startYear = (int)$searchBy['fiscalStartYear'];
        $endYear = $searchBy['fiscalEndYear'];
        $leaveTypeId = $searchBy['leaveType'];
        $customFiscal = $this->findCustomFiscalYear($startYear, $endYear);



        $sfy = $customFiscal['startDate']->format('Y-m-d');
        $efy = $customFiscal['endDate']->format('Y-m-d');

        $con = $this->getDoctrine()->getConnection();
        $sql = "SELECT user.id FROM user";
        $stmt = $con->prepare($sql);
        $users = $stmt->executeQuery()->fetchAllAssociative();
        $responseArray = [];
        for ($index = 0; $index < count($users); $index++) {
            $userId = $users[$index]['id'];
            // $userId = 2;

            $leaveDetails = $this->getDoctrine()->getRepository(LeaveRequest::class)->findLeaveDetailsBetweenYear($sfy, $efy, $leaveTypeId, $userId);



            if ($leaveDetails) {
                $totalHour = 0;
                $totalDay = 0;
                foreach ($leaveDetails as $ls) {
                    $leaveTime = $ls['leave_time'];
                    $arrivalTime = $ls['arrival_time'];
                    $dateOfArrival = $ls['date_of_arrival'];
                    $dateOfLeave = $ls['date_of_leave'];
                    $userName = $ls['first_name'] . ' ' . $ls['last_name'];
                    $leaveTypeName = $ls['leave_type_name'];
                    $pp = $ls['pp_image_path'];
                    $joinDate = $ls['joined_at'];
                    $probationEndAt = $ls['probation_end_at'];
                    $leaveTotal = (int)$ls['leave_type_total_days'];
                    $isYearlyRenew = (bool)$ls['is_yearly_renewed'];
                    $isPayable = (bool)$ls['is_payable'];
                    if ($isYearlyRenew == false) {
                        $leaveStat = $this->findLeaveStatOfUser($leaveTypeId, date_create($dateOfLeave), $userId, 0);
                        $vUsedDays = $leaveStat['usedDay'];
                        $vUsedHours = $leaveStat['usedHour'];
                    }

                    $durationHour = $this->calculateByHours($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                    $durationDay = $this->calculateByDays($leaveTime, $arrivalTime, $dateOfLeave, $dateOfArrival);
                    $totalHour = $durationHour + $totalHour;
                    $totalDay = $durationDay + $totalDay;
                }
                // if ($probationEndAt !== null) {
                //     $afterProbation = date_create($probationEndAt);
                //     $checkEndProbationInFY = $afterProbation >= date_create($sfy) && $afterProbation <= date_create($efy);
                // } else {
                //     $checkEndProbationInFY = false;
                // }
                $afterProbation = date_create($probationEndAt);
                $checkEndProbationInFY = $afterProbation >= date_create($sfy) && $afterProbation <= date_create($efy);


                if ($checkEndProbationInFY) {
                    $duration = $afterProbation->diff(date_create($efy));

                    $usedDay =  $totalDay;
                    $usedHour =  $totalHour;
                    if ($isPayable == false || $leaveTotal == 0) {
                        $remainingDay = "NULL";
                        $remainingHour = "NULL";
                        $leaveTotalDays = $usedDay;
                        $leaveTotalHours = $usedHour;
                    } else {
                        $leaveTotalDays = $duration->m * ($leaveTotal / 12);
                        $leaveTotalHours = $leaveTotalDays * 8;
                        $remainingDay =  $leaveTotalDays - $usedDay;
                        $remainingHour = $leaveTotalHours - $usedHour;
                    }
                    $leaveTotalDays = $duration->m * ($leaveTotal / 12);
                    $leaveTotalHours = $leaveTotalDays * 8;
                    $remainingDay =  $leaveTotalDays - $usedDay;
                    $remainingHour = $leaveTotalHours - $usedHour;
                } else {
                    $usedDay =  $totalDay;
                    $usedHour =  $totalHour;
                    $leaveTotalDays = $leaveTotal;
                    $leaveTotalHours = $leaveTotal * 8;
                    if ($isYearlyRenew == false) {
                        $remainingDay =  $leaveTotalDays - $vUsedDays;
                        $remainingHour = $leaveTotalHours - $vUsedHours;
                    } elseif ($isPayable == false) {
                        $remainingDay = "NULL";
                        $remainingHour = "NULL";
                        $leaveTotalDays = $usedDay;
                        $leaveTotalHours = $usedHour;
                    } else {
                        $remainingDay =  $leaveTotalDays - $usedDay;
                        $remainingHour = $leaveTotalHours - $usedHour;
                    }
                }
                if ($leaveTotal == 0) {
                    $remainingDay = "NULL";
                    $remainingHour = "NULL";
                    $leaveTotalDays = $totalDay;
                    $leaveTotalHours = $totalHour;
                }

                $res_array = array(
                    'remainingDay' => round($remainingDay, 3), 'remainingHour' => round($remainingHour, 3),
                    'usedDay' => round($usedDay, 3), 'usedHour' => round($usedHour, 3), 'profileP' => $pp,
                    'userName' => $userName, 'leaveTypeName' => $leaveTypeName,
                    'leaveTotalDays' => round($leaveTotalDays, 3), 'leaveTotalHours' => round($leaveTotalHours, 3)
                );
                array_push($responseArray, $res_array);
            }
        }
        $normalizers = [new ObjectNormalizer()];
        $encoders =  [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($responseArray, 'json');

        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/setLeaveNotify", name="set_leave_notify", methods="POST")
     */
    public function setLeaveNotification(EntityManagerInterface $em, Email $email)
    {
        $todaysLeave = $this->getTodaysLeave()->getContent();

        $leaveDetails = json_decode($todaysLeave, true);

        if ($leaveDetails) {
            $admin = $this->getDoctrine()->getRepository(User::class)->findBy(['roles' => 1]);
            $toMail = [];
            foreach ($admin as $ad) {
                array_push($toMail, $ad->getEmail());
            }

            foreach ($leaveDetails as $ld) {
                $alreadyExist = $this->getDoctrine()->getRepository(NotifyLeave::class)->findOneBy(['leaveRequest' => $ld['id']]);
                $userName = $ld['first_name'] . " " . $ld['last_name'];


                if ($alreadyExist == null) {
                    $leaveRequest = $this->getDoctrine()->getRepository(LeaveRequest::class)->findOneBy(['id' => $ld['id']]);
                    $dateNow = date('Y-m-d');
                    $leaveNotify = new NotifyLeave();
                    $leaveNotify->setLeaveRequest($leaveRequest);
                    $leaveNotify->setSeenDates($dateNow);
                    $em->persist($leaveNotify);
                    $em->flush();
                    $email->sendTodaysLeaveMail($userName, $toMail);
                } else {
                    $seenDate = $alreadyExist->getSeenDates();
                    $dateNow = date('Y-m-d');
                    $arrayDates = explode(',', $seenDate);
                    if (in_array($dateNow, $arrayDates)) {
                        return new Response("Mail Already Sent", 200);
                    } else {
                        array_push($arrayDates, $dateNow);
                        $stringDates = implode(",", $arrayDates);
                        $alreadyExist->setSeenDates($stringDates);
                        $em->persist($alreadyExist);
                        $em->flush();
                        $email->sendTodaysLeaveMail($userName, $toMail);
                    }
                }
            }
            return new Response("Mail Has Been Sent", 200);
        } else {
            return new Response("Noone is on leave Today", 200);
        }
    }
}
