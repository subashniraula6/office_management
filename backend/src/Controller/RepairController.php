<?php

namespace App\Controller;

use App\Entity\Inventory;
use App\Entity\Repair;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class RepairController extends AbstractController
{

    /**
     * @Route("/api/admin/inventories/{id}/repairs", methods={"POST"}, name="new_repair")
     */
    public function createRepair($id, Request $request)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findOneBy(['inventoryId' => $id]);

        $parameters = json_decode($request->getContent(), 1);
        $sentAt = $parameters['sentAt'];

        if (empty($inventory)) {
            $response = array(
                'code' => 404,
                'errors' => 'No inventory',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        if ($inventory->getInRepair()) {
            $response = array(
                'code' => 400,
                'errors' => 'Already in repair',
                'result' => null
            );
            return new JsonResponse($response, 400);
        }

        $repair = new Repair();
        $repair->setSentAt($parameters['sentAt'] ? new \DateTime($parameters['sentAt']) : new \DateTime('NOW'));
        $repair->setIsCurrentRepair(true);
        $repair->setInventory($inventory);
        $repair->setIsCompleted(false);

        //change inventory status
        $inventory->setInRepair(true);
        $em->persist($repair);
        $em->persist($inventory);
        $em->flush();

        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return null;
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'sentAt', 'returnedAt', 'isCurrentRepair', 'notes', 'inventory', 'name', 'inventoryId', 'cost', 'remarks'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($repair, 'json');
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/api/admin/inventories/{id}/repair/return", methods={"PUT"}, name="return_from_repair")
     */
    public function returnFromRepair(Request $request, $id)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findOneBy(['inventoryId' => $id]);

        if (empty($inventory)) {
            $response = array(
                'code' => 404,
                'errors' => 'No inventory',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }
        if ($inventory->getInRepair() === false) {
            $response = array(
                'code' => 400,
                'errors' => 'Inventory not in repair',
                'result' => null
            );
            return new JsonResponse($response, 400);
        }

        $parameters = json_decode($request->getContent(), true);
        $cost = $parameters['cost'];
        $remarks = $parameters['remarks'];

        $repair = $this->getDoctrine()
            ->getRepository(Repair::class)->findOneBy(['inventory' => $inventory->getId(), 'isCurrentRepair' => true]);
        $repair->setReturnedAt($parameters['returnedAt'] ? new \DateTime($parameters['returnedAt']) : new \DateTime('NOW'));
        $repair->setIsCurrentRepair(false);
        $repair->setCost($cost);
        $repair->setRemarks($remarks);
        $repair->setIsCompleted(true);
        $em->persist($repair);

        //change inventory status
        $inventory->setInRepair(false);
        $em->persist($inventory);
        $em->flush();

        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return null;
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'sentAt', 'returnedAt', 'isCurrentRepair', 'notes', 'inventory', 'name', 'inventoryId', 'cost', 'remarks'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($repair, 'json');
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/api/admin/inventories/{id}/repairs", methods={"GET"}, name="get_repairs")
     */
    public function getRepairs(Request $request, $id)
    {
        //Authorize admin only
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findOneBy(['inventoryId' => $id]);

        if (empty($inventory)) {
            $response = array(
                'code' => 404,
                'errors' => 'No inventory',
                'result' => null
            );
            return new JsonResponse($response, 404);
        }

        $repairs = $this->getDoctrine()
            ->getRepository(Repair::class)->findBy(['inventory' => $inventory]);

        $encoders = [new JsonEncoder()];
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return null;
            },
            AbstractNormalizer::ATTRIBUTES => [
                'id', 'sentAt', 'returnedAt', 'isCurrentRepair', 'notes', 'inventory', 'name', 'inventoryId', 'cost', 'remarks'
            ]
        ];
        $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)];

        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($repairs, 'json');
        $response = array(
            'code' => 200,
            'errors' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }
}
