<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\LeaveRequest;
use App\Entity\LeaveType;
use App\Utils\LeaveCalculation;
use Doctrine\ORM\EntityManagerInterface;
use Normalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Controller\LeaveRequestController;



class LeaveTypeController extends AbstractController
{
    use LeaveCalculation;

    /**
     * @Route("api/leave/type", name="leave_type", methods="GET")
     */
    public function getLeaveTypeOnUserBasis()
    {
        $roles = $this->getUser()->getRoles()[0];
        $complementary = $this->getDoctrine()->getRepository(LeaveType::class)->findComplementaryLeave();
        $dateNow = Date('Y-m-d');
        $dateNow = date_create($dateNow);
        parse_str($_SERVER['QUERY_STRING'], $result);

        if ($result) {
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $result['userId']]);
            $probationEndAt = $user->getProbationEndAt();
            $userFName = $user->getFirstName();
            $userLName = $user->getLastName();
            $joinedAt = $user->getJoinedAt();
            $userGender = $user->getGender();
            $userDesignation = $user->getDesignation();
        } else {
            $probationEndAt = $this->getUser()->getProbationEndAt();
            $userFName = $this->getUser()->getFirstName();
            $userLName = $this->getUser()->getLastName();
            $joinedAt = $this->getUser()->getJoinedAt();
            $userGender = $this->getUser()->getGender();
            $userDesignation = $this->getUser()->getDesignation();
        }
        $joinedAt = date_create($joinedAt);
        $diff = $joinedAt->diff($dateNow);

        if ($probationEndAt == null) {
            $checkAfterProbation = false;
            $diffMonths = 0;
        } else {
            $probationEndAt =  date_create($probationEndAt);
            $checkAfterProbation =  $dateNow >= $probationEndAt;
            $diffMonths = round(($diff->y * 12 + $diff->m + $diff->d / 30), 2);
        }



        if ($checkAfterProbation == false) {
            $allLeaveType = $this->getDoctrine()->getRepository(LeaveType::class)->findLeaveBeforeProbation();
            if ($result) {
                array_push($allLeaveType, $complementary);
            }

            $result_array = array("data" => $allLeaveType, "firstName" => $userFName, "lastName" => $userLName, "joinedAt" => $joinedAt);
            $data = json_encode($result_array);
            return new Response($data, 200, ['Content-Type' => 'application/json']);
        } else {

            $allLeaveType = $this->getDoctrine()->getRepository(LeaveType::class)->findLeaveAfterProbation($userGender, $diffMonths);
            if ($result) {
                array_push($allLeaveType, $complementary);
            }

            $result_array = array("data" => $allLeaveType, "firstName" => $userFName, "lastName" => $userLName, "joinedAt" => $joinedAt);
            $data = json_encode($result_array);
            return new Response($data, 200, ['Content-Type' => 'application/json']);
        }
    }
    /**
     * @Route("api/add/leaveType", name="add_leave_type", methods="POST")
     */
    public function addLeaveType(Request $request, EntityManagerInterface $em)
    {
        $leaveType = $request->getContent();
        if ($leaveType) {
            $leaveType = json_decode($leaveType, true);


            $leaveTypeName = ucwords($leaveType["leave_type_name"]);
            $leaveTypeTotalDays = (int)$leaveType["leave_type_total_days"];
            $isPayable = (bool)$leaveType['is_payable'];
            $isYearlyRenewed = (bool)$leaveType['is_yearly_renewed'];

            $newLeaveType = new LeaveType();
            $newLeaveType->setLeaveTypeName($leaveTypeName);
            $newLeaveType->setLeaveTypeTotalDays($leaveTypeTotalDays);
            $newLeaveType->setIsPayable($isPayable);
            $newLeaveType->setIsYearlyRenewed($isYearlyRenewed);
            $em->persist($newLeaveType);
            $em->flush();

            $responseResult = $em->getRepository(LeaveType::class)->findAll();
            $normalizers = [new ObjectNormalizer()];
            $encoders = [new JsonEncoder()];

            $serializer = new Serializer($normalizers, $encoders);
            $data = $serializer->serialize($responseResult, 'json');

            return new Response($data, 200, ['Content-Type' => 'application/json']);
        } else {
            return new Response("Please provide leave type", 500);
        }
    }
    /**
     * @Route("api/leaveTypes", name="get_leave_types", methods="GET")
     */
    public function getLeaveTypes()
    {

        $leaveTypes = $this->getDoctrine()->getRepository(LeaveType::class)->findAll();
        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);

        $data = $serializer->serialize($leaveTypes, "json");

        return new Response($data, 200, ["Content-Type" => "application/json"]);
    }
    /**
     * @Route("api/leaveTypes/edit/{id}", name="edit_leave_types", methods="PUT")
     */
    public function editLeaveType($id, Request $request, EntityManagerInterface $em)
    {
        $editedLeaveType = $request->getContent();
        $editedLeaveType = json_decode($editedLeaveType, true);
        // dd($editedLeaveType);

        $leaveType = $this->getDoctrine()->getRepository(LeaveType::class)->findOneBy(['id' => $id]);

        $leaveTypeTotalDays = $leaveType->getLeaveTypeTotalDays();

        $leaveType->setLeaveTypeTotalDays($editedLeaveType['leaveTypeTotalDays']);
        $em->persist($leaveType);
        $em->flush();
        $responseResult = $em->getRepository(LeaveType::class)->findAll();
        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($responseResult, "json");

        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("api/getNonPaidLeave", name = "get_non_paid_leave", methods="GET")
     */
    public function getNonPaidLeave()
    {
        $leaveType = $this->getDoctrine()->getRepository(LeaveType::class)->findBy(['isPayable' => false]);
        $normalizers = [new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($leaveType, "json");

        return new Response($data, 200, ['Content-Type' => 'application/json']);
    }
}
