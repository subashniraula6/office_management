import "./App.css"
import Topbar from "./Components/topbar/Topbar"
import Dashboard from "./Components/dashboard/Dashboard"
import Landing from "./Components/LandingPage/Landing"
import { Route, Switch } from "react-router-dom"
import Login from "./Components/Login/Login"
import Footer from "./Components/Footer/Footer"
import { useEffect } from "react"
import { useDispatch } from "react-redux"
import { fetchCurrentUser } from "./Components/Redux/actions/authActions"
import AdminRoute from "./Components/routing/AdminRoute"
import UserRoute from "./Components/routing/UserRoute"
import PageNotFound from "./Components/Pages/PageNotFound/PageNotFound.jsx"
import UserHomePage from "./Components/userHomePage/HomePage"
import InventoryDetailsProfile from "./Components/InventoryDetailsProfile/InventoryDetailsProfile"
import ForgotPassword from "./Components/Email/ForgotPassword"
import ResetPassword from "./Components/Email/ResetPassword"
import RequestTable from "./Components/Request/RequestTable"
import { Container } from "react-bootstrap"
import UserProfile from "./Components/Users/profile/UserProfile"
import { ToastContainer } from "react-toastify"
import "react-toastify/dist/ReactToastify.css"
import RequestForm from "./Components/Pages/RequestForm/RequestForm"
import Oauth from "./Components/Login/Oauth"
import LeaveApplyForm from "./Components/LeaveApplyForm/LeaveApplyForm"
import MyLeaveDetails from "./Components/MyLeaveDetails/MyLeaveDetails"
import EditPendingRequest from "./Components/LeaveTable/EditPendingRequest/EditPendingRequest"
import UserSideLeaveRequest from "./Components/UserSideLeaveRequest/UserSideLeaveRequest"
import axios from "axios"

function App() {
	const dispatch = useDispatch()
	useEffect(() => {
		dispatch(fetchCurrentUser())
	}, [dispatch])
	useEffect(() => {
		axios
			.post(`${process.env.REACT_APP_API_URL}setLeaveNotify`)
			.then((res) => console.log(res))
			.catch((err) => console.log(err))
	}, [])
	return (
		<div className="App">
			<div>
				<ToastContainer
					position="top-center"
					autoClose={3000}
					hideProgressBar
					newestOnTop={false}
					closeOnClick
					rtl={false}
					pauseOnFocusLoss
					draggable
					pauseOnHover
				/>
			</div>
			<Topbar />
			<div className="container-fluid mb-5">
				<Switch>
					<Route exact path="/" component={Landing} />
					<Route path="/login" exact component={Login} />
					<Route path="/forgotpassword" exact component={ForgotPassword} />
					<Route path="/resetpassword/:token" exact component={ResetPassword} />
					<AdminRoute exact path="/dashboard" component={Dashboard} />
					<AdminRoute exact path="/dashboard/users" component={Dashboard} />
					<AdminRoute
						exact
						path="/dashboard/inventories"
						component={Dashboard}
					/>
					<AdminRoute exact path="/dashboard/requests" component={Dashboard} />
					<AdminRoute
						exact
						path="/dashboard/servicings"
						component={Dashboard}
					/>
					<AdminRoute exact path="/dashboard/leaveTab" component={Dashboard} />
					<AdminRoute
						exact
						path="/dashboard/myLeaveDetails"
						component={Dashboard}
					/>
					<AdminRoute
						exact
						path="/dashboard/leaveTab/edit/:id"
						component={Dashboard}
					/>
					<AdminRoute
						exact
						path="/dashboard/myLeaveDetails/edit/:id"
						component={Dashboard}
					/>
					<AdminRoute exact path="/dashboard/users/:id" component={Dashboard} />
					<AdminRoute
						exact
						path="/dashboard/users/edit/:id"
						component={Dashboard}
					/>
					<AdminRoute exact path="/dashboard/users/add" component={Dashboard} />
					<AdminRoute
						exact
						path="/dashboard/inventories/add"
						component={Dashboard}
					/>
					<AdminRoute
						exact
						path="/dashboard/myLeaveDetails/applyLeave"
						component={Dashboard}
					/>
					<AdminRoute
						exact
						path="/dashboard/inventories/edit/:id"
						component={Dashboard}
					/>
					<AdminRoute
						exact
						path="/dashboard/leaveTab/leaveTypes"
						component={Dashboard}
					/>
					<AdminRoute
						exact
						path="/dashboard/inventories/:id"
						component={Dashboard}
					/>

					<UserRoute
						exact
						path="/users/:id/inventories"
						component={UserHomePage}
					/>
					<UserRoute
						exact
						path="/inventories/:id"
						component={InventoryDetailsProfile}
					/>
					<UserRoute exact path="/requests/add" component={RequestForm} />
					<UserRoute
						exact
						path="/leave/applyLeave"
						component={LeaveApplyForm}
					/>
					<UserRoute
						exact
						path="/leave/myLeaveDetails"
						component={MyLeaveDetails}
					/>
					<UserRoute
						exact
						path="/leave/leaveRequests"
						component={UserSideLeaveRequest}
					/>

					<UserRoute
						exact
						path="/leave/myLeaveDetails/edit/:id"
						component={EditPendingRequest}
					/>
					<UserRoute
						exact
						path="/requests"
						render={(props) => (
							<Container>
								<RequestTable {...props} role="ROLE_USER" />
							</Container>
						)}
					/>
					<UserRoute exact path="/users/:id" component={UserProfile} />
					<Route path="/oauth" component={Oauth} />
					<Route path="*" exact component={PageNotFound} />
				</Switch>
			</div>
			<Footer />
		</div>
	)
}

export default App
