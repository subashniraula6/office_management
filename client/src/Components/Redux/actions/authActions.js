import axios from "../../utils/axios"
import { notify } from "../../utils/toast"

//Login User
export const loginUser = (userCredentials) => async (dispatch) => {
	const body = JSON.stringify(userCredentials)
	try {
		const res = await axios.post("/api/login", body)
		dispatch({
			type: "LOGIN_SUCCESS",
			payload: res.data,
		})
		dispatch(fetchCurrentUser())
		if (res.status === 200) {
			notify("success", "logged in!")
		} else {
			notify("error", "login failed!")
		}
	} catch (error) {
		dispatch({
			type: "LOGIN_FAILURE",
			payload: error.response && error.response.data.message,
		})
	}
}

export const oAuthLogin = (id_token, history) => async (dispatch) => {
	try {
		const res = await axios.post("/api/oauth", id_token)
		dispatch({
			type: "LOGIN_SUCCESS",
			payload: res.data,
		})
		dispatch(fetchCurrentUser())
		if (res.status === 200) {
			notify("success", "logged in!")
		} else {
			notify("error", "login failed!")
		}
	} catch (error) {
		notify("error", "Login Failed!")
		history.push("/login")
		dispatch({
			type: "LOGIN_FAILURE",
			payload: error.response && error.response.data.message,
		})
	}
}

export const fetchCurrentUser = () => async (dispatch) => {
	try {
		const userResponse = await axios.get("/api/me")
		const user = userResponse.data.user
		dispatch({
			type: "USER_LOADED",
			payload: user,
		})
	} catch (error) {
		dispatch({
			type: "AUTH_ERROR",
		})
	}
}

export const logoutUser = () => async (dispatch) => {
	try {
		//clear inventories, requests
		dispatch({
			type: "LOGOUT_SUCCESS",
		})
		notify("error", "logged out!")
	} catch (error) {
		dispatch({
			type: "LOGOUT_FAILURE",
		})
	}
}
