<?php

namespace App\Services;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Email extends AbstractController
{
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }
    public function sendAccountCreationLink($to, $link)
    {
        $sender = $this->getParameter('app.mailersender');
        $message = (new \Swift_Message('Hello Email'))
            ->setSubject('Account created')
            ->setFrom($sender)
            ->setTo($to)
            ->setBody($this->renderView('email/accountCreation.html.twig', ['link' => $link]), 'text/html');
        $this->mailer->send($message);
    }
    public function sendLink($to, $link)
    {
        $sender = $this->getParameter('app.mailersender');
        $message = (new \Swift_Message('Hello Email'))
            ->setSubject('Password reset')
            ->setFrom($sender)
            ->setTo($to)
            ->setBody($this->renderView('email/emailLink.html.twig', ['link' => $link]), 'text/html');
        $this->mailer->send($message);
    }
    public function sendResetMessage($to)
    {
        $sender = $this->getParameter('app.mailersender');
        $message = (new \Swift_Message('Hello Email'))
            ->setSubject('Password reset')
            ->setFrom($sender)
            ->setTo($to)
            ->setBody($this->renderView('email/resetInfo.html.twig'), 'text/html');
        $this->mailer->send($message);
    }

    public function sendLeaveRequestMail($userName, $to)
    {
        $sender = $this->getParameter('app.mailersender');

        foreach ($to as $a) {
            $message = (new \Swift_Message('Leave Request'))
                ->setSubject('Request for Leave')
                ->setFrom($sender)
                ->setTo($a)
                ->setBody($this->renderView('email/leaveRequest.html.twig', ['userName' => $userName]), 'text/html');
            $this->mailer->send($message);
        }
    }

    public function sendTodaysLeaveMail($userName, $toMail)
    {
        $sender = $this->getParameter('app.mailersender');

        foreach ($toMail as $to) {
            $message = (new \Swift_Message('Today`s Leave'))
                ->setSubject('Today`s Leave')
                ->setFrom($sender)
                ->setTo($to)
                ->setBody($this->renderView('email/todaysLeave.html.twig', ['userName' => $userName]), 'text/html');
            $this->mailer->send($message);
        }
    }
}
