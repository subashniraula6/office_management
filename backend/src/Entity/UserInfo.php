<?php

namespace App\Entity;

use App\Repository\UserInfoRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserInfoRepository::class)
 */
class UserInfo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $citizenshipNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $panNumber;

    /**
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/\d{9}$/", message="please enter correct phone number")
     * @ORM\Column(type="bigint")
     */
    private $phoneNumber;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $temporaryAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $permanentAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accountNumber;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $notes;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="userInfo", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $ppImagePath;

    public function getId(): ?int
    {
        return $this->id;
    }

    
    public function getTemporaryAddress()
    {
        return $this->temporaryAddress;
    }

    public function setTemporaryAddress(string $temporaryAddress): self
    {
        $this->temporaryAddress = $temporaryAddress;

        return $this;
    }

    public function getPermanentAddress()
    {
        return $this->permanentAddress;
    }

    public function setPermanentAddress(string $permanentAddress): self
    {
        $this->permanentAddress = $permanentAddress;

        return $this;
    }


    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    public function setAccountNumber(string $accountNumber): self
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getCitizenshipNumber()
    {
        return $this->citizenshipNumber;
    }

    public function setCitizenshipNumber(string $citizenshipNumber): self
    {
        $this->citizenshipNumber = $citizenshipNumber;

        return $this;
    }

    public function getPanNumber()
    {
        return $this->panNumber;
    }

    public function setPanNumber(string $panNumber): self
    {
        $this->panNumber = $panNumber;

        return $this;
    }
    
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPpImagePath(): ?string
    {
        return $this->ppImagePath;
    }

    public function setPpImagePath(?string $ppImagePath): self
    {
        $this->ppImagePath = $ppImagePath;

        return $this;
    }
}
