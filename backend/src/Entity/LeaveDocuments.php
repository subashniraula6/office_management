<?php

namespace App\Entity;

use App\Repository\LeaveDocumentsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LeaveDocumentsRepository::class)
 */
class LeaveDocuments
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $documentsPath;

    /**
     * @ORM\ManyToOne(targetEntity=LeaveRequest::class)
     */
    private $leaveRequest;

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocumentsPath(): ?string
    {
        return $this->documentsPath;
    }

    public function setDocumentsPath(?string $documentsPath): self
    {
        $this->documentsPath = $documentsPath;

        return $this;
    }

    public function getLeaveRequest(): ?LeaveRequest
    {
        return $this->leaveRequest;
    }

    public function setLeaveRequest(?LeaveRequest $leaveRequest): self
    {
        $this->leaveRequest = $leaveRequest;

        return $this;
    }

    
}
