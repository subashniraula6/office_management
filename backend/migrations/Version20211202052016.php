<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211202052016 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE leave_request CHANGE start_first_half start_first_half TINYINT(1) DEFAULT NULL, CHANGE start_second_half start_second_half TINYINT(1) DEFAULT NULL, CHANGE end_first_half end_first_half TINYINT(1) DEFAULT NULL, CHANGE end_second_half end_second_half TINYINT(1) DEFAULT NULL, CHANGE approvers approvers LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE leave_request CHANGE start_first_half start_first_half TINYINT(1) NOT NULL, CHANGE start_second_half start_second_half TINYINT(1) NOT NULL, CHANGE end_first_half end_first_half TINYINT(1) NOT NULL, CHANGE end_second_half end_second_half TINYINT(1) NOT NULL, CHANGE approvers approvers LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\'');
    }
}
