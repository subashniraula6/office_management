<?php

namespace App\Entity;

use App\Repository\ApproversRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApproversRepository::class)
 */
class Approvers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=LeaveRequest::class, inversedBy="approvers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $leaveRequest;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $isApproved;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAdminApproved;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $adminId;





    public function __construct()
    {
        $this->user = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }



    public function getLeaveRequest(): ?LeaveRequest
    {
        return $this->leaveRequest;
    }

    public function setLeaveRequest(?LeaveRequest $leaveRequest): self
    {
        $this->leaveRequest = $leaveRequest;

        return $this;
    }

    public function getIsApproved(): ?bool
    {
        return $this->isApproved;
    }

    public function setIsApproved(bool $isApproved): self
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsAdminApproved(): ?bool
    {
        return $this->isAdminApproved;
    }

    public function setIsAdminApproved(bool $isAdminApproved): self
    {
        $this->isAdminApproved = $isAdminApproved;

        return $this;
    }

    public function getAdminId(): ?int
    {
        return $this->adminId;
    }

    public function setAdminId(?int $adminId): self
    {
        $this->adminId = $adminId;

        return $this;
    }
}
